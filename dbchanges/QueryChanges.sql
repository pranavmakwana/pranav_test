#Change Dashboard Resolution Time and Response Time Column   1/7/2020
alter table ticketsummaries modify column responsetime int,modify resolutiontime int

#update all data status is active  26/6/2020
UPDATE users SET is_active = TRUE ;
UPDATE companies SET is_active = TRUE ;
UPDATE documents SET is_active = TRUE ;
UPDATE reportactivities SET is_active = TRUE ;
UPDATE reports SET is_active = TRUE ;
UPDATE reportsections SET is_active = TRUE ;
UPDATE templates SET is_active = TRUE ;

#Change in Report frquencyofactivities is string  1/7/2020
ALTER TABLE reportdetails MODIFY COLUMN frquencyofactivities VARCHAR(50); 

