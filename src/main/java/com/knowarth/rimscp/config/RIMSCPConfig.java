package com.knowarth.rimscp.config;

import static io.swagger.models.auth.In.HEADER;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@ComponentScan("com.knowarth.rimscp")
@EnableSwagger2
public class RIMSCPConfig {

	@Bean
	public Docket jsonApi() {
		return new Docket(SWAGGER_2).securitySchemes(singletonList(new ApiKey("JWT", AUTHORIZATION, HEADER.name())))
				.securityContexts(singletonList(SecurityContext.builder()
						.securityReferences(singletonList(
								SecurityReference.builder().reference("JWT").scopes(new AuthorizationScope[0]).build()))
						.build()))
				.select().apis(RequestHandlerSelectors.basePackage("com.knowarth.rimscp.web.controller")).build();
	}

	
	
	
	@Bean
	public RestTemplate gerRestTemplate() {
		return new RestTemplate();
	}

}
