package com.knowarth.rimscp.web.model.zoho.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TicketSummaryHandler {

	private String key;
	private String status;
	private String title;
	@JsonProperty("escalation_level")
	private String escalationLevel;

	private String severity;

	@JsonProperty("created_time_format")
	private String createdTimeFormat;

	private String module;

	@JsonProperty("updated_time_format")
	private String updatedTimeFormat;

	private String zohoId;

}
