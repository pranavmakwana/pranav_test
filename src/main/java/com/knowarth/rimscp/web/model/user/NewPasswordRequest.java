package com.knowarth.rimscp.web.model.user;

import lombok.Data;

/**
 * To Update password following token
 * 
 * @author vinit.prajapati
 */
@Data
public class NewPasswordRequest
{

	private String password;
	private String confirmPassword;

}
