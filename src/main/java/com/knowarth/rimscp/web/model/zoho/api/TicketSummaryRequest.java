package com.knowarth.rimscp.web.model.zoho.api;

import java.util.List;

import lombok.Data;

@Data
public class TicketSummaryRequest {

	private List<BugTicketSummaryRequest> bugs;

}
