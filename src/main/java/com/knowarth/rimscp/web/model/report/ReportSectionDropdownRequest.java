package com.knowarth.rimscp.web.model.report;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ReportSectionDropdownRequest
{

	private String template;
	private List<String> section = new ArrayList<>(); 
}
