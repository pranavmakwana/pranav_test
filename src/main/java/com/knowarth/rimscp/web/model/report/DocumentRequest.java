package com.knowarth.rimscp.web.model.report;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class DocumentRequest
{

	private Long id;
	
	@JsonProperty("documentname")
	private String documentName;
	@JsonProperty("documenttype")
	private String documentType;	
	private String description;
	private String version;
	private String company;
	@JsonProperty("filename")
	private String fileName;  
	@JsonProperty("reviewedby")
	private String  reviewedBy;
	@JsonProperty("revieweddate")
	private Date reviewedDate;
	@JsonProperty("approvedby")
	private String approvedBy;
	@JsonProperty("approveddate")
	private Date  approvedDate;
	@JsonProperty("createdby")
	private String createdBy;
	@JsonProperty("createddate")
	private Date createdDate;
	@JsonProperty("updatedby")
	private String updatedBy;
	@JsonProperty("updateddate")
	private Date updatedDate;
	private String role;
	private String status;
	private String isActive;
	 
}
