package com.knowarth.rimscp.web.model.zoho.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SeverityDetails {

	@JsonProperty("severity_id")
	private String severityId;

	@JsonProperty("severity_name")
	private String severityName;

}
