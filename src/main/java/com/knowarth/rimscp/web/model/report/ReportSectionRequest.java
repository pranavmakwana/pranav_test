package com.knowarth.rimscp.web.model.report;

import lombok.Data;

@Data
public class ReportSectionRequest {
	private Long id;
	private String name;
	private String templateName;
	private String isActive;
}
