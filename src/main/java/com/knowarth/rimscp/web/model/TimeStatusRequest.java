package com.knowarth.rimscp.web.model;

import lombok.Data;

@Data
public class TimeStatusRequest {

    private Long id;

    private String companyName;

    private double upTime;

    private double downTime;

    private double maintenance;

    private String month;

    private String year;
    
    private String isActive;
}
