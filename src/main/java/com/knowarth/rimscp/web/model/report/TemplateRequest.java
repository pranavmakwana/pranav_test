package com.knowarth.rimscp.web.model.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class TemplateRequest {
	private Long id;
	private String name;
	private String description;
	private String isActive;
}
