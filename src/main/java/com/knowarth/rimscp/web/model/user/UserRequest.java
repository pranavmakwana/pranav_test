package com.knowarth.rimscp.web.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * User Request
 * 
 * @author vinit.prajapati
 */
@Data
@JsonInclude(Include.NON_NULL)
public class UserRequest {
	private Long id;
	@JsonProperty("firstname")
	private String firstName;
	@JsonProperty("lastname")
	private String lastName;
	private String email;
	private String password;
	private String phone;
	private String role;
	private String company;
	private String profile;
	private String isActive;
}
