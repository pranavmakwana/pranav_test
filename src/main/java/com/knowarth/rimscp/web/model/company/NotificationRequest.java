package com.knowarth.rimscp.web.model.company;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class NotificationRequest 
{
	private Long id;
	private String type;
	private Long document;
	private String user;
	private String role;
	private String text;
	private String status;
	private String comment;
	private Boolean completed;
	private String profile;
	@JsonProperty("requesteddate")
	private Date requestedDate;

}
