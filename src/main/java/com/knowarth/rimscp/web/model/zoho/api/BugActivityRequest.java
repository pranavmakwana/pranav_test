package com.knowarth.rimscp.web.model.zoho.api;

import lombok.Data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class BugActivityRequest {

	@JsonProperty("activity_details")
	List<BugActivityDetailRequest> activityDetails;
}
