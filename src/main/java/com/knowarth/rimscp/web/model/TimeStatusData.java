package com.knowarth.rimscp.web.model;

import lombok.Data;

@Data
public class TimeStatusData {

    private double avgUptime;

    private double avgDownTime;

    private double avgMaintenance;
}
