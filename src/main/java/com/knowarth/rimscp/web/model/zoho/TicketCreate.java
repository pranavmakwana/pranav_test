package com.knowarth.rimscp.web.model.zoho;

import java.io.File;

import lombok.Data;

@Data
public class TicketCreate {
	private String title;
	private String description;
	private Long moduleId;
	private Long severityId;
	private File uploadDoc;
	private String bugId;
	public TicketCreate() {
	}

	public TicketCreate(String title, String description, Long moduleId, Long severityId, String bugId) {
		super();
		this.title = title;
		this.description = description;
		this.moduleId = moduleId;
		this.severityId = severityId;
		this.bugId = bugId;
	}

}
