package com.knowarth.rimscp.web.model.aws;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class EC2Detail
{
	private String publicIp;

	private String privateIp;

	private String instanceType;

	private String instanceId;
	
	private String instanceName;

	private String name;

	private Integer cpus;

	private String availabilityZone;

	private String status;

//	@JsonFormat(pattern = "yyyy-MM-dd hh:mm a")
	@JsonFormat(pattern = "dd-MM-yyyy hh:mm a")
	private Date launchTime;
	
	private String companyName;
	
	
	
	
}
