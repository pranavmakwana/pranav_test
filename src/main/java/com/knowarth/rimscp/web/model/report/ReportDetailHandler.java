package com.knowarth.rimscp.web.model.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.ALWAYS)
public class ReportDetailHandler
{

	private String sectionName;

	private String activityName;

	private Long id;

	private String attachment;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date date;

	private String frequencyOfActivities;

	private String notes;

	private String priority;

	private String server;

	private String status;

	private Long reportId;

	private Long activityId;

	public ReportDetailHandler(String sectionName, String activityName, Long id, String attachment,
			Date date,  String frequencyOfActivities,
			String notes, String priority,
			String server, String status, Long reportId, Long activityId)
	{
		this.sectionName = sectionName;
		this.activityName = activityName;
		this.id = id;
		this.attachment = attachment;
		this.date = date;
		this.frequencyOfActivities = frequencyOfActivities;
		this.notes = notes;
		this.priority = priority;
		this.server = server;
		this.status = status;
		this.reportId = reportId;
		this.activityId = activityId;
	}

}
