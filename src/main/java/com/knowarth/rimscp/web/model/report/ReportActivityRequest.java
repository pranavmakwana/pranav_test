package com.knowarth.rimscp.web.model.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class ReportActivityRequest
{

	private Long id;
	@JsonProperty("name")
	private String name;
	private String sectionName;
	private String templateName;
	private String isActive;
}
