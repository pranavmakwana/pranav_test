package com.knowarth.rimscp.web.model.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ReportActivityDetailRequest
{

	private Long id;
	private Long reportId;
	private String activity;
	private String server;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date date;
	private String frequencyOfActivities;
	private String priority;
	private Long activityId;
	private String status;
	private String notes;
	private String fileId;
	private String attachement;
	private String isActive;
	public ReportActivityDetailRequest()
	{
	}

	public ReportActivityDetailRequest(String activity, String server,
			Date date, String frequencyOfActivities, String priority,
			String status, String notes, String attachement)
	{
		this.activity = activity;
		this.server = server;
		this.date = date;
		this.frequencyOfActivities = frequencyOfActivities;
		this.priority = priority;
		this.status = status;
		this.notes = notes;
		this.attachement = attachement;
		
	}
}
