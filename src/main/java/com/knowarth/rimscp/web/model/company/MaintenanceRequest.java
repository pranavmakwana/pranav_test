package com.knowarth.rimscp.web.model.company;

import java.time.LocalDate;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MaintenanceRequest
{

	private Long id; 
	private String company;
	@JsonProperty("deploymentdate")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate deploymentDate;
	@JsonProperty("servername")
	private String serverName;
	private String description;
	@JsonProperty("starttime")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private String startTime;
	@JsonProperty("endtime")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private String endTime;
	private String user;
	private String status;
	private String bugId;
	private String ticketId;
}
