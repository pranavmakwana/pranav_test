package com.knowarth.rimscp.web.model.aws;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class RDSDetail {

	private String availabilityZone;
	private Integer retentionPeriod;
	private String storageType;
	private Integer allocatedStorage;
	
	@JsonFormat(pattern = "dd-MM-yyyy hh:mm a")
	private Date createdTime;
	
	private boolean isStorageEncrypted;
	private boolean isPubliclyAccessible;
	
	@JsonFormat(pattern = "dd-MM-yyyy hh:mm a")
	private Date latestRestorableTime;
	
	private String engine;
	
	private String status;
	private String identifier;
	private String companyName;
}
