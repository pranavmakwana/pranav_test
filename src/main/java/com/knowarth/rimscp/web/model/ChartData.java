package com.knowarth.rimscp.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChartData {
	String label; //day1
	List<Integer> values = new ArrayList<>();	
	List<String> types = new ArrayList<>();
}
