package com.knowarth.rimscp.web.model.zoho.api;

import lombok.Data;

/**
 * Generic Response Message
 * 
 * @author vinit.prajapati
 */
@Data
public class ResponseMessage {

	private String status;
	private String message;

	/**
	 * @param status
	 * @param message
	 */
	public ResponseMessage(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
}
