package com.knowarth.rimscp.web.model.report;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ReportDetailRequest
{

	private String section;
	private List<ReportActivityDetailRequest> reportActivityDetailRequests = new ArrayList<>();

}
