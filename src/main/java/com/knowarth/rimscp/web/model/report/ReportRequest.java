package com.knowarth.rimscp.web.model.report;

import java.util.List;
import lombok.Data;

@Data
public class ReportRequest
{

	private ReportMasterRequest report;
	private List<ReportActivityDetailRequest> activities;

}
