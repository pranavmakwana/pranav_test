package com.knowarth.rimscp.web.model.company;

import lombok.Data;

/**
 * Company Requests
 * 
 * @author vinit.prajapati
 */
@Data
public class CompanyRequest
{ 
	private Long id;
	private String name;
	private String address;
	private String contactNo;
	private String website;
	private String logo;
	private String shortName;
	private String region;
	private String clientId;
	private String secret;
	private String projectKey;
	private String isActive;
}
