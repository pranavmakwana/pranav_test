package com.knowarth.rimscp.web.model;

import lombok.Data;

@Data
public class DependantData {
	private Long count;
	private String type;
	public DependantData(Long count, String type) {
		this.count = count;
		this.type = type;
	}
	
}
