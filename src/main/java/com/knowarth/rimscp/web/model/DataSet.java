package com.knowarth.rimscp.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataSet {
	private String label;
	private String backgroundColor;
	private int data[];
}
