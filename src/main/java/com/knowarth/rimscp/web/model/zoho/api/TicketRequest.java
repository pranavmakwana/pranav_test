package com.knowarth.rimscp.web.model.zoho.api;

import java.util.List;

import lombok.Data;

@Data
public class TicketRequest {

	private List<BugRequest> bugs;
}
