package com.knowarth.rimscp.web.model.user;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import com.knowarth.rimscp.db.model.User;
import lombok.Data;

/**
 * RIMS Application User
 * 
 * @author vinit.prajapati
 */
@Data
public class RIMSUser implements UserDetails {
	private static final long serialVersionUID = -2535058383761037494L;

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String role;
	private String company;
	private Collection<? extends GrantedAuthority> authorities;

	/**
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 * @param role
	 */
	public RIMSUser(Long id, String firstName, String lastName, String email, String password, String role,
			String company) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.role = role;
		this.company = company;

		authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(role);
	}

	/**
	 * @param user
	 */
	public RIMSUser(final User user) {
		this(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(),
				"ROLE_" + user.getRole().getName(), user.getCompany().getName());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
