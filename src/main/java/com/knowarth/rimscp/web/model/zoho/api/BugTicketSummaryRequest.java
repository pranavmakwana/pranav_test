package com.knowarth.rimscp.web.model.zoho.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.knowarth.rimscp.web.model.report.StatusRequest;

import lombok.Data;

@Data
public class BugTicketSummaryRequest {

	private String key;
	private StatusRequest status;
	private String title;
	@JsonProperty("escalation_level")
	private String escalationLevel;
	private SeverityRequest severity;
	@JsonProperty("updated_time_format")
	private String createdTimeFormat;
	private ModuleRequest module;
	@JsonProperty("updated_time_format")
	private String updatedTimeFormat;
	private String id;

}
