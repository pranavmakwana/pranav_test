package com.knowarth.rimscp.web.model.report;

import lombok.Data;

@Data
public class StatusRequest {

	private String colorcode;
	private String id;
	private String type;

}
