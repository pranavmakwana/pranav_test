package com.knowarth.rimscp.web.model.report;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReportMasterRequest
{

	private Long reportId;
	private String name;
	private String description; // this is Report name in angular side
	private String clientName;
	@JsonProperty("reviewedby")
	private String  reviewedBy;
	@JsonProperty("revieweddate")
	private Date reviewedDate;
	@JsonProperty("approvedby")
	private String approvedBy;
	@JsonProperty("approveddate")
	private Date  approvedDate;
	@JsonProperty("createdby")
	private String createdBy;
	@JsonProperty("createddate")
	private Date createdDate;
	@JsonProperty("updatedby")
	private String updatedBy;
	@JsonProperty("updateddate")
	private Date updatedDate;
	@JsonProperty("role")
	private String role;
	private String status;
	private String isActive;
}
