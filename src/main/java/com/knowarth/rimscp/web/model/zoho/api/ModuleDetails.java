package com.knowarth.rimscp.web.model.zoho.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ModuleDetails {

	@JsonProperty("module_id")
	private String moduleId;

	@JsonProperty("module_name")
	private String moduleName;
}
