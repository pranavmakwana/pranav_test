package com.knowarth.rimscp.web.model.zoho.api;

import lombok.Data;

@Data
public class SeverityRequest {
	private Long id;
	private String type;
}
