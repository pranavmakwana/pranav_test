package com.knowarth.rimscp.web.model.zoho.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.knowarth.rimscp.web.model.report.StatusRequest;

import lombok.Data;

@Data
public class BugRequest {

	private String description;

	@JsonProperty("assignee_name")
	private String assigneeName;

	private Long id;

	@JsonProperty("escalation_level")
	private String escalationLevel;

	private SeverityRequest severity;

	private ModuleRequest module;

	@JsonProperty("created_time")
	private String createdTime;

	@JsonProperty("reported_person")
	private String reportedPerson;

	@JsonProperty("reporter_email")
	private String reporterEmail;

	private StatusRequest status;

	@JsonProperty("due_date")
	private String dueDate;

	private String key;

	@JsonProperty("updated_time_long")
	private Long updatedTimeLong;

	private String title;

	@JsonProperty("created_time_long")
	private Long createdTimeLong;

	private Boolean closed;
}
