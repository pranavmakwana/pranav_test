package com.knowarth.rimscp.web.model.zoho.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BugActivityDetailRequest {

	@JsonProperty("action_time_long")
	private Long actionTimeLong;

	@JsonProperty("action_field")
	private String actionField;

	@JsonProperty("previous_value")
	private String previousValue;

	private String action;

	private String type;

	@JsonProperty("current_value")
	private String currentValue;

}
