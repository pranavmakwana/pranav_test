package com.knowarth.rimscp.web.model.zoho.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Defaultfields {

	@JsonProperty("module_details")
	private List<ModuleDetails> moduelDetails;

	@JsonProperty("severity_details")
	private List<SeverityDetails> severity_details;
}
