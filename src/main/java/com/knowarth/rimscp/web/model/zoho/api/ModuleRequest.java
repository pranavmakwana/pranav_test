package com.knowarth.rimscp.web.model.zoho.api;

import lombok.Data;

@Data
public class ModuleRequest {

	private String name;
	private Long id;
}
