package com.knowarth.rimscp.web.controller.company.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.service.company.report.ITemplateService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.TemplateRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class TemplateController {
	
	@Autowired
	private ITemplateService service;

	/**
	 * Create / Update Template
	 *
	 * @param templateRequest
	 * @return ResponseMessage
	 */
	@RequestMapping(value = "/templates", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> createTemplate(@RequestBody TemplateRequest templateRequest) {
		Long id = service.createTemplate(templateRequest);
		ResponseMessage response = WebUtil.getResponse("Success", id, "Template");
		return new ResponseEntity<ResponseMessage>(response, HttpStatus.CREATED);
	}

	/**
	 * List Of Active Templates
	 */
	@GetMapping(value = "/templates")
	public List<TemplateRequest> getTemplates() {
		return service.getTemplates();
	}
	 
	/**
	 * List Of InActive Templates
	 */
	@GetMapping(value = "/inactive-templates")
	public List<TemplateRequest> getInActiveTemplates() {
		return service.getInActiveTemplates();
	}
	
	@RequestMapping(value = "/templates/deactivate/{id}",method = RequestMethod.PUT)
	public Long deactiveTemplate(@PathVariable long id)
	{
		return service.deactivateTemplate(id);
	}
	
	@RequestMapping(value = "/templates/activate/{id}",method = RequestMethod.PUT)
	public Long activeTemplate(@PathVariable long id)
	{
		return service.activateTemplate(id);
	}
	
	@DeleteMapping(path = "/templates/{id}")
	public Long deleteSection(@PathVariable long id)
	{
		return service.deleteTemplate(id);
	}
}
