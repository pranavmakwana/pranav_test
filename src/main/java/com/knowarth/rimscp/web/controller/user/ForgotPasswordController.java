package com.knowarth.rimscp.web.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.service.user.EmailService;
import com.knowarth.rimscp.service.user.ForgotPasswordTokenService;
import com.knowarth.rimscp.web.model.user.NewPasswordRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

/**
 * Handled forgot password functionality
 * 
 * @author Ali
 */
@RestController
public class ForgotPasswordController {
	@Autowired
	private ForgotPasswordTokenService forgotService;

	@Autowired
	private EmailService emailService;

	@GetMapping("/user-management/credential/generate-token/{email}")
	public ResponseEntity<ResponseMessage> generateToken(@PathVariable String email) {
		return forgotService.generateForgotPasswordToken(email);
	}

	@GetMapping("/user-management/credential/validate-token/{token}")
	public boolean validateToken(@PathVariable String token) {
		return forgotService.isValidToken(token);
	}

	@PostMapping("/user-management/credential/change-password/{token}")
	public ResponseEntity<ResponseMessage> changePassword(@PathVariable String token,
			@RequestBody NewPasswordRequest newPasswordRequest) {
		// validate password & confirm password
		if (forgotService.isValidToken(token)) {
			return forgotService.changePassword(token, newPasswordRequest.getPassword());
		}
		return null;
	}

	@PostMapping("/user-management/credential/update-password")
	public ResponseEntity<ResponseMessage> updatePassword(@RequestBody NewPasswordRequest newPasswordRequest) {
		return forgotService.updatePassword(newPasswordRequest.getPassword());
	}

	@GetMapping("/testmail/{to}")
	public void testMail(@PathVariable String to) {
		emailService.sendResetPasswordEmail(to, "mytoken");
	}

}
