package com.knowarth.rimscp.web.controller.company.report;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.service.company.report.IReportActivityService;
import com.knowarth.rimscp.service.company.report.ReportActivityService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.ReportActivityRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class ReportActivityController 
{
	@Autowired
	private IReportActivityService activityService;

	@Autowired
	private ReportActivityService reportActivityService;

	/**
	 * create and update activity
	 */
	@RequestMapping(value = "/report-management/report/activities", method = {RequestMethod.POST, RequestMethod.PUT})
	public ResponseEntity<ResponseMessage> createActivity(@RequestBody ReportActivityRequest reportActivityRequest)
	{
		long id = reportActivityService.createActivity(reportActivityRequest);
		ResponseMessage response = WebUtil.getResponse("Success", id, "ReportActivity");
		return new ResponseEntity<>(response, HttpStatus.CREATED);

	}

	/**
	 * get activity name
	 */
	@GetMapping(value = "/report-management/report/activities/byname")
	public ResponseEntity<List<String>> getActivity()
	{
		return new ResponseEntity<>(activityService.getActivity(), HttpStatus.OK);
	}

	/**
	 * get active activity data
	 */

	@GetMapping(value = "/report-management/report/activities")
	public List<ReportActivityRequest> getAllActivities()
	{
		return reportActivityService.getAllActivities();
	}
	
	/**
	 * get in-active activity data
	 */

	@GetMapping(value = "/report-management/report/inactive-activities")
	public List<ReportActivityRequest> getInActiveAllActivities()
	{
		return activityService.getInActiveAllActivities();
	}
	
	@DeleteMapping(path = "/report-management/report/activities/{id}")
	public Long deleteActivity(@PathVariable long id)
	{
		return activityService.deleteActivity(id);
	}
}
