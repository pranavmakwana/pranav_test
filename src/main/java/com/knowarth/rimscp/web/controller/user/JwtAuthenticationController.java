package com.knowarth.rimscp.web.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.user.JwtTokenUtil;
import com.knowarth.rimscp.service.user.JwtUserDetailsService;
import com.knowarth.rimscp.web.model.user.JwtRequest;
import com.knowarth.rimscp.web.model.user.JwtResponse;
import com.knowarth.rimscp.web.model.user.RIMSUser;

@RestController
public class JwtAuthenticationController
{

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CompanyRespository companyRepo;

	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception
	{
		User user = userRepository.findOneByEmail(authenticationRequest.getUsername());
		Boolean status = user.getIsActive();
		Boolean companyStatus = companyRepo.getOne(user.getCompany().getId()).getIsActive();
		if(status && companyStatus) { 
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
			final String token = jwtTokenUtil.generateToken(userDetails);
			return ResponseEntity.ok(new JwtResponse(token));
		}else {
			return null;
		}
		
	}

	private void authenticate(String username, String password) throws Exception
	{
		try
		{
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		}
		catch (DisabledException e)
		{
			throw new Exception("USER_DISABLED", e);
		}
		catch (BadCredentialsException e)
		{
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
