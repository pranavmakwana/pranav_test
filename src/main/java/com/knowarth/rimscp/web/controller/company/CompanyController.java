package com.knowarth.rimscp.web.controller.company;

import java.io.IOException;
import java.util.List;

import javax.sound.midi.Soundbank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowarth.rimscp.service.company.ICompanyService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.company.CompanyRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

/**
 * Company Resource
 *
 * @author vinit.prajapati
 */
@RestController
public class CompanyController { 
	@Autowired
	private ICompanyService service;

	/**
	 * Create / Update Companies
	 * 
	 * @param company
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException
	 */
	@RequestMapping(value = "/companies", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> createCompany(@RequestParam(required = false) MultipartFile file,
			@NotNull @RequestParam("company") String company) throws IOException {
		
		
		CompanyRequest companyRequest = new ObjectMapper().readValue(company, CompanyRequest.class);
		Long id = service.createCompany(companyRequest, file);
//		if(id!=null) {
			ResponseMessage response = WebUtil.getResponse("Success", id, "Company");
			return new ResponseEntity<>(response, HttpStatus.CREATED);
			
//		} 
//		ResponseMessage response = WebUtil.getResponse("Success", id, "Company");
		
//		return new ResponseEntity<>(WebUtil.getResponse("Error", 0L, "Company"), HttpStatus.INTERNAL_SERVER_ERROR);
		
	}

	/**
	 * returns company name as List<String>
	 * 
	 * @return List of active Company
	 */
	@GetMapping(value = "/companies")
	public ResponseEntity<List<String>> getCompanies() { 
		return new ResponseEntity<>(service.getCompanies(), HttpStatus.OK);
	}

	/**
	 * returns company name as List<Company>
	 *
	 * @return List of active Company
	 */
	@GetMapping(value = "/companies-detail")
	public List<CompanyRequest> getAllCompanies() {
		return service.getAllCompanies();
	}

	/**
	 * returns company name as List<Company>
	 *
	 * @return List of inactive Company
	 */
	@GetMapping(value = "/inactive-companies-detail")
	public List<CompanyRequest> getAllInActiveCompanies() {
		return service.getAllInActiveCompanies();
	}
	
	@GetMapping("/company/logo/{logo}")
	public @ResponseBody byte[] getMyCompanyLogo(@PathVariable String logo) {
		return service.getMyCompanyLogo(logo);
	}
	
	@DeleteMapping(path = "/companies/{id}")
	public Long deleteCompany(@PathVariable long id)
	{
		return service.deleteCompany(id);
	}
	
	@RequestMapping(value = "/companies/deactivate/{id}",method = RequestMethod.PUT)
	public Long deactiveCompany(@PathVariable long id)
	{
		return service.deactivateCompany(id);
	}
	
	@RequestMapping(value = "/companies/activate/{id}",method = RequestMethod.PUT)
	public Long activeCompany(@PathVariable long id)
	{
		return service.activateCompany(id);
	}
}
