package com.knowarth.rimscp.web.controller.company.report;

import java.io.IOException;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowarth.rimscp.service.user.IDocumentService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.DocumentRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class DocumentController {

	@Autowired
	private IDocumentService service;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);


	/**
	 * @param file
	 * @param document
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/documents", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> createDocument(@RequestParam(required = false) MultipartFile file,
			@NotNull @RequestParam("document") String document) throws IOException, JsonParseException {
		 LOGGER.debug("...in .Document Controller start.");
		 LOGGER.info("-----------in document controller----------");
		try {
			DocumentRequest documentRequest = new ObjectMapper().readValue(document, DocumentRequest.class);
			Long id = service.createDocument(documentRequest, file);
			ResponseMessage response = WebUtil.getResponse("Success", id, "Document");
			LOGGER.info("-----------in document controller----- try ---return----------");
			return new ResponseEntity<ResponseMessage>(response, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.error("EROR "+ e.getMessage());
			LOGGER.info("-----------in document controller----- catch ---return----------");
			return null; 
		}
		
	}

	/**
	 * get the all documents
	 * 
	 * @return
	 */
	@GetMapping(value = "/documents")
	public List<DocumentRequest> getDocuments() {
		return service.getDocuments();
	}

	/**
	 * get the all in-active documents
	 * 
	 * @return
	 */
	@GetMapping(value = "/documents/inActive")
	public List<DocumentRequest> getInActiveDocuments() {
		return service.getInActiveDocuments();
	}
	
	/**
	 * Download Document
	 * 
	 * @param companyName
	 * @param fileName
	 * @return
	 */
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@GetMapping(value = "/document-management/company/{companyName}/document/{fileName}")
	public @ResponseBody ResponseEntity<byte[]> downloadDocument(@PathVariable String companyName,
			@PathVariable String fileName) {
		return service.getDocuments(companyName, fileName);
	}
	
	@RequestMapping(value = "/document-management/document-review", method = { RequestMethod.POST, RequestMethod.PUT })
	public void sendDocumentForReview(@RequestBody DocumentRequest documentRequest)
	{
		service.sendDocumentForReview(documentRequest);
	}

	@RequestMapping(value = "/document-management/document-reviewed", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void documentReviewed(@RequestParam("documentRequestObj")String documentRequestObj,@RequestParam (required = false) String documentComment) throws IOException {
		DocumentRequest documentRequest = new ObjectMapper().readValue(documentRequestObj, DocumentRequest.class);
		service.documentReviewed(documentRequest,documentComment);
	}

	@RequestMapping(value = "/document-management/document-approved", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void documentApproved(@RequestParam("documentRequestObj")String  documentRequestObj,@RequestParam (required = false) String documentComment ) throws IOException {
		DocumentRequest documentRequest = new ObjectMapper().readValue(documentRequestObj, DocumentRequest.class);
		service.documentApproved(documentRequest,documentComment);
		
	}
	
	@RequestMapping(value = "/document-management/document-rejected-onreview", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void documentRejectedOnReview(@RequestBody DocumentRequest documentRequest ) {
		service.documentRejectedOnReview(documentRequest);
	}
	
	@RequestMapping(value = "/document-management/document-rejected-onapprove", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void documentRejectedOnApprove(@RequestBody DocumentRequest documentRequest) {
		service.documentRejectedOnApprove(documentRequest);
	}
	
	@RequestMapping(value = "/document-management/document-reedit", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void documentReEdit(@RequestBody DocumentRequest documentRequest) {
		service.documentReEdit(documentRequest);
	}
	
	@GetMapping(value = "/document-management/{companyName}/document/{fileName}")
	public @ResponseBody ResponseEntity<byte[]> viewDocument(@PathVariable String fileName,@PathVariable String companyName)
	{
		LOGGER.debug("Fetching Document for {}", companyName);
		return service.getDocumentForDispaly(fileName,companyName);
	}
}
