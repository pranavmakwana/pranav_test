package com.knowarth.rimscp.web.controller.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.service.company.INotificationService;
import com.knowarth.rimscp.web.model.company.NotificationRequest;

@RestController
public class NotificationController { 
	@Autowired
	private INotificationService service;

	@GetMapping(value = "/notification/latest-activities")
	public List<NotificationRequest> getNotificationForLatestActivities() {
		return service.getNotificationForLatestActivities();
	}
	
	@GetMapping(value = "/notification")
	public List<NotificationRequest> getNotification() {
		return service.getNotification();
	}
}
