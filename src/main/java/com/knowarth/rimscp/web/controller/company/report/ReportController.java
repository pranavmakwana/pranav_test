package com.knowarth.rimscp.web.controller.company.report;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowarth.rimscp.service.company.report.IReportService;
import com.knowarth.rimscp.web.model.report.DocumentRequest;
import com.knowarth.rimscp.web.model.report.ReportMasterRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class ReportController {
	@Autowired
	private IReportService service;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

	/*
	//	get all active report
	*/
	
	@GetMapping("/report-management/reports")
	public List<ReportMasterRequest> getReports() {
		return service.getReports();
	}

	/*
	//	get all in active report
	*/
	
	@GetMapping("/report-management/inactive-reports")
	public List<ReportMasterRequest> getInActiveReports() {
		return service.getInActiveReports();
	}
	
	@RequestMapping(value = "/report-management/reports", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> createReports(@RequestBody ReportMasterRequest reportMasterRequest) {
		LOGGER.debug("...in .Report Controller start.");
		try {
			return service.createReport(reportMasterRequest);
		} catch (Exception e) {
			LOGGER.error("EROR " + e.getMessage());
			return null;
		}
	}

	@RequestMapping(value = "/report-management/reports-review", method = { RequestMethod.POST, RequestMethod.PUT })
	public void sendReportForReview(@RequestBody ReportMasterRequest reportMasterRequest) {
		service.sendReportForReview(reportMasterRequest);
	}

	@RequestMapping(value = "/report-management/reports-reviewed", method = { RequestMethod.POST, RequestMethod.PUT })
	public void reportReviewed(@RequestParam("reportRequestObj") String reportRequestObj,
			@RequestParam(required = false) String reportComment) throws IOException {
		ReportMasterRequest reportMasterRequest = new ObjectMapper().readValue(reportRequestObj,
				ReportMasterRequest.class);
		service.reportReviewed(reportMasterRequest, reportComment);
	}

	@RequestMapping(value = "/report-management/reports-approved", method = { RequestMethod.POST, RequestMethod.PUT })
	public void reportApproved(@RequestParam("reportRequestObj") String reportRequestObj,
			@RequestParam(required = false) String reportComment) throws IOException {
		ReportMasterRequest reportMasterRequest = new ObjectMapper().readValue(reportRequestObj,
				ReportMasterRequest.class);
		service.reportApproved(reportMasterRequest, reportComment);
	}
	
	@DeleteMapping(value = "/report-management/report/{id}")
	public Long deleteReport(@PathVariable long id)
	{
		return service.deleteReport(id);
	}
	
	//
		
	@RequestMapping(value = "/report-management/report-rejected-onreview", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void reportRejectedOnReview(@RequestBody ReportMasterRequest reportMasterRequest) {
		service.reportRejectedOnReview(reportMasterRequest);
	}
	
	@RequestMapping(value = "/report-management/report-rejected-onapprove", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void reportRejectedOnApprove(@RequestBody ReportMasterRequest reportMasterRequest) {
		service.reportRejectedOnApprove(reportMasterRequest);
	}
	
	@RequestMapping(value = "/report-management/document-reedit", method = { RequestMethod.POST,
			RequestMethod.PUT })
	public void reportReEdit(@RequestBody ReportMasterRequest reportMasterRequest) {
		
		service.reportReEdit(reportMasterRequest);
	}
}
