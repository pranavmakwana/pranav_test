package com.knowarth.rimscp.web.controller.company.aws;
import java.util.List; 
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.knowarth.rimscp.service.cache.CacheService;
import com.knowarth.rimscp.service.company.aws.EC2Service;
import com.knowarth.rimscp.service.company.aws.RDSService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.aws.EC2Detail;
import com.knowarth.rimscp.web.model.aws.RDSDetail;
import com.knowarth.rimscp.web.model.user.JwtResponse;
/**
 * AWS Resource Getting EC2 & AWS Details
 *
 * @author arkan.malik
 */
@RestController
public class AWSResourceController {
	 
	private static final Logger LOGGER = Logger.getLogger(AWSResourceController.class);
	@Autowired
	private EC2Service ec2Service;
	
	@Autowired
	private RDSService rdsService;
	
	@Autowired
	private CacheService cacheService;
	/**
	 * List Of AWS Instances
	 */
//	@Cacheable("ecdetail")
	@GetMapping(value = "/aws/ec-detail")
	public List<EC2Detail> getAwsDetails() {
		LOGGER.info("Fetching EC2 details");
		String id = WebUtil.getLoggedInUserId().toString();
		return ec2Service.getAwsDetails(id);
	}
	/**
	 * List of RDS Instances
	 */
//	@Cacheable("rdsdetail")
	@GetMapping(value = "/aws/rds-detail")
	public List<RDSDetail> getRdsDetails() {
		LOGGER.info("Fetching RDS details");
		String id = WebUtil.getLoggedInUserId().toString();
	 	return rdsService.getRDSDetails(id);
	}
	/**
	 * Clear AWS Cache
	 *
	 * TODO: Add button at Front end with clear and refresh
	 *	NOT USED
	 */
	@GetMapping("/aws/clear-cache")
	public ResponseEntity<?> clearAWSCache() {
		LOGGER.info("******************* \n EC2  Cache Called.");
		cacheService.clearAWSDetails();
		return ResponseEntity.ok(new JwtResponse("Cached..."));
	}
	
	/**
	 * Cache EC2 Details & Get Return List of EC2
	 * @return List Ec2 Detail
	 */ 
	@GetMapping("/ec/clear-cache")
	public ResponseEntity<List<EC2Detail>> clearEC2Cache() {
		LOGGER.info("******************* \n EC2 Cache Called.");
		cacheService.clearEc2Details();
		LOGGER.debug("******************* EC2 Cache Called.");
		String id = WebUtil.getLoggedInUserId().toString();
		List<EC2Detail> ec2List = ec2Service.getAwsDetails(id);
		return new ResponseEntity<List<EC2Detail>>(ec2List,HttpStatus.OK); 
	} 
	
	/**
	 * Cache RDS Details & Get Return List of RDS
	 * @return List RDS Detail
	 */
	@GetMapping("/rds/clear-cache")
	public ResponseEntity<List<RDSDetail>> clearRDSCache() {
		LOGGER.info("******************* \n RDS Cache Called.");
		cacheService.clearRDSDetails();
		LOGGER.debug("******************* RDS Cache Called.");
		String id = WebUtil.getLoggedInUserId().toString();
		List<RDSDetail> rdsList = rdsService.getRDSDetails(id);
		return new ResponseEntity<List<RDSDetail>>(rdsList,HttpStatus.OK); 
	}
}