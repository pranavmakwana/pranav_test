package com.knowarth.rimscp.web.controller;

import com.knowarth.rimscp.service.TimeStatusService;
import com.knowarth.rimscp.web.model.TimeStatusData;
import com.knowarth.rimscp.web.model.TimeStatusRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TimeStatusController {

    @Autowired
    private TimeStatusService timeStatusService;

    @GetMapping("/timestatuses")
    public @ResponseBody List<TimeStatusRequest> getAllTimeStatus(){
        return timeStatusService.getAllTimeStatus();
    } 

    @GetMapping("/timestatus/{id}")
    public @ResponseBody TimeStatusRequest getTimeStatus(@PathVariable Long id){
        return timeStatusService.getTimeStatus(id);
    }

    @RequestMapping(value = "/timestatus", method = { RequestMethod.POST, RequestMethod.PUT })
    public ResponseEntity<ResponseMessage> createTimeStatus(@RequestBody TimeStatusRequest timeStatusRequest){
        return timeStatusService.createTimeStatus(timeStatusRequest);
    }

    @DeleteMapping("/timestatus/{id}")
    public ResponseEntity<ResponseMessage> deleteTimeStatus(@PathVariable Long id){
        return timeStatusService.deleteTimeStatus(id);
    }

    @GetMapping("timestatus/monthly")
    public @ResponseBody TimeStatusData getMonthlyData(){
        return timeStatusService.getMonthlyData();
    }

    @GetMapping("timestatus/threemonthly")
    public @ResponseBody TimeStatusData getThreeMonthlyData(){
        return timeStatusService.getThreeMonthlyData();
    }

    //@GetMapping("timestatus/example")
    //public List<Object[]> example(){
    //    return timeStatusService.example();
    //}

    @GetMapping("timestatus/yearly")
    public @ResponseBody TimeStatusData getYearlyData(){
        return timeStatusService.getYearlyData();
    }

}
