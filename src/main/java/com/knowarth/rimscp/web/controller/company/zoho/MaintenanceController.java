package com.knowarth.rimscp.web.controller.company.zoho;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.knowarth.rimscp.service.zoho.IMaintenanceService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.company.MaintenanceRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class MaintenanceController {

	@Autowired
	private IMaintenanceService service;

	private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceController.class);

	/**
	 * @param maintenance
	 * @return ResponseMessage
	 */
	@RequestMapping(value = "/maintenance", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> createMaintenance(@RequestBody MaintenanceRequest maintenanceRequest) {
		LOGGER.debug("...in Maintenance Controller...");
		try {
			Long id = service.createMaintenance(maintenanceRequest);
			ResponseMessage response = WebUtil.getResponse("Success", id, "Maintenance");
			return new ResponseEntity<>(response, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.error("EROR " + e.getMessage());
			return null;
		}
	}

	/**
	 * Maintenance List
	 */
	@GetMapping(value = "/maintenance")
	public List<MaintenanceRequest> getMaintenance() {
		service.getMaintenanceFromTicketSummery();
		return service.getMaintenance();
	}
	
	@DeleteMapping(path = "/maintenance/{id}")
	public Long deleteMaintenance(@PathVariable long id)
	{
		return service.deleteMaintenance(id);
	}
}
