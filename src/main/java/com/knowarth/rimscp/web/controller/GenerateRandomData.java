package com.knowarth.rimscp.web.controller;

import com.knowarth.rimscp.db.model.TicketSummary;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.TicketSummaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@RestController
public class GenerateRandomData {
    @Autowired
    private TicketSummaryRepository ticketSummaryRepository;

    @Autowired
    private CompanyRespository companyRespository;

    @GetMapping("/generate")
    public void generateRandomData(){
        String[] escalation = {"L1","L2","L3","L4"};
        String[] severity = {"High","Low","Medium","Moderate"};
        String[] module = {"Maintenance", "Issue", "Incident"};
        Random random = new Random();
        for (int i = 0; i <1000 ; i++) {
            TicketSummary ticketSummary = new TicketSummary();
            ticketSummary.setCreatedDate(between(new Date(1577836800000l),new Date(1609459199000l)));
            ticketSummary.setSeverity(severity[random.nextInt(4)]);
            ticketSummary.setClosed(random.nextBoolean());
            ticketSummary.setEscalationLevel(escalation[random.nextInt(4)]);
            //ticketSummary.setResolutionTime(between(new Date(1577836800000l),new Date(1609459199000l)));
            //ticketSummary.setResponseTime(between(new Date(1577836800000l),new Date(1609459199000l)));
            ticketSummary.setModule(module[random.nextInt(3)]);
            ticketSummary.setCompany(companyRespository.findOneByName("Knowarth Technologies"));
            ticketSummaryRepository.save(ticketSummary);
        }

    }


    public static Date between(Date startInclusive, Date endExclusive) {
        long startMillis = startInclusive.getTime();
        long endMillis = endExclusive.getTime();
        long randomMillisSinceEpoch = ThreadLocalRandom
                .current()
                .nextLong(startMillis, endMillis);

        return new Date(randomMillisSinceEpoch);
    }
}
