package com.knowarth.rimscp.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.dialect.function.TemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.ReportActivityRepository;
import com.knowarth.rimscp.repository.ReportSectionRepository;
import com.knowarth.rimscp.repository.TemplateRepository;
import com.knowarth.rimscp.service.DeleteService;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.web.model.DependantData;


@RestController
public class DeleteController {

	@Autowired
	private DeleteService deleteService;
	
	@Autowired 
	private TemplateRepository templateRepo;
	
	@Autowired
	private ReportActivityRepository activityRepo;
	
	@Autowired
	private ReportSectionRepository sectionRepo;
	
	@Autowired
	private CompanyRespository companyRepo;
	
	@GetMapping(value="/deleteEligibility/{entity}/{id}")
	public List<String> deleteEligibility(@PathVariable String entity,@PathVariable Long id ) {
		
		List<DependantData> count = new ArrayList<>();
		DependantData dependantData;
		
		if(entity.equals(AppConstants.ENTITY_TEMPLATE)) {
			dependantData = templateRepo.getDependReportDataCountByJPAQuery(id);
			count.add(dependantData);
			dependantData = templateRepo.getDependSectionDataCountByJPAQuery(id);
			count.add(dependantData);
			dependantData = templateRepo.getDependActivityDataCountByJPAQuery(id);
			count.add(dependantData);
			return returnString(count,entity,id);
		}else if(entity.equals(AppConstants.ENTITY_ACTIVITY)) {
			dependantData = activityRepo.getDependReportDataCountByJPAQuery(id);
			count.add(dependantData);
			return returnString(count, entity,id);
		}else if(entity.equals(AppConstants.ENTITY_SECTION)) {
			dependantData =	sectionRepo.getDependActivityDataCountByJPAQuery(id);
			count.add(dependantData);
			dependantData =	sectionRepo.getDependReportDataCountByJPAQuery(id);
			count.add(dependantData);
			return returnString(count, entity,id);
		}else if(entity.equals(AppConstants.ENTITY_COMPANY)) {
			dependantData =	companyRepo.getDependUserDataCountByJPAQuery(id);
			count.add(dependantData);
			dependantData =  companyRepo.getDependDocumentDataCountByJPAQuery(id);
			count.add(dependantData);
			dependantData = companyRepo.getDependReportCountByJPAQuery(id);
			count.add(dependantData);
			return returnString(count, entity,id);
		}else  {
			return null;
		}
	}
	
	public List<String> returnString(List<DependantData> count, String entity, Long id) {
		List<String> returnList = new ArrayList<>();
		String result ="There are ";
		Boolean status = false;
		for(DependantData data: count) {
			if(data.getCount() > 0) {
				status = true;
				result = result + data.getCount() + " " + data.getType() +", ";
			}
		}
		if(status) {
			result = result + "associated with this " + entity + ", please delete them first.";
			returnList.add(result);
			String deletePath = "/delete/" + entity + "/" + id;
			returnList.add(deletePath);
		}else {
			delete(entity, id);
			returnList.add("NoAssociate");
		}
		return returnList;
	}
	
	@DeleteMapping(path = "/delete/{entity}/{id}")
	public Long delete(@PathVariable String entity,@PathVariable Long id) {
		if(entity.equals(AppConstants.ENTITY_DOCUMENT)) {
			return deleteService.deleteDocument(id);
		}else if(entity.equals(AppConstants.ENTITY_REPORT)) {
			return deleteService.deleteReport(id);
		}else if(entity.equals(AppConstants.ENTITY_USER)) {
			return deleteService.deleteUser(id);
		}else if(entity.equals(AppConstants.ENTITY_ACTIVITY)) {
			return deleteService.deleteActivity(id);
		}else if(entity.equals(AppConstants.ENTITY_SECTION)) {
			return deleteService.deleteSection(id);
		}else if(entity.equals(AppConstants.ENTITY_TEMPLATE)) {
			return deleteService.deleteTemplate(id);
		}else if(entity.equals(AppConstants.ENTITY_COMPANY)) {
			return deleteService.deleteCompany(id);
		}else {
			return null;
		}
	}
	
}
