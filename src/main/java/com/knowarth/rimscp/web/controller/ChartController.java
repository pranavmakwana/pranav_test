package com.knowarth.rimscp.web.controller;


import com.knowarth.rimscp.service.ChartService;
import com.knowarth.rimscp.web.model.ChartDataV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ChartController {

    @Autowired
    private ChartService chartService;

    @GetMapping("dashboard/data/average-time/one-week")
    public Map<String,String> getweeklyDataForResponseTime(){
    	 
        return chartService.getWeeklyAvgResolutionTime();
    }
    
    @GetMapping("dashboard/data/average-time/one-month")
    public Map<String,String> getMonthlyDataForResponseTime(){
    	 
        return chartService.getMonthlyAvgResolutionTime();
    }
    @GetMapping("dashboard/data/average-time/three-month")
    public Map<String,String> getThreeMonthlyDataForResponseTime(){
    	 
        return chartService.getThreeMonthlyAvgResolutionTime();
    }
    @GetMapping("dashboard/data/average-time/one-year")
    public Map<String,String> getYearlyDataForResponseTime(){
    	 
        return chartService.getYearlyAvgResolutionTime();
    }
    
    
    
    @GetMapping("chart/data/severity/one-week")
    public ChartDataV2 getWeeklyChartDataBySeverity(){

        return chartService.getWeekChartDataBySeverity();
    }

    @GetMapping("chart/data/severity/one-month")
    public ChartDataV2 getMonthlyChartDataBySeverity(){

        return chartService.getMonthlyChartDataBySeverity();
    }
    
    @GetMapping("chart/data/severity/three-month")
    public ChartDataV2 getThreeMonthChartDataBySeverity(){

        return chartService.getThreeMonthChartDataBySeverity();
    }
    
    @GetMapping("chart/data/escalation/one-week")
    public ChartDataV2 getWeeklyChartDataByEscalation(){

        return chartService.getWeeklyChartDataByEscalation();
    }
    
    @GetMapping("chart/data/escalation/one-month")
    public ChartDataV2 getMonthlyChartDataByEscalation(){

        return chartService.getMonthlyChartDataByEscalation();
    }
    
    @GetMapping("chart/data/escalation/three-month")
    public ChartDataV2 getThreeMonthChartDataByEscalation(){

        return chartService.getThreeMonthChartDataByEscalation();
    }

    @GetMapping("dashboard/data/ticket/one-week")
    public Map<String, Integer> getGroupedTickedDataForOneWeeK(){
        return chartService.getGroupedTickedDataForOneWeek();
    }

    @GetMapping("dashboard/data/ticket/one-month")
    public Map<String, Integer> getGroupedTickedDataForOneMonth(){
        return chartService.getGroupedTickedDataForOneMonth();
    }
    @GetMapping("dashboard/data/ticket/three-month")
    public Map<String, Integer> getGroupedTickedDataForThreeMonth(){
        return chartService.getGroupedTickedDataForThreeMonth();
    }
    @GetMapping("dashboard/data/ticket/one-year")
    public Map<String, Integer> getGroupedTickedDataForOneYear(){
        return chartService.getGroupedTickedDataForOneYear();
    }
}
