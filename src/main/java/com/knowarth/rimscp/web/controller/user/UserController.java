package com.knowarth.rimscp.web.controller.user;

import java.util.List;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.service.user.IUserService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.user.UserRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

/**
 * User Resource
 *
 * @author vinit.prajapati
 */
@RestController
public class UserController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private IUserService service;

	@Autowired
	ServletContext servletcontext;

	/**
	 * @param userRequest
	 * @return ResponseMessage
	 */
	@RequestMapping(value = "/user-management/users", method = {RequestMethod.POST, RequestMethod.PUT})
	public ResponseEntity<ResponseMessage> createUser(@RequestBody UserRequest userRequest)
	{
		Long id = service.createUser(userRequest);
		ResponseMessage response = WebUtil.getResponse("Success", id, "User");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	/**
	 * List Of active Users
	 */
	@GetMapping(value = "/user-management/users")
	public List<UserRequest> getUsers()
	{
		return service.getUsers();
	}
	
	/**
	 * List Of inactive Users
	 */
	@GetMapping(value = "/user-management/inactive-users")
	public List<UserRequest> getInactiveUsers()
	{
		return service.getInactiveUsers();
	}
	/**
	 * upload User profile
	 **/
	@RequestMapping(value = "/user-management/users/profile", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> uploadUserProfileImage(@RequestParam("file") MultipartFile file)
	{
		String fileName = service.setUserProfile(file);
		LOGGER.debug("file =name" + fileName);
		ResponseMessage response = new ResponseMessage("Success", fileName + " File Uploaded Successfully.");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	/**
	 * Retrieves requested user profile details
	 *
	 * @return UserRequest
	 */
	@GetMapping(value = "/user-management/users/profile")
	public ResponseEntity<UserRequest> myProfile()
	{
		return new ResponseEntity<>(service.getMyProfile(), HttpStatus.OK);
	}

	/**
	 * * Get User profile
	 *
	 * @param
	 * @return String
	 */
	@GetMapping(value = "/user-management/users/profile/{logo}")
	public @ResponseBody byte[] viewUserProfile(@PathVariable String logo)
	{
		LOGGER.debug("Fetching profile for {}", logo);
		return service.getUserProfile(logo);
	}

	@GetMapping(value = "/user-management/mycompany/logo")
	public byte[] getMyCompanyLogo()
	{
		return service.getMyCompanyLogo();
	}

	/**
	 * returns roles as List<String>
	 *
	 * @return List of Role
	 */
	@GetMapping(value = "/user-management/roles")
	public ResponseEntity<List<String>> getRoles()
	{
		return new ResponseEntity<>(service.getRoles(), HttpStatus.OK);
	}

	/**
	 * List<String> Of Users where role = Technician/Lead/Manager
	 */
	@GetMapping(value = "/user-management/roles/owners")
	public ResponseEntity<List<String>> getOwners()
	{
		return new ResponseEntity<>(service.getOwners(), HttpStatus.OK);
	}

	@GetMapping(value = "/user-management/users/uploadImg")
	public String uploadImg()
	{
		return "Upload Image Successfull";
	}
	
	@DeleteMapping(path = "/user-management/users/{id}")
	public Long deleteSection(@PathVariable long id)
	{
		return service.deleteUser(id);
	}
	
	@RequestMapping(value = "/user-management/users/deactivate/{id}",method = RequestMethod.PUT)
	public Long deactiveCompany(@PathVariable long id)
	{
		return service.deactivateUser(id);
	}
	
	@RequestMapping(value = "/user-management/users/activate/{id}",method = RequestMethod.PUT)
	public Long activeCompany(@PathVariable long id)
	{
		return service.activateUser(id);
	}
}
