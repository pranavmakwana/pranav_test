package com.knowarth.rimscp.web.controller.company.report;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.service.company.report.ReportDetailService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.ReportDetailHandler;
import com.knowarth.rimscp.web.model.report.ReportDetailRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class ReportDetailController {
	
	private static final Logger LOGGER = Logger.getLogger(ReportDetailController.class);
	@Autowired 
	private ReportDetailService reportDetailService;

	@Autowired
	private ReportDetailRepository reportDetailRepository;

	@GetMapping("/report-management/report-details/{reportId}")
	public List<ReportDetailRequest> getReportDetailRequestByReportId(@PathVariable(required = true) Long reportId) {
		
		return reportDetailService.getAllReportDetailRequestByReportId(reportId);
	}

	@GetMapping("/report-management/report-details/jpa/{id}")
	public List<ReportDetailHandler> getReportDetailHandler(@PathVariable Long id) {
		return reportDetailRepository.getReportDetailByJPAQuery(id);
	}

	@RequestMapping(value = "/report-management/report-details", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> updateReportDetailRequest(
			@RequestBody List<ReportDetailRequest> reportDetailRequests) {
		return reportDetailService.updateReportDeatilRequest(reportDetailRequests);
	}

	@RequestMapping(value = "/report-management/attachment", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> uploadAttachment(@RequestParam("file") @Nullable MultipartFile multipartFile,
			 @RequestParam("reportId") Long reportId, @RequestParam("activityId") @Nullable Long activityId) {
		
		LOGGER.info("**** Activity  "+ reportId + "File " + multipartFile + "Activy ID " + activityId);
	  	String fileName = reportDetailService.uploadFile(reportId, multipartFile,activityId);
	  	LOGGER.info("**** Activity FILE NAME "+ fileName);  
		ResponseMessage response = new ResponseMessage("Success", fileName);
		return new ResponseEntity<ResponseMessage>(response, HttpStatus.CREATED); 
	}

	/**
	 *  
	 * @param companyName
	 * @param fileName
	 * @return
	 */

	@RequestMapping(value = "/report-management/company/{reportDetailId}/report/{fileName}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> viewUserProfile(@PathVariable Long reportDetailId,
			@PathVariable String fileName) {
		return reportDetailService.getReport(reportDetailId, fileName);
	} 
}
