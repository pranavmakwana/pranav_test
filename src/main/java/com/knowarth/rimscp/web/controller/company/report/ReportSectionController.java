package com.knowarth.rimscp.web.controller.company.report;

import java.util.List;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.service.company.report.IReportSectionService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.ReportActivityRequest;
import com.knowarth.rimscp.web.model.report.ReportSectionDropdownRequest;
import com.knowarth.rimscp.web.model.report.ReportSectionRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@RestController
public class ReportSectionController {
	@Autowired
	private IReportSectionService reportSectionService;

	/**
	 * Create / Update || ReportSection
	 * 
	 * @param ReportSectionRequest
	 * @return
	 */
	@RequestMapping(value = "/report-management/report/sections", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<ResponseMessage> createReportSection(@RequestBody ReportSectionRequest reportSectionRequest) {
		Long id = reportSectionService.createReportSection(reportSectionRequest);
		ResponseMessage response = WebUtil.getResponse("Success", id, "ReportSection");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	/**
	 * returns ReportSection name as List<String>
	 * 
	 * @return List of ReportSection name
	 */
	@RequestMapping(value = "/report-management/report/sections/byname", method = { RequestMethod.GET })
	public ResponseEntity<List<String>> getReportSections() {
		return new ResponseEntity<>(reportSectionService.getReportSections(), HttpStatus.OK);
	}

	/**
	 * returns ReportSection as List<ReportSection>
	 *
	 * @return List of ReportSection
	 */
	@GetMapping(value = "/report-management/report/sections")
	public List<ReportSectionRequest> getAllActiveReportSections() {
		return reportSectionService.getAllReportSection();
	}
	
	 
	/**
	 * returns ReportSection as List<ReportSection> IN Active Sections
	 *
	 * @return List of ReportSection
	 */
	@GetMapping(value = "/report-management/report/inactive-sections")
	public List<ReportSectionRequest> getAllInActiveReportSections() {
		return reportSectionService.getInActiveReportSection();
	}
	 
	@RequestMapping(value = "/report-management/report/sections/deactivate/{id}",method = RequestMethod.PUT)
	public Long deactiveSection(@PathVariable long id)
	{
		return reportSectionService.deactivateSection(id);
	}
	
	@RequestMapping(value = "/report-management/report/sections/activate/{id}",method = RequestMethod.PUT)
	public Long activeSection(@PathVariable long id)
	{
		return reportSectionService.activateSection(id);
	}
	
	@GetMapping(value = "/report-management/report/sections/fordropdown")
	public List<ReportSectionDropdownRequest> getReportSectionsForDropdown() {
		return reportSectionService.getReportSectionForDropdown();
	}

	@DeleteMapping(path = "/report-management/report/section/{id}")
	public Long deleteSection(@PathVariable long id)
	{
		return reportSectionService.deleteSection(id);
	}
	
	
}
