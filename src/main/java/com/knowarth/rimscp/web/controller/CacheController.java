package com.knowarth.rimscp.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.knowarth.rimscp.service.cache.CacheService;

@RestController
public class CacheController {
	
	@Autowired
	private CacheService cacheService;
	
	@GetMapping(value = "/clear-cache" )
	public void cacheClear() {
		
//		cacheService.clearTicketDetails();
//		cacheService.clearEc2Details();
//		cacheService.clearRDSDetails();
		
	}
}
