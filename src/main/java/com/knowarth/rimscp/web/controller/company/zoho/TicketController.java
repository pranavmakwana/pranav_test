package com.knowarth.rimscp.web.controller.company.zoho;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.cache.CacheService;
import com.knowarth.rimscp.service.company.report.ITicketService;
import com.knowarth.rimscp.service.zoho.ZohoService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.zoho.TicketCreate;
import com.knowarth.rimscp.web.model.zoho.api.GetTicketDataRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;
import com.knowarth.rimscp.web.model.zoho.api.TicketRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@RestController
public class TicketController {
	
	@Autowired
	private ITicketService service;
	
	@Autowired
	private ZohoService zohoService;
	
	@Autowired
	private CacheService cacheService;

	@Autowired
	private static UserRepository userRepository;
	
	private static final Logger LOGGER = Logger.getLogger(TicketController.class);
 
	/**
	 * @return list of tickets
	 */
	
	
	@GetMapping(value = "/tickets")
	public List<TicketRequest> getTickets() {
		LOGGER.debug("..............in Ticket_getData_Controller start....................");
		String id = WebUtil.getLoggedInUserId().toString();
		return service.getTickets(id);
	}
 
	@GetMapping(value= "/ticket-count")
	public List<Long> getTicketCount(){
		String id = WebUtil.getLoggedInUserId().toString();
		return service.getTicketCount(id);
	}
	
//	 @Cacheable("TicketDataDetail")
	@GetMapping(value = "/ticket-Defaultfields")
	public GetTicketDataRequest getTicketData() {
		return service.getTicketData();
	}

	@RequestMapping(value = "/tickets", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessage> createTicket(
			@RequestPart(value = "file", required = false) MultipartFile file,
			@NotNull @RequestPart("ticketRequest") String ticketRequest)
			throws JsonMappingException, JsonProcessingException, IOException {
			LOGGER.debug("..............in Ticket_Create_Controller start....................");
			try {
				TicketCreate ticketObj = new ObjectMapper().readValue(ticketRequest, TicketCreate.class);
				service.createTicket(ticketObj, file); 
				return new ResponseEntity<ResponseMessage>(HttpStatus.CREATED);	
			}
			catch(Exception e) {
				LOGGER.error("..............Error occured on ticket controller .............." + e.getMessage(), e);
				return null;
		}
	}

	/**
	 * Clear ZOHO TICKET Cache
	 * 
	 * TODO: Add button at Front end with clear and refresh
	 * 
	 */
	@GetMapping("/tickets/clear-cache")
	public  ResponseEntity<List<TicketRequest>> clearTicketCache() {
		LOGGER.debug("******************* Ticket Cache Called.");

		cacheService.clearTicketDetails();
		LOGGER.debug("******************** ticket cache is working");
		String id = WebUtil.getLoggedInUserId().toString();
		List<TicketRequest> ticketRequest = service.getTickets(id);
		LOGGER.debug("******************* Refreshed ticket data");
		return new ResponseEntity<>(ticketRequest,HttpStatus.OK);
	}

	@GetMapping("/ticket-count/clear-cache")
	public List<Long> clearTicketCount(){
		cacheService.clearTicketCount();
		String id = WebUtil.getLoggedInUserId().toString();
		return service.getTicketCount(id);
	}
	
	@GetMapping("/test")
	public void test() {
		zohoService.updateZohoTickets();
	}

	@GetMapping("asynch/test")
	public void example() throws InterruptedException {
		System.out.println("Staring asynch test");
		zohoService.example();
		System.out.println("Stopping asynch test");
	}
}
