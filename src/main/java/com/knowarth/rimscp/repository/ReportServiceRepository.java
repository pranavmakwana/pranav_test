package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.knowarth.rimscp.db.model.ReportSection;
import com.knowarth.rimscp.db.model.User;

public interface ReportServiceRepository extends JpaRepository<ReportSection, Long> {

	public ReportSection findOneByName(final String reportSection);

	
}
