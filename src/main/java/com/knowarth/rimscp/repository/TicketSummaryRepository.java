package com.knowarth.rimscp.repository;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.db.model.TicketSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


@Repository
public interface TicketSummaryRepository extends JpaRepository<TicketSummary, Long> {

	@Query(value = "select ts.zohoId from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where ts.closed = 0 and ts.status ='Open' and ts.companyid = :id and c.is_active = 1",
			nativeQuery = true)
	public Set<Long> getAllOpenTicketIds(@Param("id") Long id);

	public TicketSummary findByZohoId(final Long zohoid);
	 
	public TicketSummary findByZohoIdAndCompany(Long zohoid, Company company);
	
							// 	AVG. Resolution - AVG.Response    QUERY PART
	
	@Query(value=" select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25))  "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where ts.closed = 1 and ts.createddate >= CURDATE() - INTERVAL 7 day and c.is_active = 1",nativeQuery = true)
	public List<Object[]> getWeeklyAvgResolutionTime();
	
	
	@Query(value=" select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25)) "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where closed = 1 and ts.createddate >= CURDATE() - INTERVAL 30 day and c.is_active = 1",nativeQuery = true)
	public List<Object[]> getMonthlyAvgResolutionTime();
	
	@Query(value="select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25))  "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id  " +
			"where closed = 1 and ts.createddate >= CURDATE() - INTERVAL 90 day and c.is_active = 1",nativeQuery = true)
	public List<Object[]> getThreeMonthlyAvgResolutionTime();
	
	@Query(value="select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25))  "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where closed = 1 and ts.createddate >= CURDATE() - INTERVAL 365 day and  c.is_active = 1",nativeQuery = true)
	public List<Object[]> getYearlyAvgResolutionTime();

	// 	AVG. Resolution - AVG.Response For customer   QUERY PART

	@Query(value=" select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25))  "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where ts.closed = 1 and ts.createddate >= CURDATE() - INTERVAL 7 day and c.is_active = 1 " +
			"and companyid = :companyId and project_key = :projectKey",nativeQuery = true)
	public List<Object[]> getWeeklyAvgResolutionTimeForCustomer(@Param("companyId")Long companyId,@Param("projectKey")String projectKey);


	@Query(value=" select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25)) "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where closed = 1 and ts.createddate >= CURDATE() - INTERVAL 30 day and c.is_active = 1 " +
			"and companyid = :companyId and project_key = :projectKey",nativeQuery = true)
	public List<Object[]> getMonthlyAvgResolutionTimeForCustomer(@Param("companyId")Long companyId,@Param("projectKey")String projectKey);

	@Query(value="select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25))  "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id  " +
			"where closed = 1 and ts.createddate >= CURDATE() - INTERVAL 90 day and c.is_active = 1 " +
			"and companyid = :companyId and project_key = :projectKey",nativeQuery = true)
	public List<Object[]> getThreeMonthlyAvgResolutionTimeForCustomer(@Param("companyId")Long companyId,@Param("projectKey")String projectKey);

	@Query(value="select cast(sec_to_time(avg(resolutiontime)) as char(25)),cast(sec_to_time(avg(responsetime)) as char(25))  "
			+ " from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where closed = 1 and ts.createddate >= CURDATE() - INTERVAL 365 day and  c.is_active = 1 " +
			"and companyid = :companyId and project_key = :projectKey",nativeQuery = true)
	public List<Object[]> getYearlyAvgResolutionTimeForCustomer(@Param("companyId")Long companyId,@Param("projectKey")String projectKey);
	
	@Query(value= "SELECT count(ts.id) FROM ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where ts.closed = 1 and  c.is_active = 1",nativeQuery = true)
	public Long getCloseCount();
	
	@Query(value= "SELECT count(ts.id)  FROM ticketsummaries ts inner join companies c on ts.companyid = c.id" +
			" where ts.closed = 1 && companyid = :id and c.is_active = 1",nativeQuery = true)
	public Long getCloseCountFromCompany(@Param("id") Long id);
	
	
	//----------------------------Escalation and Severity Data for any role except customer------------------
				// SEVERITY PART
	
	@Query(value="select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 7 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  7 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  6 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity UNION ALL   \n" +
			" select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 6 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  6 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  5 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity UNION ALL   \n" +
			" select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 5 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  5 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  4 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity UNION ALL   \n" +
			" select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 4 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  4 DAY \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  3 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity UNION ALL   \n" +
			" select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 3 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  3 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  2 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity UNION ALL   \n" +
			" select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 2 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  2 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  1 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity UNION ALL   \n" +
			" select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 1 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  1 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  0 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by severity",nativeQuery =  true)
	public List<Object[]> getWeeklyChartDataBySeverity(@Param("projectKeys") List<String> projectKeys);
	
	 
	@Query(value = "select severity , count(severity) as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  22 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  28 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  22 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by severity UNION ALL\n" +
			"select severity , count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  21 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  21 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  15 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by severity\n" +
			"UNION ALL\n" +
			"select severity , count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY , '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  14 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  14 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  8 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by severity\n" +
			"UNION ALL\n" +
			"select severity, count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  7 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  7 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by severity", nativeQuery = true )
	public List<Object[]> getMonthlyChartDataBySeverity(@Param("projectKeys") List<String> projectKeys);

	@Query(value="select severity, count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  90 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  76 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  90 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  76 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by severity  UNION ALL\n" +
			"select severity, count(severity)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  75 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  61 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  75 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  61 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  60 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  46 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  60 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  46 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  45 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  31 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  45 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  31 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by severity  UNION ALL\n" +
			"select severity, count(severity)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  30 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  16 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  30 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  16 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by severity  UNION ALL\n" +
			"select severity, count(severity)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  15 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by severity",nativeQuery = true)
	public List<Object[]> getThreeMonthChartDataBySeverity(@Param("projectKeys") List<String> projectKeys);
	
	 
								// ESCALATION PART
	
	@Query(value = "select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 7 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  7 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  6 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel UNION ALL   \n" +
			" select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 6 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  6 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  5 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel UNION ALL   \n" +
			" select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 5 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  5 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  4 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel UNION ALL   \n" +
			" select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 4 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  4 DAY \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  3 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel UNION ALL   \n" +
			" select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 3 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  3 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  2 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel UNION ALL   \n" +
			" select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 2 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  2 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  1 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel UNION ALL   \n" +
			" select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 1 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			" where ts.createddate >= CURDATE() - INTERVAL  1 DAY   \n" +
			" AND ts.createddate  < CURDATE() - INTERVAL  0 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			" group by escalationlevel",nativeQuery = true)
	public List<Object[]>  getWeeklyChartDataByEscalationLevel(@Param("projectKeys") List<String> projectKeys);
	
	  
	@Query(value = "select escalationlevel , count(escalationlevel) as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  22 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  28 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  22 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel , count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  21 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  21 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  15 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel\n" +
			"UNION ALL\n" +
			"select escalationlevel , count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY , '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  14 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  14 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  8 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel\n" +
			"UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  7 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  7 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY AND c.is_active =1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel", nativeQuery = true )
	public List<Object[]> getMonthlyChartDataByEscalationLevel(@Param("projectKeys") List<String> projectKeys);

	@Query(value="select escalationlevel, count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  90 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  76 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  90 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  76 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel  UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  75 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  61 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  75 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  61 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  60 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  46 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  60 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  46 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  45 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  31 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  45 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  31 DAY  AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel  UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  30 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  16 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  30 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  16 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel  UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  15 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY AND c.is_active = 1 and project_key in (:projectKeys)\n" +
			"group by escalationlevel",nativeQuery = true)
	public List<Object[]> getThreeMonthChartDataByEscalationLevel(@Param("projectKeys") List<String> projectKeys);


//----------------------------Escalation and Severity Data customer------------------


	@Query(value="select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 7 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  7 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  6 DAY AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 6 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  6 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  5 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 5 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  5 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  4 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 4 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  4 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  3 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 3 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  3 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  2 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 2 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  2 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  1 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity) as data, DATE_FORMAT(curdate()-interval 1 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  1 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity",nativeQuery =  true)
	public List<Object[]> getWeeklyChartDataBySeverityForCustomer(@Param("companyId") Long companyId, @Param("projectKey") String projectKey);


	@Query(value = "select severity , count(severity) as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  22 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  28 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  22 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity , count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  21 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  21 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  15 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity\n" +
			"UNION ALL\n" +
			"select severity , count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY , '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  14 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  14 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  8 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity\n" +
			"UNION ALL\n" +
			"select severity, count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  7 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  7 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity", nativeQuery = true )
	public List<Object[]> getMonthlyChartDataBySeverityForCustomer(@Param("companyId") Long companyId, @Param("projectKey") String projectKey);


	@Query(value="select severity, count(severity) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  90 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  76 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  90 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  76 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity  UNION ALL\n" +
			"select severity, count(severity)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  75 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  61 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  75 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  61 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  60 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  46 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  60 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  46 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity UNION ALL\n" +
			"select severity, count(severity)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  45 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  31 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  45 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  31 DAY  and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity  UNION ALL\n" +
			"select severity, count(severity)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  30 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  16 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  30 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  16 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity  UNION ALL\n" +
			"select severity, count(severity)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  15 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by severity",nativeQuery = true)
	public List<Object[]> getThreeMonthChartDataBySeverityForCustomer(@Param("companyId") Long companyId, @Param("projectKey") String projectKey);


	// ESCALATION PART

	@Query(value="select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 7 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  7 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  6 DAY AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 6 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  6 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  5 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 5 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  5 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  4 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 4 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  4 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  3 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 3 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  3 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  2 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 2 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  2 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  1 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, DATE_FORMAT(curdate()-interval 1 day, '%e/%b') from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  1 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY  AND c.is_active = 1 and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel",nativeQuery =  true)
	public List<Object[]> getWeeklyChartDataByEscalationLevelForCustomer(@Param("companyId") Long companyId, @Param("projectKey") String projectKey);


	@Query(value = "select escalationlevel , count(escalationlevel) as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  22 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  28 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  22 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel , count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  21 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  21 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  15 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel\n" +
			"UNION ALL\n" +
			"select escalationlevel , count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  28 DAY , '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  14 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  14 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  8 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel\n" +
			"UNION ALL\n" +
			"select escalationlevel, count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  7 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  7 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel", nativeQuery = true )
	public List<Object[]> getMonthlyChartDataByEscalationLevelForCustomer(@Param("companyId") Long companyId, @Param("projectKey") String projectKey);


	@Query(value="select escalationlevel, count(escalationlevel) as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  90 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  76 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  90 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  76 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel  UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  75 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  61 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  75 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  61 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  60 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  46 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  60 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  46 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  45 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  31 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  45 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  31 DAY  and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel  UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data, concat(DATE_FORMAT(CURDATE() - INTERVAL  30 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  16 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  30 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  16 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel  UNION ALL\n" +
			"select escalationlevel, count(escalationlevel)as data,concat(DATE_FORMAT(CURDATE() - INTERVAL  15 DAY, '%e/%b'),'-',DATE_FORMAT(CURDATE() - INTERVAL  0 DAY, '%e/%b'))\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"where ts.createddate >= CURDATE() - INTERVAL  15 DAY\n" +
			"  AND ts.createddate  < CURDATE() - INTERVAL  0 DAY and project_key = :projectKey AND ts.companyid = :companyId\n" +
			"group by escalationlevel",nativeQuery = true)
	public List<Object[]> getThreeMonthChartDataByEscalationLevelForCustomer(@Param("companyId") Long companyId, @Param("projectKey") String projectKey);


	//------------------------------------------ticketData-------------------------
	//get grouped ticket data for 1 week
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 7 DAY ) AND CURDATE( ) and c.is_active =1 \n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForOneWeek();

	//get grouped ticket data for 1 month
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 30 DAY ) AND CURDATE( ) and c.is_active =1\n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForOneMonth();

	//get grouped ticket data for 3 month
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 90 DAY ) AND CURDATE( ) and c.is_active =1\n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForThreeMonth();

	//get grouped ticket data for 1 Year
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 365 DAY ) AND CURDATE( ) and c.is_active =1\n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForOneYear();

//-----------------------------------for customers------------------------------------
	//get grouped ticket data for 1 week
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 7 DAY ) AND CURDATE( ) and companyid = :companyId and c.is_active =1 \n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForOneWeekCustomer(@Param("companyId") Long companyId);

	//get grouped ticket data for 1 month
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 30 DAY ) AND CURDATE( ) and companyid = :companyId and c.is_active =1\n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForOneMonthCustomer(@Param("companyId") Long companyId);

	//get grouped ticket data for 3 month
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 90 DAY ) AND CURDATE( ) and companyid = :companyId and c.is_active =1\n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForThreeMonthCustomer(@Param("companyId") Long companyId);

	//get grouped ticket data for 1 Year
	@Query(value = "SELECT module, COUNT(module)\n" +
			"from ticketsummaries ts inner join companies c on ts.companyid = c.id\n" +
			"WHERE ts.createddate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 365 DAY ) AND CURDATE( ) and companyid = :companyId and c.is_active =1\n" +
			"Group by module", nativeQuery = true)
	public List<Object[]> getGroupedTickedDataForOneYearCustomer(@Param("companyId") Long companyId);
	
	//get BugId list For Maintenance
	@Query(value = "SELECT zohoid from ticketsummaries ts inner join companies c on ts.companyid = c.id  " +
			"WHERE module = 'Maintenance' and closed = 0 and c.is_active =1", nativeQuery = true)
	public List<String> getBugIdForMaintenance();

	@Query(value = "SELECT * from ticketsummaries ts inner join companies c on ts.companyid = c.id" +
			" where status = 'Closed' and module = 'Maintenance' and c.is_active =1 and project_key in(:projectKeys) ", nativeQuery = true)
	public List<TicketSummary> getClosedTicket(@Param("projectKey") List<String> projectKeys);
	
	@Query(value = "SELECT * from ticketsummaries ts inner join companies c on ts.companyid = c.id" +
			" where status = 'Open'  and module = 'Maintenance' and c.is_active =1 and project_key in(:projectKeys)", nativeQuery = true)
	public List<TicketSummary> getOpenTicket(@Param("projectKeys") List<String> projectKeys);

	@Query(value = "SELECT * from ticketsummaries ts inner join companies c on ts.companyid = c.id" +
			" where status = 'Closed' and module = 'Maintenance' and c.is_active =1 and project_key=:projectKey ", nativeQuery = true)
	public List<TicketSummary> getClosedTicketForCustomer(@Param("projectKey") String projectKey);

	@Query(value = "SELECT * from ticketsummaries ts inner join companies c on ts.companyid = c.id" +
			" where status = 'Open'  and module = 'Maintenance' and c.is_active =1 and project_key=:projectKey", nativeQuery = true)
	public List<TicketSummary> getOpenTicketForCustomer(@Param("projectKey") String projectKey);

	
	@Query(value = "select id from ticketsummaries where companyid = :id", nativeQuery = true)
	public List<Long> getTicketSummariesByCompanyId(@Param("id") Long id);


	@Query("select ts from ticketsummaries ts where ts.projectKey in (:projectKey) and ts.zohoId in (:existingTickets)")
	public List<TicketSummary> isExistingTicketOfProjectKey(@Param("projectKey") String projectKey,@Param("existingTickets") List<Long> existingTickets );


	@Query(value = "select ts.zohoId from ticketsummaries ts inner join companies c on ts.companyid = c.id " +
			"where ts.closed = 1 and ts.status ='Closed' and ts.companyid = :id and c.is_active = 1",
			nativeQuery = true)
	public Set<Long> getAllClosedTicketIds(@Param("id") Long id);

}
