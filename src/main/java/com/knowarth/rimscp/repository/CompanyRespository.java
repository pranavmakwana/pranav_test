package com.knowarth.rimscp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.web.model.DependantData;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRespository extends JpaRepository<Company, Long> {

	public Company findOneByName(final String name);

	@Query(value = "select projectkey from companies where projectkey is not null and is_active = 1", nativeQuery = true)
	public List<String> getAllProjectKeys();

	public Company findByProjectKey(final String projectKey);

	@Query(value = "SELECT * FROM companies where is_active = 0", nativeQuery= true)
	public List<Company> getAllInActiveCompany();
	
	@Query(value = "SELECT * FROM companies where is_active = 1", nativeQuery= true)
	public List<Company> getAllActiveCompany();
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(u.id),'Users') "
			+ "from companies c left join users u on c.id = u.company.id where c.id = :id")
	public DependantData getDependUserDataCountByJPAQuery(@Param("id") Long id);
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(d.id),'Documents') "
			+ "from companies c left join documents d on c.id = d.company.id where c.id = :id")
	public DependantData getDependDocumentDataCountByJPAQuery(@Param("id") Long id);
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(r.id),'Report') "
			+ "from companies c left join reports r on c.id = r.company.id where c.id = :id")
	public DependantData getDependReportCountByJPAQuery(@Param("id") Long id);
}
