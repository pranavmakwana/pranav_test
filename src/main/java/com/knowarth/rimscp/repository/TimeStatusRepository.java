package com.knowarth.rimscp.repository;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.db.model.TimeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeStatusRepository extends JpaRepository<TimeStatus, Long> {

    List<TimeStatus> findAll();

    List<TimeStatus> findAllByCompany(Company currentUserCompany);

    TimeStatus findOneById(Long id);

    @Query(value = "select s.* from timestatus s inner join companies c on s.companyid = c.id where c.is_active = 1", nativeQuery = true)
    List<TimeStatus> findAllByActiveCompany();
    
    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month like :month% and year like %:currentYear and is_active = 1")
    List<Object[]> getMonthlyData(@Param("month") String month, @Param("currentYear") String currentYear);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month like :month% and year like %:previousYear and is_active = 1")
    List<Object[]> getMonthlyData2(@Param("month") String month, @Param("previousYear") String currentYear);


    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus  where month in(:month1,:month2,:month3) and year in(:currentYear) and is_active = 1")
    List<Object[]> getThreeMonthlyData(@Param("month1") String month1, @Param("month2") String month2,
                                       @Param("month3") String month3, @Param("currentYear") String currentYear);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month in(:month1,:month2) and year in(:currentYear) and is_active = 1 " +
            "or month in(:month3) and year in(:previousYear)")
    List<Object[]> getThreeMonthlyData2(@Param("month1") String month1, @Param("month2") String month2,
                                       @Param("currentYear") String currentYear, @Param("month3") String month3,
                                        @Param("previousYear") String previousYear);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month in(:month1) and year in(:currentYear) " +
            "  or month in(:month2,:month3) and year in(:previousYear) and is_active = 1")
    List<Object[]> getThreeMonthlyData3(@Param("month1") String month1, @Param("currentYear") String currentYear,
                                        @Param("month2") String month2, @Param("month3") String month3,
                                        @Param("previousYear") String previousYear);
//
//    @Query("SELECT " +
//            "    AVG(upTime),  " +
//            "    AVG(downTime), " +
//            "    AVG(maintenance) " +
//            "FROM timestatus where month in(:list)")
//   List<Object[]> example(@Param("list") List<String> list);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month in(:list1) and year in(:previousYear) " +
            "  or month in(:list2) and year in(:currentYear) and is_active = 1")
    List<Object[]> getYearlyData(@Param("list1") List<String> previousYearMonths, @Param("previousYear") String previousYear,
                                 @Param("list2") List<String> currentYearMonths,@Param("currentYear") String currentYear);
//===========================for customer===============================
    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month like :month% and year like %:currentYear and company.id = :id")
    List<Object[]> getMonthlyDataForCustomer(@Param("month") String month, @Param("currentYear") String currentYear,@Param("id") Long companyId );

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month like :month% and year like %:previousYear and company.id = :id")
    List<Object[]> getMonthlyData2ForCustomer(@Param("month") String month, @Param("previousYear") String currentYear,@Param("id") Long companyId );

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus  where month in(:month1,:month2,:month3) and year in(:currentYear) and company.id = :id")
    List<Object[]> getThreeMonthlyDataForCustomer(@Param("month1") String month1, @Param("month2") String month2,
                                       @Param("month3") String month3, @Param("currentYear") String currentYear,@Param("id") Long companyId);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month in(:month1,:month2) and year in(:currentYear) " +
            "or month in(:month3) and year in(:previousYear) and company.id = :id")
    List<Object[]> getThreeMonthlyData2ForCustomer(@Param("month1") String month1, @Param("month2") String month2,
                                        @Param("currentYear") String currentYear, @Param("month3") String month3,
                                        @Param("previousYear") String previousYear,@Param("id") Long companyId);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month in(:month1) and year in(:currentYear) " +
            "  or month in(:month2,:month3) and year in(:previousYear) and company.id = :id")
    List<Object[]> getThreeMonthlyData3ForCustomer(@Param("month1") String month1, @Param("currentYear") String currentYear,
                                        @Param("month2") String month2, @Param("month3") String month3,
                                        @Param("previousYear") String previousYear,@Param("id") Long companyId);

    @Query("SELECT " +
            "    AVG(upTime),  " +
            "    AVG(downTime), " +
            "    AVG(maintenance) " +
            "FROM " +
            "    timestatus where month in(:list1) and year in(:previousYear) " +
            "  or month in(:list2) and year in(:currentYear) and company.id = :id")
    List<Object[]> getYearlyDataForCustomer(@Param("list1") List<String> previousYearMonths, @Param("previousYear") String previousYear,
                                 @Param("list2") List<String> currentYearMonths,@Param("currentYear") String currentYear,@Param("id") Long companyId);
    
    @Query(value= "SELECT id FROM timestatus where companyid = :id", nativeQuery = true)
	public List<Long> getTimeStatusbyCompanyId(@Param("id") Long id );
}
