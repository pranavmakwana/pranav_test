package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.knowarth.rimscp.db.model.ReportSection;
import com.knowarth.rimscp.db.model.Template;
import com.knowarth.rimscp.web.model.DependantData;

public interface ReportSectionRepository extends JpaRepository<ReportSection, Long> {
	
	@Query(value = "SELECT * FROM reportsections where is_active = 1", nativeQuery = true)
	public List<ReportSection> getActiveSections();
	
	@Query(value = "SELECT * FROM reportsections where is_active = 0", nativeQuery = true)
	public List<ReportSection> getInActiveSections();
	
	public ReportSection findOneByName(final String reportSection);

	public List<ReportSection> findAllByOrderByTemplateAsc();

	public ReportSection findOneByNameAndTemplate(final String reportSectionName, final Template template);

	@Query(value= "SELECT id FROM reportsections where templateid = :id", nativeQuery = true)
	public List<Long> getSectionbyTemplateId(@Param("id") Long id );
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(ra.id),'Activity') "
			+ 	"from reportsections rs left join reportactivities ra on rs.id = ra.reportSection.id where rs.id = :id")
	public DependantData getDependActivityDataCountByJPAQuery(@Param("id") Long id);
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(rd.id),'Report') "
			+ 	"from reportsections rs left join reportactivities ra on rs.id = ra.reportSection.id"
			+ 	" left join reportdetails rd on ra.id = rd.reportActivities.id where rs.id = :id")
	public DependantData getDependReportDataCountByJPAQuery(@Param("id") Long id);
}
