package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.knowarth.rimscp.db.model.Template;
import com.knowarth.rimscp.web.model.DependantData;

@Repository
public interface TemplateRepository extends JpaRepository<Template, Long> {

	
	public Template findOneByName(final String templateName);
	
	@Query(value = "SELECT * FROM templates where is_active = 1", nativeQuery = true)
	public List<Template> getActiveTemplates();
	
	@Query(value = "SELECT * FROM templates where is_active = 0", nativeQuery = true)
	public List<Template> getInActiveTemplates();
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(r.id),'Report')" 
			+ 	"from templates t left join reports r on t.id = r.template.id where t.id = :id ")
	public DependantData getDependReportDataCountByJPAQuery(@Param("id") Long id);
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(rs.id),'Section')"
			+ 	"from templates t left join reportsections rs on t.id = rs.template.id where t.id = :id ")
	public DependantData getDependSectionDataCountByJPAQuery(@Param("id") Long id);
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(ra.id),'Activity')"
			+ 	"from templates t left join reportsections rs on t.id = rs.template.id "
			+ 	"left join reportactivities ra on ra.id = rs.id where t.id = :id")
	public DependantData getDependActivityDataCountByJPAQuery(@Param("id") Long id);
}
