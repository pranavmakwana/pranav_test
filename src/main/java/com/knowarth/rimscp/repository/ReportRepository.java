package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.knowarth.rimscp.db.model.Report;
import com.knowarth.rimscp.db.model.User;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

	public Report findOneById(final Long id);
	
	

	@Query(value = "SELECT * FROM reports where is_active = 1", nativeQuery = true)
	public List<Report> getActiveReport(); 
	
	@Query(value = "SELECT * FROM reports where is_active = 0", nativeQuery = true)
	public List<Report> getInActiveReport();
	
	@Query(value= "SELECT id FROM reports where companyid = :id", nativeQuery = true)
	public List<Long> getReportbyCompanyId(@Param("id") Long id );
	
	@Query(value= "SELECT id FROM reports where templateid = :id", nativeQuery = true)
	public List<Long> getReportbyTemplateId(@Param("id") Long id );
	
}
