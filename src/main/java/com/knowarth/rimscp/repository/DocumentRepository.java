package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.knowarth.rimscp.db.model.Document;
import com.knowarth.rimscp.db.model.User;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

	public Document findOneByCompanyId(Long companyid);

	public Document findOneByFileName(String filename);

	public Document findOneByDocumentName(String documentName);

	@Query(value = "SELECT * FROM documents where is_active = 1", nativeQuery = true)
	public List<Document> getActiveDocument(); 
	
	@Query(value = "SELECT * FROM documents where is_active = 0", nativeQuery = true)
	public List<Document> getInActiveDocument(); 
	
	@Query(value = "SELECT id FROM documents where companyid = :id", nativeQuery = true)
	public List<Long> getDocumentbyCompanyId(@Param("id") Long id );
	
	}
