package com.knowarth.rimscp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.knowarth.rimscp.db.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	public Role findOneByName(final String name);
}
