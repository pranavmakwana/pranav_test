
package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.knowarth.rimscp.db.model.ReportActivity;
import com.knowarth.rimscp.db.model.ReportSection;
import com.knowarth.rimscp.web.model.DependantData;

@Repository
public interface ReportActivityRepository extends JpaRepository<ReportActivity, Long> {

	 
	public ReportActivity findOneByName(final String name);
	
	@Query(value = "select id from reportactivities where name= :name and reportsectionid = :reportsectionid",nativeQuery = true)
	public Long findActivityIdBySectionIdAndName(@Param("reportsectionid") Long reportsectionid,@Param("name") String name);
	
	public ReportActivity findOneByNameAndReportSection(final String activityName , final ReportSection reportSection);
	 
	@Query(value = "SELECT * FROM reportactivities where is_active = 1", nativeQuery = true)
	public List<ReportActivity> getActiveActivity(); 
	
	@Query(value = "SELECT * FROM reportactivities where is_active = 0", nativeQuery = true)
	public List<ReportActivity> getInActiveActivity(); 
	
	@Query(value= "SELECT id FROM reportactivities where reportsectionid = :id", nativeQuery = true)
	public List<Long> getReportActivitybySectionId(@Param("id") Long id );
	
	@Query(value = "select new com.knowarth.rimscp.web.model.DependantData(count(rd.id),'Report') from reportactivities ra "
			+ 	"left join reportdetails rd on ra.id = rd.reportActivities.id where ra.id = :id")
	public DependantData getDependReportDataCountByJPAQuery(@Param("id") Long id);
}
