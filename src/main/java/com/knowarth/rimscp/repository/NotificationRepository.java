package com.knowarth.rimscp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.knowarth.rimscp.db.model.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
	@Query(value = "select * from notification n join roles r on roleid = r.id where status='Available For Approve'  or (status='Available For Review' and r.name in('Manager','Lead'))  or status='Approved' or (status='Reviewed' and r.name in ('Manager','Lead')) ORDER BY n.id DESC LIMIT 15", nativeQuery = true)
	public List<Notification> getLatestActivitiesForManager();

	@Query(value = "select * from  notification n join roles r on roleid = r.id where  (status='Available For Review' and r.name in('Lead','Technician')) or  (status='Reviewed' and r.name in ('Technician')) ORDER BY n.id DESC LIMIT 15", nativeQuery = true)
	public List<Notification> getLatestActivitiesForLead();

	@Query(value = "select * from notification n join roles r on roleid = r.id where status='Available For Approve' or (status='Available For Review' and r.name in('Manager','Lead'))", nativeQuery = true)
	public List<Notification> getNotificationForManager();

	@Query(value = "select * from notification n JOIN roles r on roleid = r.id where status='Available For Review' and r.name in('Technician')", nativeQuery = true)
	public List<Notification> getNotificationForLead();

	@Transactional
	@Modifying
	@Query("update notification n set n.status = :status ,n.completed = :completed ,n.comment= :comment where (n.document= :document and n.type = :type)")
	public void updateStatusOnReview(@Param("status") String status, @Param("completed") Boolean completed,
			@Param("comment") String comment, @Param("document") Long document, @Param("type") String type);

	@Transactional
	@Modifying
	@Query("update notification n set n.status = :status ,n.completed = :completed ,n.comment= :comment where (n.document= :document and n.type = :type and n.status = :previousStatus)")
	public void updateStatusOnApprove(@Param("status") String status, @Param("completed") Boolean completed,
			@Param("comment") String comment, @Param("document") Long document, @Param("type") String type,
			@Param("previousStatus") String previousStatus);

	@Transactional
	@Modifying
	@Query("update notification n set n.completed = :completed where (n.document= :document and n.type = :type and n.status = :previousStatus)")
	public void updateStatusOnComplete(@Param("previousStatus") String previousStatus,
			@Param("completed") Boolean completed, @Param("document") Long document, @Param("type") String type);

	@Transactional
	@Modifying
	@Query("DELETE FROM notification WHERE type = :type and document = :document")
	public void rejectedDocument(@Param("type") String type, @Param("document") Long document);

	@Query(value = "select id from notification where document = :id", nativeQuery = true)
	public List<Long> getNotificationDetailByDocumentId(@Param("id") Long id);
	

	@Query(value = "select id from notification where userid = :id", nativeQuery = true)
	public List<Long> getNotificationDetailByUserId(@Param("id") Long id);
	
	
}
