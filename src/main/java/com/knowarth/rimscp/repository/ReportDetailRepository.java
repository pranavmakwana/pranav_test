package com.knowarth.rimscp.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.knowarth.rimscp.db.model.Report;
import com.knowarth.rimscp.db.model.ReportDetail;
import com.knowarth.rimscp.web.model.report.ReportDetailHandler;
import com.knowarth.rimscp.web.model.report.ReportDetailRequest;

@Repository
public interface ReportDetailRepository extends JpaRepository<ReportDetail, Long> {
	public List<ReportDetail> findAllByOrderByReportActivitiesAsc();
	public List<ReportDetail> findAllByReportOrderByReportActivitiesAsc(final Report report);
	
	

	@Query(value = "select rd.* from reports r inner join templates t on t.id = r.templateid "
			+ "inner join reportsections rs on rs.templateid = t.id "
			+ "inner join reportactivities ra on ra.reportsectionid = rs.id "
			+ "left join reportdetails rd on rd.activityid = ra.id and rd.reportid = r.id "
			+ "where r.id = :id", nativeQuery = true)
	public List<ReportDetail> getReportDetailByQuery(@Param("id") Long id);
	public List<ReportDetail> findAllByReport(@Param("id") Long id);
	public ReportDetail findOneById(Long id);

	
	@Query("select id from reportdetails where reportid = :reportid and activityid = :activityid")
	public Long findOneByReportIdAndActivityId(@Param("reportid") Long reportid,@Param("activityid") Long activityid);
	 
	@Query(value = "select new com.knowarth.rimscp.web.model.report.ReportDetailHandler(rs.name, ra.name, rd.id, rd.attachment, rd.date, "
			+ " rd.frequencyOfActivities, rd.notes, rd.priority,  rd.servers, rd.status, rd.report.id, rd.reportActivities.id) "
			+ " from reports r inner join templates t on t.id = r.template.id "
			+ "            inner join reportsections rs on rs.template.id = t.id "
			+ "            inner join reportactivities ra on ra.reportSection.id = rs.id"
			+ "            left join reportdetails rd on rd.reportActivities.id = ra.id and rd.report.id = r.id "
			+ "  where r.id = :id order by rs.id")
	public List<ReportDetailHandler> getReportDetailByJPAQuery(@Param("id") Long id);
	
	public ReportDetail findOneByAttachment(String attachment);
	
	@Query(value= "SELECT id FROM reportdetails where reportid = :id", nativeQuery = true)
	public List<Long> getReportDetailsbyReportId(@Param("id") Long id );
	
	@Query(value= "SELECT id FROM reportdetails where activityid = :id", nativeQuery = true)
	public List<Long> getReportDetailsbyReportActivityId(@Param("id") Long id );
	
	@Query(value= "SELECT activityid FROM reportdetails where id= :id and reportid= :reportid", nativeQuery = true)
	public Long getActivityIdByReportDetailId(@Param("id") Long id,@Param("reportid") Long reportid);
	
	@Query(value= "SELECT reportid FROM reportdetails where id = :id", nativeQuery = true)
	public Long getReportbyReportDetailsId(@Param("id") Long id );
	
	@Query(value = "delete from reportdetails where reportid = :id", nativeQuery = true)
	public void deleteReportDetailById(@Param("id") Long id);
	
	
}