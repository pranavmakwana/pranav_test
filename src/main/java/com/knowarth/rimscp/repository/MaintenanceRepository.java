package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.knowarth.rimscp.db.model.Maintenance;

@Repository
public interface MaintenanceRepository extends JpaRepository<Maintenance, Long> {

//	@Query("select bug_id from maintenance where id = :id")
//	public String getBugId(final Long id);

	@Query(value = "SELECT bugid FROM maintenance WHERE companyid= :companyid", nativeQuery = true )
	public List<String> getTicketId(@Param("companyid") Long companyid);
	
	@Query(value = "SELECT bugid FROM maintenance where bugid is not null ",nativeQuery = true)
	public List<String> getAllTicketId();
	
	@Query(value = "select status from maintenance where bugid = :id", nativeQuery = true)
	public String getStatusByTicketId(@Param("id") String id);
	
	@Query(value = "select * from maintenance where status = 'Open'", nativeQuery = true)
	public List<Maintenance> getOpenMaintenance();
	
	@Query(value = "select id from maintenance where companyid = :id", nativeQuery = true)
	public List<Long> getMaintenanceByCompanyId(@Param("id") Long id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE maintenance SET status = 'Closed' where bugid = :bugid", nativeQuery = true)
	public void setMaintenanceStatus(@Param("bugid") String bug_id);
}
