package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.knowarth.rimscp.db.model.ForgotPasswordToken;

public interface ForgotPasswordTokenRepository extends JpaRepository<ForgotPasswordToken, Long> {
	public ForgotPasswordToken findOneByToken(final String token);
	

	@Query(value = "select id from forgotpasswordtokens where userid = :id", nativeQuery = true)
	public List<Long> getForgotPasswordByUserId(@Param("id") Long id);
}
