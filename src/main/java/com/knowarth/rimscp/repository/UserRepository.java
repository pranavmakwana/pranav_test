package com.knowarth.rimscp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.knowarth.rimscp.db.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	/**
	 * Fetch user for provided email
	 * 
	 * @param email
	 * @return
	 */
	public User findOneByEmail(String email);
	@Query("select concat(u.firstName, ' ', u.lastName) from users u JOIN u.role r where r.name in ('Manager','Lead','Technician')")
	public List<String> getUsersFullName();

	@Query("select u from users u where concat(u.firstName, ' ', u.lastName) = :fullName")
	public User getUserByFullName(final String fullName);

	@Query(value = "SELECT * FROM users where is_active = 1", nativeQuery = true)
	public List<User> getActiveUser(); 
	
	@Query(value = "SELECT * FROM users where is_active = 0", nativeQuery = true)
	public List<User> getInActiveUser(); 
	
	@Query(value= "SELECT id FROM users where companyid = :id", nativeQuery = true)
	public List<Long> getUsersbyCompanyId(@Param("id") Long id );
	
	@Query(value = "update Users set is_active = false where id = :id", nativeQuery = true)
	public void setUserInActivate(@Param("id") Long id);
}
