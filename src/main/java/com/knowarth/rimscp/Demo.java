package com.knowarth.rimscp;

import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {

    public static void main(String[] args) {
        String month = "SEPTEMBER";

        Month[] monthArray = Month.values();

        //System.out.println(monthArray[0].ordinal());

        List<String> newMonths = new ArrayList<>(3);

        List<String> monthNames = new ArrayList<>();
        monthNames = Arrays.stream(monthArray).map(Enum::name).collect(Collectors.toList());

        int currentMonthIndex = monthNames.indexOf(month);

        if (currentMonthIndex <=2) {
            newMonths.add("JANUARY" );
            newMonths.add("FEBUARY");
            newMonths.add("MARCH");
        } else {
            for (int i = currentMonthIndex; i > (currentMonthIndex -3) ; i--) {
                newMonths.add(monthNames.get(i));
            }
        }

        newMonths.forEach(System.out::println);

    }
}
