package com.knowarth.rimscp.db.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * Documents tables
 */
@Data
@Entity(name = "documents")
@EqualsAndHashCode(callSuper = false)
public class Document extends AuditableEntity 
{
	private static final long serialVersionUID = 1L;

	@Column(name = "documentname")
	private String documentName;

	@Column(name = "documenttype")
	private String documentType;

	private String description;

	private String version;

	@Column(name = "reviewedby")
	private Long reviewedBy;

	@Column(name = "revieweddate")
	private Date reviewedDate;

	@Column(name = "approveddate")
	private Date approvedDate;

	@Column(name = "approvedby")
	private Long approvedBy;

	@ManyToOne
	@JoinColumn(name = "companyid")
	private Company company;

	@Column(name = "filename")
	private String fileName;
	
	private String status;
	
	private Boolean isActive;
}
