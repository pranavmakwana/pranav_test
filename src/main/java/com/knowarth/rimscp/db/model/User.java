package com.knowarth.rimscp.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Users tables
 * 
 * @author vinit.prajapati
 */

@Data
@Entity(name = "users")
@EqualsAndHashCode(callSuper = false)
public class User extends AuditableEntity {
	private static final long serialVersionUID = 1L;

	@Column(name = "firstname")
	private String firstName;

	@Column(name = "lastname")
	private String lastName;

	private String email;

	private String password;

	private String phone;

	private String profile;

	@ManyToOne
	@JoinColumn(name = "roleid")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "companyid")
	private Company company;
	
	private Boolean isActive;
}
