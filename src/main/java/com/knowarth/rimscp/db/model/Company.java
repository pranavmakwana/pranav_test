package com.knowarth.rimscp.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Companies table - Hold customer details
 * 
 * @author vinit.prajapati
 */
@Data
@Entity(name = "companies")
@EqualsAndHashCode(callSuper = false)
public class Company extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	@Column(unique = true)
	private String name;

	private String address;

	@Column(name = "contactno")
	private String contactNo;

	private String website;

	private String logo;

	@Column(name = "shortname")
	private String shortName;

	private String region;

	@Column(name = "clientid")
	private String clientId;

	private String secret;

	@Column(name = "projectkey",unique = true)
	private String projectKey;
	
	private Boolean isActive; 
}
