package com.knowarth.rimscp.db.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "timestatus")
@Table( uniqueConstraints=
@UniqueConstraint(columnNames={"months", "years","companyid"}))
public class TimeStatus extends AuditableEntity{

    @ManyToOne
    @JoinColumn(name = "companyid")
    private Company company;

    @Column(name = "uptime")
    private double upTime;

    @Column(name = "downtime")
    private double downTime;

    private double maintenance;

    @Column(name = "months")
    private String month;

    @Column(name = "years")
    private String year;
    
    private Boolean isActive;
}
