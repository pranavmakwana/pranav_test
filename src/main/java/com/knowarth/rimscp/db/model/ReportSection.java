package com.knowarth.rimscp.db.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "reportsections")
@EqualsAndHashCode(callSuper = false)
public class ReportSection extends AuditableEntity
{
	private static final long serialVersionUID = 1L;

	private String name; 
	@ManyToOne
	@JoinColumn(name = "templateid")
	private Template template;
	private Boolean isActive;
	
}
