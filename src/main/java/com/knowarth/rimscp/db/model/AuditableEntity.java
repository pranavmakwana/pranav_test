/**
 * 
 */
package com.knowarth.rimscp.db.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author parth.pithadia
 */

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = false)
public abstract class AuditableEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "createdby")
	private Long createdBy;

	@Column(name = "createddate")
	private Date createdDate;

	@Column(name = "updatedby")
	private Long updatedBy;

	@Column(name = "updateddate")
	private Date updatedDate;
}
