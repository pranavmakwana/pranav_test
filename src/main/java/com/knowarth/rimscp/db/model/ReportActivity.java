package com.knowarth.rimscp.db.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "reportactivities")
@EqualsAndHashCode(callSuper = false)
public class ReportActivity extends AuditableEntity
{
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "reportsectionid")
	private ReportSection reportSection;
}
