package com.knowarth.rimscp.db.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "notification")
@EqualsAndHashCode(callSuper = false)
public class Notification extends BaseEntity 
{

	private static final long serialVersionUID = 1L;

	private String type;
	
	private Long  document;
	
	
	@Column(name="requesteddate")
	private Date requestedDate;
	
	@ManyToOne
	@JoinColumn(name = "userid")
	private User user;

	@ManyToOne
	@JoinColumn(name = "roleid")

	private Role role;

	private String text;

	private String status;

	private String comment;

	private Boolean completed;
	
	
}
