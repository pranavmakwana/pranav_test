package com.knowarth.rimscp.db.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Forgot Password Token
 * 
 * @author vinit.prajapati
 */
@Data
@Entity(name = "forgotpasswordtokens")
@EqualsAndHashCode(callSuper = false)
public class ForgotPasswordToken extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "userid")
	private User user;

	private String token;

	@Column(name = "expirytime")
	private Date expiryTime;
}
