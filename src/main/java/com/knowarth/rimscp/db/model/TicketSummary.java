package com.knowarth.rimscp.db.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Entity(name = "ticketsummaries")
@EqualsAndHashCode(callSuper = false)
public class TicketSummary extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String ticketKey;

	private String title;

	private String severity;

	private String module;

	private String projectKey;

	@ManyToOne
	@JoinColumn(name = "companyid")
	private Company company;

	@Column(name = "escalationlevel")
	private String escalationLevel;

	private String status;

	@Column(name = "createddate")
	private Date createdDate;

	@Column(name = "lastmodifiedtime")
	private Date lastModifiedTime;

	@Column(name = "responsetime")
	private Long responseTime;

	@Column(name = "resolutiontime")
	private Long resolutionTime;

	@Column(name = "zohoid")
	private Long zohoId;

	private Boolean closed;
}
