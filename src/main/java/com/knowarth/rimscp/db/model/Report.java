package com.knowarth.rimscp.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "reports")
@EqualsAndHashCode(callSuper = false)
public class Report extends AuditableEntity
{
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "companyid")
	private Company company;
	
	private String description;
	
	private String reportName;

	private String status;
	
	private Boolean isActive;
	
	@ManyToOne
	@JoinColumn(name = "templateid")
	private Template template;
	
	@Column(name = "reviewedby")
	private Long reviewedBy;

	@Column(name = "revieweddate")
	private Date reviewedDate;

	@Column(name = "approveddate")
	private Date approvedDate;

	@Column(name = "approvedby")
	private Long approvedBy;
}
