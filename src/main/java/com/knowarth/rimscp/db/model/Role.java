package com.knowarth.rimscp.db.model;

import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Role table
 * 
 * @author vinit.prajapati
 */
@Data
@Entity(name = "roles")
@EqualsAndHashCode(callSuper = false)
public class Role extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	private String name;
}
