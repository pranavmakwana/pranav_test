package com.knowarth.rimscp.db.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "maintenance")
@EqualsAndHashCode(callSuper = false)
public class Maintenance extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "companyid")
	private Company company; 
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "deploymentdate")

	private LocalDate deploymentDate;
	
	@Column(name = "servername")
	private String serverName;

	private String description;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	@Column(name = "starttime")
	private String startTime;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	@Column(name = "endtime")
	private String endTime;

	@ManyToOne
	@JoinColumn(name = "username")
	private User user;

	private String status;
	@Column(name = "bugid")
	private String bugId;
	private String ticketId;
}
