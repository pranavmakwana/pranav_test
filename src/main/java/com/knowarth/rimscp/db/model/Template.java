package com.knowarth.rimscp.db.model;

import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "templates")
@EqualsAndHashCode(callSuper = false)
public class Template extends AuditableEntity {
	private static final long serialVersionUID = 1L;

	private String name;

	private String description;
	
	private Boolean isActive;
}
