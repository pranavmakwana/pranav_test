package com.knowarth.rimscp.db.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Entity(name = "reportdetails")
@EqualsAndHashCode(callSuper = false)
public class ReportDetail extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "reportid")
	private Report report;

	@ManyToOne
	@JoinColumn(name = "activityid")
	private ReportActivity reportActivities;

	private String servers;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date date;

	@Column(name = "frquencyofactivities")
	private String frequencyOfActivities;

	private String priority;
 
	private String status;

	private String notes;

	private String attachment;

	private Boolean isActive;
}
