package com.knowarth.rimscp.service;

import com.knowarth.rimscp.web.model.TimeStatusRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ITimeStatusService {

    List<TimeStatusRequest> getAllTimeStatus();

    TimeStatusRequest getTimeStatus(Long id);

    ResponseEntity<ResponseMessage> createTimeStatus(TimeStatusRequest timeStatusRequest);

    ResponseEntity<ResponseMessage> deleteTimeStatus(Long id);
}
