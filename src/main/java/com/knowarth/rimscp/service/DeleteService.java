package com.knowarth.rimscp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knowarth.rimscp.db.model.Maintenance;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.DocumentRepository;
import com.knowarth.rimscp.repository.ForgotPasswordTokenRepository;
import com.knowarth.rimscp.repository.MaintenanceRepository;
import com.knowarth.rimscp.repository.NotificationRepository;
import com.knowarth.rimscp.repository.ReportActivityRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.ReportSectionRepository;
import com.knowarth.rimscp.repository.TemplateRepository;
import com.knowarth.rimscp.repository.TicketSummaryRepository;
import com.knowarth.rimscp.repository.TimeStatusRepository;
import com.knowarth.rimscp.repository.UserRepository;

@Service
public class DeleteService {

	@Autowired
	private ReportDetailRepository reportDetailRepo;
	
	@Autowired
	private ReportRepository reportRepo;
	
	@Autowired
	private DocumentRepository documentRepo;
	
	@Autowired
	private NotificationRepository notificationRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ForgotPasswordTokenRepository forgotPasswordRepo;
	
	@Autowired
	private ReportActivityRepository activityRepo;
	
	@Autowired
	private ReportSectionRepository sectionRepo;
	
	@Autowired
	private TemplateRepository templateRepo;
	
	@Autowired
	private TicketSummaryRepository ticketSummaryRepo;
	
	@Autowired
	private MaintenanceRepository maintenanceRepo;
	
	@Autowired
	private TimeStatusRepository timeStatusRepo;
	
	@Autowired
	private CompanyRespository companyRepo;
	
	public Long deleteReport(Long id) {
		Long deleteId = id;
		List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportId(id);
		for(Long reportDetailId: reportDetailsId) {
			Long returnId = deleteReportDetail(reportDetailId);
		}
		reportRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteReportDetail(Long id) {
		Long deleteId = id;
		reportDetailRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteUser(Long id) {
		Long deleteId = id;
		List<Long> notificationsId = notificationRepo.getNotificationDetailByUserId(id);
		for(Long notificationId: notificationsId) {
			Long returnId = deleteNotification(notificationId);
		}
		List<Long> forgetPassworsId = forgotPasswordRepo.getForgotPasswordByUserId(id);
		for(Long forgetPassworId: forgetPassworsId) {
			Long returnId = deleteForgotPasswordTokens(forgetPassworId);
		}
		userRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteCompany(Long id) {
		Long deleteId = id;
		List<Long> ticketSummariesId = ticketSummaryRepo.getTicketSummariesByCompanyId(id);
		for(Long ticketSummaryId: ticketSummariesId) {
			Long returnId = deleteTicketSummary(ticketSummaryId);
		}
		List<Long> maintenancesId = maintenanceRepo.getMaintenanceByCompanyId(id);
		for(Long maintenanceId: maintenancesId) {
			Long returnId = deleteMaintenance(maintenanceId);
		}
		List<Long> timeStatussId = timeStatusRepo.getTimeStatusbyCompanyId(id);
		for(Long timeStatusId: timeStatussId) {
			Long returnId = deleteTimeStatus(timeStatusId);
		}
		List<Long> documentsId = documentRepo.getDocumentbyCompanyId(id);
		for(Long documentId: documentsId) {
			Long returnId = deleteDocument(documentId);
		}
		List<Long> reportsId = reportRepo.getReportbyCompanyId(id);
		for(Long reportId: reportsId) {
			Long returnId = deleteReport(reportId);
		}
		List<Long> usersId = userRepo.getUsersbyCompanyId(id);
		for(Long userId: usersId) {
			Long returnId = deleteUser(userId);
		}
		companyRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteDocument(Long id) {
		Long deleteId = id;
		List<Long> notificationsId = notificationRepo.getNotificationDetailByDocumentId(id);
		for(Long notificationId: notificationsId) {
			Long returnId = deleteNotification(notificationId);
		}
		documentRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteSection(Long id) {
		Long deleteId = id;
		List<Long> activitiesId = activityRepo.getReportActivitybySectionId(id);
		for(Long activityId: activitiesId) {
			Long returnId = deleteActivity(activityId);
		}
		sectionRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteTemplate(Long id) {
		Long deleteId = id;
		List<Long> reportsId = reportRepo.getReportbyTemplateId(id);
		for(Long reportId: reportsId) {
			Long returnId = deleteReport(reportId);
		}
		List<Long> sectionsId = sectionRepo.getSectionbyTemplateId(id);
		for(Long sectionId: sectionsId) {
			Long returnId = deleteSection(sectionId);
		}
		templateRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteActivity(Long id) {
		Long deleteId = id;
		List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportActivityId(id);
		for(Long reportDetailId: reportDetailsId) {
			Long returnId = deleteReportDetail(reportDetailId);
		}
		activityRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteTimeStatus(Long id) {
		Long deleteId = id;
		timeStatusRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteMaintenance(Long id) {
		Long deleteId = id;
		maintenanceRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteTicketSummary(Long id) {
		Long deleteId = id;
		ticketSummaryRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteNotification(Long id) {
		Long deleteId = id;
		notificationRepo.deleteById(id);
		return deleteId;
	}
	
	public Long deleteForgotPasswordTokens(Long id) {
		Long deleteId = id;
		forgotPasswordRepo.deleteById(id);
		return deleteId;
	}
}
