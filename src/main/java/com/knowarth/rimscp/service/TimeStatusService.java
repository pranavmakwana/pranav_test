package com.knowarth.rimscp.service;

import com.knowarth.rimscp.db.model.TimeStatus;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.TimeStatusRepository;
import com.knowarth.rimscp.service.user.UserService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.TimeStatusData;
import com.knowarth.rimscp.web.model.TimeStatusRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TimeStatusService implements ITimeStatusService {

    @Autowired
    private TimeStatusRepository timeStatusRepository;

    @Autowired
    private CompanyRespository companyRespository;

    @Autowired
    private UserService userService;

    @Override
    public List<TimeStatusRequest> getAllTimeStatus() {
        List<TimeStatus> timeStatuses = null;
        if (userService.isCurrentUserCustomer()) {
        	if(userService.getCurrentUserCompany().getIsActive()) {
        		timeStatuses = timeStatusRepository.findAllByCompany(userService.getCurrentUserCompany());        		
        	}
        } else {
            timeStatuses = timeStatusRepository.findAllByActiveCompany();
        }
        List<TimeStatusRequest> timeStatusRequests = new ArrayList<>();
        timeStatuses.forEach((timeStatus) -> {
                    TimeStatusRequest timeStatusRequest = getTimeStatusRequest(timeStatus);
                    timeStatusRequests.add(timeStatusRequest);
                }
        );
        return timeStatusRequests;
    }

    private TimeStatusRequest getTimeStatusRequest(TimeStatus timeStatus) {
        TimeStatusRequest timeStatusRequest = new TimeStatusRequest();
        timeStatusRequest.setId(timeStatus.getId());
        timeStatusRequest.setCompanyName(timeStatus.getCompany().getName());
        timeStatusRequest.setDownTime(timeStatus.getDownTime());
        timeStatusRequest.setUpTime(timeStatus.getUpTime());
        timeStatusRequest.setMaintenance(timeStatus.getMaintenance());
        timeStatusRequest.setMonth(timeStatus.getMonth());
        timeStatusRequest.setYear(timeStatus.getYear());
        return timeStatusRequest;
    }

    @Override
    public TimeStatusRequest getTimeStatus(Long id) {
    	if(companyRespository.getOne(timeStatusRepository.getOne(id).getCompany().getId()).getIsActive())
    	{
    		TimeStatus timeStatus = timeStatusRepository.findOneById(id);
    		return getTimeStatusRequest(timeStatus);
    	}else {
    		return null;
    	}
    }

    @Override
    public ResponseEntity<ResponseMessage> createTimeStatus(TimeStatusRequest timeStatusRequest) {
        TimeStatus timeStatus = new TimeStatus();
        timeStatus.setId(timeStatusRequest.getId());
        timeStatus.setCompany(companyRespository.findOneByName(timeStatusRequest.getCompanyName()));
        timeStatus.setDownTime(timeStatusRequest.getDownTime());
        timeStatus.setUpTime(timeStatusRequest.getUpTime());
        timeStatus.setMaintenance(timeStatusRequest.getMaintenance());
        timeStatus.setYear(timeStatusRequest.getYear());
        timeStatus.setMonth(timeStatusRequest.getMonth());
        if(timeStatusRequest.getId() == null)
			{
				timeStatus.setIsActive(true);
			}else {
				Boolean status = timeStatusRepository.findOneById(timeStatusRequest.getId()).getIsActive();
				timeStatus.setIsActive(status);
			}
        Long modifiedBy = WebUtil.getLoggedInUserId();
        if (timeStatusRequest.getId() == null) {
            timeStatus.setCreatedBy(modifiedBy);
            timeStatus.setCreatedDate(new Date());
        } else {
            timeStatus.setUpdatedBy(modifiedBy);
            timeStatus.setUpdatedDate(new Date());
        }

        timeStatusRepository.save(timeStatus);
        return new ResponseEntity<ResponseMessage>(
                new ResponseMessage("Success", "Time Status created/updated with id" + timeStatus.getId()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseMessage> deleteTimeStatus(Long id) {
        TimeStatus timeStatus = timeStatusRepository.findOneById(id);
        timeStatusRepository.delete(timeStatus);
        return new ResponseEntity<ResponseMessage>(
                new ResponseMessage("Success", "Time Status deleted with id" + id), HttpStatus.OK);
    }


    private TimeStatusData getTimeStatusData(List<Object[]> fromQuery) {
        TimeStatusData timeStatusData = new TimeStatusData();
        for (Object[] object : fromQuery) {
            if (object[0] == null) {
                timeStatusData.setAvgUptime(0.00);
            } else {
                timeStatusData.setAvgUptime(WebUtil.convertToTwoDecimalPLaces((double) object[0]));
            }

            if (object[1] == null) {
                timeStatusData.setAvgDownTime(0.00);
            } else {
                timeStatusData.setAvgDownTime(WebUtil.convertToTwoDecimalPLaces((double) object[1]));
            }

            if (object[2] == null) {
                timeStatusData.setAvgMaintenance(0.00);
            } else {
                timeStatusData.setAvgMaintenance(WebUtil.convertToTwoDecimalPLaces((double) object[2]));
            }
        }

        return timeStatusData;
    }


    public TimeStatusData getMonthlyData() {
        List<Object[]> objects = null;
        if (userService.isCurrentUserCustomer()) {
            Long companyId = userService.getCurrentUserCompany().getId();
            if(companyRespository.getOne(companyId).getIsActive())
            {
	            	
	            if (WebUtil.getCurrentMonth().equals("JANUARY")){
	                objects = timeStatusRepository.getMonthlyData2ForCustomer(WebUtil.getPreviousMonth(), WebUtil.getPreviousYear(), companyId);
	            }else{
	                objects = timeStatusRepository.getMonthlyDataForCustomer(WebUtil.getPreviousMonth(), WebUtil.getCurrentYear(), companyId);
	            }
	         }
        } else {

            if (WebUtil.getCurrentMonth().equals("JANUARY")){
                objects = timeStatusRepository.getMonthlyData2(WebUtil.getPreviousMonth(), WebUtil.getPreviousYear());
            }else{
                objects = timeStatusRepository.getMonthlyData(WebUtil.getPreviousMonth(), WebUtil.getCurrentYear());
            }
        }
        return getTimeStatusData(objects);
    }

    //
    public TimeStatusData getThreeMonthlyData() {

        List<String> previousMonths = WebUtil.getPreviousMonthList();
        List<Object[]> objects = null;
        int currentMonthIndex = WebUtil.getCurrentMonthIndex();
        if (userService.isCurrentUserCustomer()) {
            Long companyId = userService.getCurrentUserCompany().getId();
            if(companyRespository.getOne(companyId).getIsActive()) {
            	
	            if (currentMonthIndex == 0) {
	                objects = timeStatusRepository.getThreeMonthlyDataForCustomer(
	                        previousMonths.get(0), previousMonths.get(1), previousMonths.get(2), WebUtil.getPreviousYear(), companyId);
	
	            } else if (currentMonthIndex == 1) {
	                objects = timeStatusRepository.getThreeMonthlyData3ForCustomer(
	                        previousMonths.get(0), WebUtil.getCurrentYear(), previousMonths.get(1),
	                        previousMonths.get(2), WebUtil.getPreviousYear(), companyId);
	            } else if(currentMonthIndex == 2){
	                objects = timeStatusRepository.getThreeMonthlyData2ForCustomer(
	                        previousMonths.get(0), previousMonths.get(1), WebUtil.getCurrentYear(),
	                        previousMonths.get(2), WebUtil.getPreviousYear(), companyId);
	            } else{
	                objects = timeStatusRepository.getThreeMonthlyDataForCustomer(
	                        previousMonths.get(0), previousMonths.get(1), previousMonths.get(2), WebUtil.getCurrentYear(), companyId);
	            }
            }
        } else {
            if (currentMonthIndex == 0) {
                objects = timeStatusRepository.getThreeMonthlyData(
                        previousMonths.get(0), previousMonths.get(1), previousMonths.get(2), WebUtil.getPreviousYear());

            } else if (currentMonthIndex == 1) {
                objects = timeStatusRepository.getThreeMonthlyData3(
                        previousMonths.get(0), WebUtil.getCurrentYear(), previousMonths.get(1),
                        previousMonths.get(2), WebUtil.getPreviousYear());
            } else if(currentMonthIndex == 2){
                objects = timeStatusRepository.getThreeMonthlyData2(
                        previousMonths.get(0), previousMonths.get(1), WebUtil.getCurrentYear(),
                        previousMonths.get(2), WebUtil.getPreviousYear());
            } else{
                objects = timeStatusRepository.getThreeMonthlyData(
                        previousMonths.get(0), previousMonths.get(1), previousMonths.get(2), WebUtil.getCurrentYear());
            }
        }

        return getTimeStatusData(objects);
    }

//    public List<Object[]> example(){
//        return timeStatusRepository.example(WebUtil.getMonthNamesList());
//    }

    public TimeStatusData getYearlyData() {
        List<List<String>> yearlyMonth = WebUtil.getCurrentYearMonthList();
        List<Object[]> objects = null;

        if (userService.isCurrentUserCustomer()) {
            Long companyId = userService.getCurrentUserCompany().getId();
            if(companyRespository.getOne(companyId).getIsActive()) {
            	objects = timeStatusRepository.getYearlyDataForCustomer(yearlyMonth.get(0), WebUtil.getPreviousYear()
                    , yearlyMonth.get(1), WebUtil.getCurrentYear(), companyId);
            }
        } else {
            objects = timeStatusRepository.getYearlyData(yearlyMonth.get(0), WebUtil.getPreviousYear()
                    , yearlyMonth.get(1), WebUtil.getCurrentYear());
        }
        return getTimeStatusData(objects);
    }

    
    public Long inActivateTimeStatus(long id) {
		TimeStatus dbTimeStatus = new TimeStatus();
		Optional<TimeStatus> timeStatus = timeStatusRepository.findById(id);
		if(timeStatus.isPresent())
		{
			TimeStatus current = timeStatus.get();
			current.setIsActive(false);
			timeStatusRepository.save(current);
		}
		return id;
	}
}
