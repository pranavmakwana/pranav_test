package com.knowarth.rimscp.service.company;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.DocumentRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.TimeStatusRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.TimeStatusService;
import com.knowarth.rimscp.service.company.report.DocumentService;
import com.knowarth.rimscp.service.company.report.FileStorageService;
import com.knowarth.rimscp.service.company.report.ReportDetailService;
import com.knowarth.rimscp.service.company.report.ReportService;
import com.knowarth.rimscp.service.user.UserService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.company.CompanyRequest;
import com.knowarth.rimscp.web.model.user.UserRequest;

/**
 * Company Service Implementation
 * 
 * @author vinit.prajapati
 */
@Service
public class CompanyService implements ICompanyService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyService.class);

	@Autowired
	private CompanyRespository companyRepo;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;
	
	@Autowired
	private DocumentRepository documentRepo;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private ReportRepository reportRepo;
	
	@Autowired
	private ReportService reportService;
	
	@Autowired
	private TimeStatusRepository timeStatusRepo;
	
	@Autowired
	private TimeStatusService timeStatusService;
	
	@Autowired
	private ReportDetailRepository reportDetailRepo;
	
	@Autowired
	private ReportDetailService reportDetailService;
	
	@Value("${company.base.path}")
	private String companyProfileBasePath;

	@Override
	public Long createCompany(CompanyRequest company, MultipartFile file)
	{
//		try {
			Company dbCompany = getDBModel(company, file);
			if(company.getIsActive().equals("Active")) {
				dbCompany.setIsActive(true);
			}else
			{
				if(company.getId() != null)
				{
					List<Long> usersId = userRepository.getUsersbyCompanyId(company.getId());
					for(long id: usersId) {
						Long userId = userService.deactivateUser(id);
//						userRepository.setUserInActivate(id);
					}
					
					List<Long> documentsId = documentRepo.getDocumentbyCompanyId(company.getId());
					for(Long id: documentsId) {
						Long docId = documentService.inActivateDocument(id);
					}
					
					List<Long> reportsId = reportRepo.getReportbyCompanyId(company.getId());
					for(Long id: reportsId) {
						Long reportId = reportService.inActivateReport(id);
						List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportId(reportId);
						for(Long reportDetailId: reportDetailsId) {
							Long report_Detail_Id = reportDetailService.inActivateReportDetail(reportDetailId);
						}
					}
					
					List<Long> timeStatussId = timeStatusRepo.getTimeStatusbyCompanyId(company.getId());
					for(Long id: timeStatussId) {
						Long timeStatusId = timeStatusService.inActivateTimeStatus(id);
					}
				}
				dbCompany.setIsActive(false);
			}
//			if(company.getId() == null)
//			{
//				dbCompany.setIsActive(true);
//			}else {
//				Boolean status = companyRepo.findOneByName(company.getName()).getIsActive();
//				dbCompany.setIsActive(status);
//			}
			
			companyRepo.save(dbCompany);
			return dbCompany.getId();
//		} catch (Exception e) {
//			LOGGER.error("Error in createCompany---"+e.getMessage(),e);
//			return null;
//		}
	
	}

	/**
	 * Converts Company to DB Company Model
	 * 
	 * @param company
	 * @return
	 */
	private Company getDBModel(CompanyRequest company, MultipartFile file)
	{
		Company dbCompany = new Company();

		dbCompany.setId(company.getId());
		dbCompany.setName(company.getName());
		dbCompany.setAddress(company.getAddress());
		dbCompany.setContactNo(company.getContactNo());
		dbCompany.setWebsite(company.getWebsite());
		dbCompany.setShortName(company.getShortName());
		dbCompany.setLogo(file != null ? fileStorageService.storeCompanyProfile(company.getId(), file) : company.getLogo());
		dbCompany.setRegion(company.getRegion());
		dbCompany.setClientId(company.getClientId());
		dbCompany.setProjectKey(company.getProjectKey());
		dbCompany.setSecret(company.getSecret());
		dbCompany.setIsActive(false);
		Long modifiedBy = WebUtil.getLoggedInUserId();
		if (company.getId() == null)
		{
			dbCompany.setCreatedBy(modifiedBy);
			dbCompany.setCreatedDate(new Date());
		}
		else
		{
			dbCompany.setCreatedBy(companyRepo.getOne(company.getId()).getCreatedBy());
			dbCompany.setCreatedDate(companyRepo.getOne(company.getId()).getCreatedDate());
			dbCompany.setUpdatedBy(modifiedBy);
			dbCompany.setUpdatedDate(new Date());
		}

		return dbCompany;
	}

	/**
	 * @return retrieves all companies
	 */
	public List<String> getCompanies()
	{
		List<Company> companies = companyRepo.getAllActiveCompany();
		return companies.stream().map(Company::getName).collect(Collectors.toList());
	}

	/**
	 * @return retrieves all companies
	 */
	public List<CompanyRequest> getAllCompanies()
	{
		List<Company> companies = companyRepo.getAllActiveCompany();
		List<CompanyRequest> companyRequests = new ArrayList<>(companies.size());
		for (Company company : companies)
		{
			companyRequests.add(getCompanyRequest(company));
		}
		return companyRequests;
	}
	
	/**
	 * @return retrieves all inactive companies
	 */
	public List<CompanyRequest> getAllInActiveCompanies()
	{
		List<Company> companies = companyRepo.getAllInActiveCompany();
		List<CompanyRequest> companyRequests = new ArrayList<>(companies.size());
		for (Company company : companies)
		{
			companyRequests.add(getCompanyRequest(company));
		}
		return companyRequests;
	}
	
	private CompanyRequest getCompanyRequest(final Company company)
	{
		CompanyRequest companyRequest = new CompanyRequest();
		companyRequest.setId(company.getId());
		companyRequest.setName(company.getName());
		companyRequest.setWebsite(company.getWebsite());
		companyRequest.setShortName(company.getShortName());
		companyRequest.setContactNo(company.getContactNo());
		companyRequest.setAddress(company.getAddress());
		companyRequest.setClientId(company.getClientId());
		companyRequest.setRegion(company.getRegion());
		companyRequest.setProjectKey(company.getProjectKey());
		companyRequest.setLogo(company.getLogo());
		companyRequest.setSecret(company.getSecret());
		if(company.getIsActive()) {
			companyRequest.setIsActive("Active");
		}else {
			companyRequest.setIsActive("InActive");
		}
//		companyRequest.setIsActive(company.getIsActive());
		return companyRequest;
	}

	@Override
	public Company getCompany(Long companyId)
	{
		Optional<Company> opValue = companyRepo.findById(companyId);
		return opValue.orElse(null);
	}

	@Override
	public byte[] getMyCompanyLogo(String logo)
	{
		try
		{
			Path companyLogoPath = Paths.get(companyProfileBasePath + logo).toAbsolutePath().normalize();
			InputStream in = Files.newInputStream(companyLogoPath);
			return IOUtils.toByteArray(in);
		}
		catch (IOException e)
		{
			LOGGER.error("Error while fetching file " + e.getMessage(), e);
		}
		
		// TODO:  Add default company logo in application and return that path instead of null
		return null;
	}

	@Override
	public Long deleteCompany(long id) {
		long deleteId = id;
		companyRepo.deleteById(id);
		return deleteId;
	}
	
	@Override
	public Long deactivateCompany(long id) {
		Company dbCompany = new Company();
		Optional<Company> company = companyRepo.findById(id);
		if (company.isPresent()) {
			Company currentCompany = company.get();
			currentCompany.setIsActive(false);
			companyRepo.save(currentCompany);
		}
		return id;
	}
	
	@Override
	public Long activateCompany(long id) {
		Company dbCompany = new Company();
		Optional<Company> company = companyRepo.findById(id);
		if (company.isPresent()) {
			Company currentCompany = company.get();
			currentCompany.setIsActive(true);
			companyRepo.save(currentCompany);
		}
		return id;
	}
}
