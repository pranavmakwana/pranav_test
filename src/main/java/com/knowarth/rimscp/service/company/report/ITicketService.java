package com.knowarth.rimscp.service.company.report;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.db.model.TicketSummary;
import com.knowarth.rimscp.web.model.zoho.TicketCreate;
import com.knowarth.rimscp.web.model.zoho.api.GetTicketDataRequest;
import com.knowarth.rimscp.web.model.zoho.api.TicketRequest;

public interface ITicketService {
	
	public List<TicketRequest> getTickets(String id);
	public GetTicketDataRequest getTicketData();
	public TicketRequest createTicket(TicketCreate ticketCreate, MultipartFile file);
	public void updateTicket(TicketCreate ticketCreate, MultipartFile file);
	public List<Long> getTicketCount(String id);
}
