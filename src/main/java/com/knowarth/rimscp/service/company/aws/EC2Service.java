package com.knowarth.rimscp.service.company.aws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Tag;
import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.service.user.IUserService;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.RIMSUtil;
import com.knowarth.rimscp.web.model.aws.EC2Detail;

/**
 * AWS Service Implementation
 * 
 * @author arkan.malik
 */
@Service
public class EC2Service implements IAWSService {

	private static final Logger LOGGER = Logger.getLogger(EC2Service.class);
	
	
	
	@Autowired
	private IUserService userService;

	@Autowired
	private CompanyRespository companyRepo;

	@Override
	@Cacheable(value = "ecdetail" , key = "#id")
	public List<EC2Detail> getAwsDetails(String id)  {
		
		LOGGER.info("\n ******************* in getAwsDetails() Method Called ************** \n");
		try {
			LOGGER.info("\n ******************* in getAwsDetails() Fetching Details.. \n"); 
			List<EC2Detail> ec2Details = new ArrayList<>();
			
			if (userService.isCurrentUserCustomer()) { 
				Company company = userService.getCurrentUserCompany();
				if (company != null && RIMSUtil.isNotBlankOrNull(company.getRegion()) && company.getIsActive()) {
					ec2Details.addAll(callAWSForEC2Instance(company.getName(),company.getRegion(), company.getSecret(), company.getClientId()));
				} 
			} else { 
				List<Company> listCompany = companyRepo.findAll(); 
				for (Company tCompany : listCompany) {
					if(tCompany.getIsActive()) {
						ec2Details.addAll(callAWSForEC2Instance(tCompany.getName(),tCompany.getRegion(), tCompany.getSecret(), tCompany.getClientId()));						
					}
					
				} 
			}
			return ec2Details;	 
			
		} catch (Exception e) {
			LOGGER.error("Something went wrong in getAwsDetails() " + e);
			return null;
		} 
			
	}



//	@Cacheable(AppConstants.CACHE_KEY_EC2_DETAILS)
	private List<EC2Detail> callAWSForEC2Instance(String companyName,String pRegions, String pSecretKey, String pClientId) {

		
		LOGGER.info("\n ******************* in callAWSForEC2Instance() Method Called ************** \n");
		try {
			LOGGER.info("\n ******************* in callAWSForEC2Instance() Fetching Details.. \n"); 
			List<EC2Detail> awsList = new ArrayList<>();

			List<String> regions = Arrays.asList(pRegions.split("\\s*,\\s*"));
			if (!CollectionUtils.isEmpty(regions)) {
				for (String region : regions) {
					 
					List<EC2Detail> tAwsList = callEC2ForRegion(companyName,region, pClientId, pSecretKey);
					if (!CollectionUtils.isEmpty(tAwsList)) {
						awsList.addAll(tAwsList);
					}
				}
			} 
			return awsList;  
		} catch (Exception e) {
			LOGGER.error("Something went wrong in callAWSForEC2Instance() " + e);
			return null;
		} 
	}

	private List<EC2Detail> callEC2ForRegion(String companyName, String pRegion, String pClientId, String pSecretKey) {
		LOGGER.info("\n ******************* in callEC2ForRegion() Method Called ************** \n");
		try {
			
			LOGGER.info("\n ******************* in callEC2ForRegion() Fetching Details.. \n"); 
			List<EC2Detail> tAWSList = new ArrayList<>();
			final AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(basicCredentials(pClientId, pSecretKey)))
					.withRegion(Regions.fromName(pRegion)).build();

			boolean done = false;
			DescribeInstancesRequest request = new DescribeInstancesRequest();
			while (!done) {
				DescribeInstancesResult response = ec2.describeInstances(request);
				
				for (Reservation reservation : response.getReservations()) { 
					for (Instance instance : reservation.getInstances()) {
							
						if(instance.getTags()!= null) {
							
						}
						EC2Detail awsObject = new EC2Detail();
						 
						validateAndPrepareAWSObject(companyName,instance, awsObject);
						tAWSList.add(awsObject);
					}
				}
				request.setNextToken(response.getNextToken());
				if (response.getNextToken() == null) {
					done = true;
				} 
			}
			return tAWSList;  
		} catch (Exception e) {
			LOGGER.error("Something went wrong in callEC2ForRegion() " + e);
			return null;
		}
	}

	/**
	 * @param instance
	 * @param awsObject
	 */
	private void validateAndPrepareAWSObject(String companyName,Instance instance, EC2Detail awsObject) { 
		try {
			LOGGER.info("\n ******************* in validateAndPrepareAWSObject() Fetching Details.. \n"); 
				if (instance != null && RIMSUtil.isNotBlankOrNull(instance.getPublicIpAddress())) {
					awsObject.setPublicIp(instance.getPublicIpAddress());
				}
				if (instance != null && RIMSUtil.isNotBlankOrNull(instance.getPrivateIpAddress())) {
					awsObject.setPrivateIp(instance.getPrivateIpAddress());
				}
				if (instance != null && RIMSUtil.isNotBlankOrNull(instance.getInstanceType())) {
					awsObject.setInstanceType(instance.getInstanceType());
				}
	
				if(instance!= null && instance.getTags()!=null) {
					for(Tag tag : instance.getTags()) {
						if(tag.getValue()!=null) {
							awsObject.setName(tag.getValue());
						}else {
							awsObject.setInstanceId(instance.getInstanceId());		
						} 
					}
				}
	
				if (instance != null && instance.getCpuOptions() != null
						&& RIMSUtil.hasId(instance.getCpuOptions().getCoreCount())) {
					awsObject.setCpus(instance.getCpuOptions().getCoreCount());
				}
	
				if (instance != null && instance.getPlacement() != null
						&& RIMSUtil.isNotBlankOrNull(instance.getPlacement().getAvailabilityZone())) {
					awsObject.setAvailabilityZone(instance.getPlacement().getAvailabilityZone());
				}
	
				if (instance != null && instance.getState() != null
						&& RIMSUtil.isNotBlankOrNull(instance.getState().getName())) {
					awsObject.setStatus(instance.getState().getName());
				}
	
				if (instance != null && instance.getLaunchTime() != null) {
					awsObject.setLaunchTime(instance.getLaunchTime());
				} 
				if(companyName!=null) {
					awsObject.setCompanyName(companyName);
				}
		 	  
		} catch (Exception e) {
			LOGGER.error("Something went wrong in validateAndPrepareAWSObject() " + e);
		}
		
	}

	private static BasicAWSCredentials basicCredentials(String clientId, String secretKey) { 
		LOGGER.info("\n ******************* in basicCredentials() Called.. \n"); 
		try {
			LOGGER.info("\n ******************* in basicCredentials() Fetching Details.. \n"); 
			return new BasicAWSCredentials(clientId, secretKey);
		} catch (Exception e) {
			LOGGER.error("Something went wrong in basicCredentials() " + e);
			return null;
		}
	}
}
