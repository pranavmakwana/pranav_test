package com.knowarth.rimscp.service.company;

import java.util.List;

import com.knowarth.rimscp.web.model.company.NotificationRequest;

public interface INotificationService {

	public Long createNotification(NotificationRequest notificationRequest);

	public List<NotificationRequest> getNotificationForLatestActivities();

	public List<NotificationRequest> getNotification();

}
