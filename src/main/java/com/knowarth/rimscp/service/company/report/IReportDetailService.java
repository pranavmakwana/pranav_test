package com.knowarth.rimscp.service.company.report;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.knowarth.rimscp.web.model.report.ReportDetailRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

public interface IReportDetailService {

    public ResponseEntity<ResponseMessage> updateReportDeatilRequest(List<ReportDetailRequest> reportDetailRequests);
    public List<ReportDetailRequest> getAllReportDetailRequestByReportId(Long reportId);

}
