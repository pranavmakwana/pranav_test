package com.knowarth.rimscp.service.company.report;

import java.util.List;

import com.knowarth.rimscp.web.model.report.TemplateRequest;

public interface ITemplateService {

	Long createTemplate(TemplateRequest templateRequest);

	List<TemplateRequest> getTemplates();

	List<TemplateRequest> getInActiveTemplates();
	Long deleteTemplate(long id);

	public Long deactivateTemplate(long id);

	public Long activateTemplate(long id);
	
}


