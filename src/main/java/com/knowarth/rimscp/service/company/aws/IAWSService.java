package com.knowarth.rimscp.service.company.aws;

import java.util.List;

import com.knowarth.rimscp.web.model.aws.EC2Detail;

/**
 * User Service
 *
 * @author arkan.malik
 */
public interface IAWSService {
	/**
	 * // * @return retrieves all Aws //
	 */
	public List<EC2Detail> getAwsDetails(String id);
}
