package com.knowarth.rimscp.service.company.report;

import java.text.ParseException;
import java.util.List;

import com.knowarth.rimscp.web.model.zoho.api.TicketSummaryHandler;

public interface ITicketSummaryService {  
 
	public List<TicketSummaryHandler> getTicketSummaries() throws ParseException;
}
