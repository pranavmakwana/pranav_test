package com.knowarth.rimscp.service.company.report;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.db.model.Document;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.DocumentRepository;
import com.knowarth.rimscp.repository.NotificationRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.company.NotificationService;
import com.knowarth.rimscp.service.user.IDocumentService;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.DocumentRequest;

/**
 * DocumentService implementation
 */
@Service
public class DocumentService implements IDocumentService {
	@Autowired
	private DocumentRepository documentRepo;

	@Autowired
	private CompanyRespository companyRepo;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private NotificationRepository notificationRepo;

	@Value("${company.base.path}")
	private String companyFilesBasePath;

	private static final Logger LOGGER = Logger.getLogger(DocumentService.class);

	@Override
	public Long createDocument(DocumentRequest documentRequest, MultipartFile file) {
		 LOGGER.debug("...in .Document Service start.");
		try {
			Document dbDocument = getDBModel(documentRequest, file);
			documentRepo.save(dbDocument);
			return dbDocument.getId();
		} catch (Exception e) {
			LOGGER.error("ERROR Document Service : -"+ e.getMessage());
			return null;
		}
		 
	}

	/**
	 * Converts Document to DB Document Model
	 * 
	 * @param documentRequest
	 * @param file
	 * @return
	 */
	private Document getDBModel(DocumentRequest documentRequest, MultipartFile file) {
		Document dbDocument = new Document();
		dbDocument.setId(documentRequest.getId());
		dbDocument.setDocumentName(documentRequest.getDocumentName());
		dbDocument.setDocumentType(documentRequest.getDocumentType());
		dbDocument.setDescription(documentRequest.getDescription());
		dbDocument.setVersion(documentRequest.getVersion());
		if(documentRequest.getIsActive().equals("Active")) {
			dbDocument.setIsActive(true);
		}else {
			dbDocument.setIsActive(false);
		}
		if (documentRequest.getCompany() != null) {
			dbDocument.setCompany(companyRepo.findOneByName(documentRequest.getCompany()));
		}
		if (file != null) {
			dbDocument.setFileName(fileStorageService.storeDocument(dbDocument.getCompany().getId(), file));
		} else if (documentRequest.getId() != null && file == null) {
			dbDocument.setFileName(documentRequest.getFileName());
			Long lastCompanyId = documentRepo.getOne(documentRequest.getId()).getCompany().getId();
			fileStorageService.moveDocumentFile(lastCompanyId, dbDocument.getCompany().getId(),
					documentRequest.getFileName());
		} else {
			dbDocument.setFileName(documentRequest.getFileName());
		}

		Long modifiedBy = WebUtil.getLoggedInUserId();
		if (documentRequest.getId() == null) {
			dbDocument.setCreatedBy(modifiedBy);
			dbDocument.setCreatedDate(new Date());
			dbDocument.setStatus(AppConstants.NOTIFICATION_STATUS_NEW);
		} else {
			dbDocument.setCreatedBy(documentRepo.getOne(documentRequest.getId()).getCreatedBy());
			dbDocument.setCreatedDate(documentRepo.getOne(documentRequest.getId()).getCreatedDate());
			dbDocument.setUpdatedBy(modifiedBy);
			dbDocument.setUpdatedDate(new Date());
			dbDocument.setStatus(documentRepo.getOne(documentRequest.getId()).getStatus());
 
		}   
	 
		return dbDocument;
	}

	public List<DocumentRequest> getDocuments() {
		List<Document> documents = documentRepo.getActiveDocument();
		List<DocumentRequest> documentRequests = new ArrayList<>(documents.size());
		for (Document document : documents) {
			documentRequests.add(getDocumentRequest(document));
		}
		return documentRequests;
	}

	public List<DocumentRequest> getInActiveDocuments() {
		List<Document> documents = documentRepo.getInActiveDocument();
		List<DocumentRequest> documentRequests = new ArrayList<>(documents.size());
		for (Document document : documents) {
			documentRequests.add(getDocumentRequest(document));
		}
		return documentRequests;
	}
	
	private DocumentRequest getDocumentRequest(final Document document) {
		DocumentRequest documentRequest = new DocumentRequest();
		documentRequest.setId(document.getId());
		documentRequest.setDocumentType(document.getDocumentType());
		documentRequest.setDocumentName(document.getDocumentName());
		documentRequest.setVersion(document.getVersion());
		documentRequest.setDescription(document.getDescription());
		documentRequest.setFileName(document.getFileName());
		documentRequest.setStatus(document.getStatus());
		if(document.getIsActive()) {
			documentRequest.setIsActive("Active");
		}else{
			documentRequest.setIsActive("InActive");
		}
		if (document.getApprovedBy() != null) {
			documentRequest.setApprovedBy(userRepo.getOne(document.getApprovedBy()).getFirstName() + " "
					+ userRepo.getOne(document.getApprovedBy()).getLastName());
		}
		if (document.getReviewedBy() != null) {
			documentRequest.setReviewedBy(userRepo.getOne(document.getReviewedBy()).getFirstName() + " "
					+ userRepo.getOne(document.getReviewedBy()).getLastName());
		}
		documentRequest.setReviewedDate(document.getReviewedDate());
		documentRequest.setApprovedDate(document.getApprovedDate());
		if (document.getCreatedBy() != null) {
			documentRequest.setCreatedBy(userRepo.getOne(document.getCreatedBy()).getFirstName() + " "
					+ userRepo.getOne(document.getCreatedBy()).getLastName());
		}
		documentRequest.setCreatedDate(document.getCreatedDate());
		if (document.getUpdatedBy() != null) {
			documentRequest.setUpdatedBy(userRepo.getOne(document.getCreatedBy()).getFirstName() + " "
					+ userRepo.getOne(document.getCreatedBy()).getLastName());
		}
		documentRequest.setUpdatedDate(document.getUpdatedDate());
		documentRequest.setRole(userRepo.getOne(document.getCreatedBy()).getRole().getName());

		if (document.getCompany() != null) {
			documentRequest.setCompany(document.getCompany().getName());
		}
		return documentRequest;
	}

	/**
	 * Download Document
	 */
	@Override
	public ResponseEntity<byte[]> getDocuments(String name, String fileName) {
		Long companyId = companyRepo.findOneByName(name).getId();
		if(companyRepo.getOne(companyId).getIsActive()) {
			String documentName = documentRepo.findOneByFileName(fileName).getFileName();
			return fileStorageService.downloadDocument(companyId, documentName);			
		}else
		{
			return null;
		}

	}

	@Override
	public void sendDocumentForReview(DocumentRequest documentRequest) {
		Document document = documentRepo.getOne(documentRequest.getId());
		document.setStatus(AppConstants.NOTIFICATION_STATUS_REVIEW);
		documentRepo.save(document);
		notificationService.setNotificationRequest(documentRequest.getId(), AppConstants.TYPE_DOCUMENT);
	}

	@Override
	public void documentReviewed(DocumentRequest documentRequest, String documentComment) {
		Document document = documentRepo.getOne(documentRequest.getId());
		document.setStatus(AppConstants.NOTIFICATION_STATUS_APPROVE);
		document.setReviewedBy(WebUtil.getLoggedInUserId());
		document.setReviewedDate(new Date());
		documentRepo.save(document);
		notificationRepo.updateStatusOnReview(AppConstants.NOTIFICATION_STATUS_REVIEWED, false, documentComment,
				document.getId(), AppConstants.TYPE_DOCUMENT);
		notificationService.setNotificationRequest(documentRequest.getId(), AppConstants.TYPE_DOCUMENT);
	}

	@Override
	public void documentApproved(DocumentRequest documentRequest, String documentComment) {
		Document document = documentRepo.getOne(documentRequest.getId());
		document.setStatus(AppConstants.NOTIFICATION_STATUS_COMPLETED);
		document.setApprovedBy(WebUtil.getLoggedInUserId());
		document.setApprovedDate(new Date());
		documentRepo.save(document);
		notificationRepo.updateStatusOnApprove(AppConstants.NOTIFICATION_STATUS_COMPLETED, true, documentComment,
				document.getId(), AppConstants.TYPE_DOCUMENT, AppConstants.NOTIFICATION_STATUS_APPROVE);
		notificationRepo.updateStatusOnComplete(AppConstants.NOTIFICATION_STATUS_REVIEWED, true, document.getId(),
				AppConstants.TYPE_DOCUMENT);
	}
	
	@Override
	public void documentRejectedOnReview(DocumentRequest documentRequest) {
		Document document = documentRepo.getOne(documentRequest.getId());
		document.setStatus(AppConstants.NOTIFICATION_STATUS_REJECTED_ON_REVIEW);
		documentRepo.save(document);
		notificationRepo.rejectedDocument(AppConstants.TYPE_DOCUMENT, document.getId());
	}
	
	@Override
	public void documentRejectedOnApprove(DocumentRequest documentRequest) {
		Document document = documentRepo.getOne(documentRequest.getId());
		document.setStatus(AppConstants.NOTIFICATION_STATUS_REJECTED_ON_APPROVE);
		documentRepo.save(document);
		notificationRepo.rejectedDocument(AppConstants.TYPE_DOCUMENT, document.getId());
	}
	
	@Override
	public void documentReEdit(DocumentRequest documentRequest) {
		Document document = documentRepo.getOne(documentRequest.getId());
		document.setStatus(AppConstants.NOTIFICATION_STATUS_NEW);
		document.setReviewedBy(null);
		document.setReviewedDate(null);
		document.setApprovedBy(null);
		document.setApprovedDate(null);
		documentRepo.save(document);
	}

	@Override
	public ResponseEntity<byte[]> getDocumentForDispaly(String fileName, String companyName) {
		try {
			Long companyId = companyRepo.findOneByName(companyName).getId();
			if(companyRepo.getOne(companyId).getIsActive()) {
				
			Path documentPath = Paths
					.get(companyFilesBasePath + companyId + "/" + AppConstants.TYPE_DOCUMENT + "/" + fileName)
					.toAbsolutePath().normalize();
	
			InputStream in = Files.newInputStream(documentPath);
			byte bytes[] = IOUtils.toByteArray(in);

			HttpHeaders respHeader = new HttpHeaders();
			respHeader.setContentLength(bytes.length);
			respHeader.setContentType(MediaType.parseMediaType("application/pdf" ));
			respHeader.set("Content-Disposition", "inline; filename=" + fileName);

			return new ResponseEntity<byte[]>(bytes, respHeader, HttpStatus.OK);
			}else {
				return null;
			}
		} catch (IOException e) {
			LOGGER.error("Error while fetching file " + e.getMessage(), e);
		}
		return null;
	}
	
	public Long inActivateDocument(long id) {
		Document dbDocument = new Document();
		Optional<Document> document = documentRepo.findById(id);
		if(document.isPresent())
		{
			Document currentDocument = document.get();
			currentDocument.setIsActive(false);
			documentRepo.save(currentDocument);
		}
		return id;
	}
}
