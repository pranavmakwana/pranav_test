package com.knowarth.rimscp.service.company.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.knowarth.rimscp.db.model.ReportSection;
import com.knowarth.rimscp.db.model.Template;
import com.knowarth.rimscp.repository.ReportActivityRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.ReportSectionRepository;
import com.knowarth.rimscp.repository.TemplateRepository;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.ReportSectionDropdownRequest;
import com.knowarth.rimscp.web.model.report.ReportSectionRequest;
import com.knowarth.rimscp.web.model.report.TemplateRequest;

@Service
public class ReportSectionService implements IReportSectionService {

	@Autowired
	ReportSectionRepository reportSectionRepo;

	@Autowired
	TemplateRepository templateRepo;
	
	@Autowired
	ReportActivityService reportActivityService;
	
	@Autowired
	ReportActivityRepository reportActivityRepo;
	
	@Autowired 
	private ReportDetailRepository reportDetailRepo;
	
	@Autowired
	private ReportDetailService reportDetailService;
	
	@Autowired
	private ReportService reportService;
	/**
	 * @return Create || Update || ReportDetail Section
	 */
	@Override
	public Long createReportSection(ReportSectionRequest reportSectionRequest) {

		ReportSection reportSection = getDBModel(reportSectionRequest);
		if(reportSectionRequest.getIsActive().equals("Active")) {
			reportSection.setIsActive(true);
		}else {
			if(reportSectionRequest.getId() != null)
			{
				List<Long> reportActivitiesId = reportActivityRepo.getReportActivitybySectionId(reportSectionRequest.getId());
				for(Long id: reportActivitiesId) {
					Long reportDetailId = reportActivityService.inActivateActivity(id);
					List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportActivityId(id);
					for(Long report_details_id: reportDetailsId) {
						Long report_Detail_Id = reportDetailService.inActivateReportDetail(report_details_id);
//						Long reportId = reportDetailRepo.getReportbyReportDetailsId(report_Detail_Id);
//						Long report_Id = reportService.inActivateReport(reportId);
					}
				}
			}
			reportSection.setIsActive(false);
		}
		reportSectionRepo.save(reportSection);
		return reportSection.getId();
	}

	/**
	 * @return List<ReportSection> all ReportSection
	 */

	@Override
	public List<ReportSectionRequest> getAllReportSection() {
//		List<ReportSection> sections = reportSectionRepo.findAllByOrderByTemplateAsc();
		List<ReportSection> sections = reportSectionRepo.getActiveSections();
		List<ReportSectionRequest> sectionRequest = new ArrayList<>(sections.size());
		for (ReportSection section : sections) {
			sectionRequest.add(getSectionRequest(section));
		}
		return sectionRequest;
	}

	@Override
	public List<ReportSectionDropdownRequest> getReportSectionForDropdown() {
		List<ReportSectionRequest> reportSectionRequests = getAllReportSection();
		String template = null;

		List<ReportSectionDropdownRequest> results = new ArrayList<>();
		ReportSectionDropdownRequest result = null;

		for (ReportSectionRequest reportSectionRequest : reportSectionRequests) {
			if (!reportSectionRequest.getTemplateName().equals(template)) {

				result = new ReportSectionDropdownRequest();
				result.setTemplate(reportSectionRequest.getTemplateName());
				template = reportSectionRequest.getTemplateName();
				results.add(result);

			}

			result.getSection().add(reportSectionRequest.getName());
		} 
		return results; 
	}

	private ReportSectionRequest getSectionRequest(final ReportSection sections) {
		ReportSectionRequest sectionRequest = new ReportSectionRequest();
		sectionRequest.setId(sections.getId());
		sectionRequest.setName(sections.getName());
		if(sections.getIsActive()) {
			sectionRequest.setIsActive("Active");
		}else{
			sectionRequest.setIsActive("InActive");
		}
		if (sections.getTemplate() != null) {
			sectionRequest.setTemplateName(sections.getTemplate().getName());
		}
		return sectionRequest;
	}

	/**
	 * @return ReportDetail Section Name || List<String>.
	 */
	@Override
	public List<String> getReportSections() {
		List<ReportSection> reportSections = reportSectionRepo.getActiveSections();
		return reportSections.stream().map(ReportSection::getName).collect(Collectors.toList());
	}

	/**
	 * @return ReportSection set Values into DB.
	 */
	public ReportSection getDBModel(ReportSectionRequest reportSectionRequest) {

		ReportSection dbReportSection = new ReportSection();

		dbReportSection.setId(reportSectionRequest.getId());
		dbReportSection.setName(reportSectionRequest.getName());
		dbReportSection.setTemplate(templateRepo.findOneByName(reportSectionRequest.getTemplateName()));
		
		if(reportSectionRequest.getIsActive().equals("Active")) {
			dbReportSection.setIsActive(true);
		}else {
			dbReportSection.setIsActive(false);
		}
		 
		Long modifiedBy = WebUtil.getLoggedInUserId();

		if (reportSectionRequest.getId() == null) {
			dbReportSection.setCreatedBy(modifiedBy);
			dbReportSection.setCreatedDate(new Date());
		} else {
			dbReportSection.setCreatedBy(reportSectionRepo.getOne(reportSectionRequest.getId()).getCreatedBy());
			dbReportSection.setCreatedDate(reportSectionRepo.getOne(reportSectionRequest.getId()).getCreatedDate());
			dbReportSection.setUpdatedDate(new Date());
			dbReportSection.setUpdatedBy(modifiedBy);
		}

		return dbReportSection;

	}
	
	@Override
	public Long deleteSection(long id) {
		long deleteId = id;
		reportSectionRepo.deleteById(id);
		return deleteId;
	}

	@Override
	public List<ReportSectionRequest> getInActiveReportSection() {
		List<ReportSection> sections = reportSectionRepo.getInActiveSections();
		List<ReportSectionRequest> sectionRequest = new ArrayList<>(sections.size());
		for (ReportSection section : sections) {
			sectionRequest.add(getSectionRequest(section));
		}
		return sectionRequest;
	}

	@Override
	public Long deactivateSection(long id) {
		Optional<ReportSection> section = reportSectionRepo.findById(id);
		if(section.isPresent())
		{
			ReportSection currentSection = section.get();
			currentSection.setIsActive(false);
			reportSectionRepo.save(currentSection);
		}
		return id;
	}

	@Override
	public Long activateSection(long id) {
		ReportSection dbUser = new ReportSection();
		Optional<ReportSection> section = reportSectionRepo.findById(id);
		if(section.isPresent())
		{
			ReportSection currentSection = section.get();
			currentSection.setIsActive(true);
			reportSectionRepo.save(currentSection);
		}
		return id;
	}
	

}
