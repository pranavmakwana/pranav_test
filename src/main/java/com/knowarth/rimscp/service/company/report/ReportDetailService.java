package com.knowarth.rimscp.service.company.report;

import java.util.ArrayList;
import java.util.List; 
import org.apache.log4j.Logger;
import org.hibernate.NonUniqueResultException;

import java.util.Optional; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.db.model.Report;
import com.knowarth.rimscp.db.model.ReportDetail;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.ReportActivityRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.ReportSectionRepository;
import com.knowarth.rimscp.web.model.report.ReportActivityDetailRequest;
import com.knowarth.rimscp.web.model.report.ReportDetailHandler;
import com.knowarth.rimscp.web.model.report.ReportDetailRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@Service
public class ReportDetailService implements IReportDetailService {
	 
	private static final Logger LOGGER = Logger.getLogger(ReportDetailService.class);
	@Autowired
	private ReportDetailRepository reportDetailRepository;

	@Autowired
	private ReportActivityRepository reportActivityRepository;

	@Autowired
	private ReportSectionRepository reportSectionRepo;

	@Autowired
	private ReportRepository reportRepository;
 
	@Autowired
	private FileStorageService fileStorageService;

	public List<ReportDetailRequest> getAllReportDetailRequestByReportId(Long reportId) {
	
		try {
			
			List<ReportDetailHandler> reportDetailHandlers = reportDetailRepository.getReportDetailByJPAQuery(reportId);
			List<ReportDetailRequest> reportDetailRequests = new ArrayList<>();
			ReportDetailRequest reportDetailRequest = null;
			String section = null;

			for (ReportDetailHandler reportDetailHandler : reportDetailHandlers) {
				if (!reportDetailHandler.getSectionName().equals(section)) {

					reportDetailRequest = new ReportDetailRequest();
					reportDetailRequest.setSection(reportDetailHandler.getSectionName());
					reportDetailRequests.add(reportDetailRequest);

					section = reportDetailHandler.getSectionName();
				}
				 
				ReportActivityDetailRequest reportActivityDetailRequest = new ReportActivityDetailRequest();
				reportActivityDetailRequest.setActivity(reportDetailHandler.getActivityName()); 
				reportActivityDetailRequest.setActivityId(reportActivityRepository.findActivityIdBySectionIdAndName(reportSectionRepo.findOneByName(reportDetailHandler.getSectionName()).getId(), reportDetailHandler.getActivityName()));
				reportActivityDetailRequest.setAttachement(reportDetailHandler.getAttachment());
				reportActivityDetailRequest.setDate(reportDetailHandler.getDate());
				reportActivityDetailRequest.setFrequencyOfActivities(reportDetailHandler.getFrequencyOfActivities());
				reportActivityDetailRequest.setNotes(reportDetailHandler.getNotes());
				reportActivityDetailRequest.setPriority(reportDetailHandler.getPriority());
				reportActivityDetailRequest.setStatus(reportDetailHandler.getStatus());
				reportActivityDetailRequest.setServer(reportDetailHandler.getServer());
				reportActivityDetailRequest.setId(reportDetailHandler.getId());
				reportActivityDetailRequest.setReportId(reportDetailHandler.getReportId());

				reportDetailRequest.getReportActivityDetailRequests().add(reportActivityDetailRequest);
		LOGGER.info("Get ReportDetail Activity ID: - " + reportActivityDetailRequest.getActivityId()+
				"\nReport ID : -" + reportId);
				
			}
			return reportDetailRequests;
			
			
		} catch (NonUniqueResultException e) {
			LOGGER.error("Error Accured in ReportDetail Service :- " + e);
			return null;
		}
		 
	}

	public ResponseEntity<ResponseMessage> updateReportDeatilRequest(List<ReportDetailRequest> reportDetailRequests) {
		for (ReportDetailRequest reportDetailRequest : reportDetailRequests) {
			System.out.println(reportDetailRequest.getSection());
			List<ReportActivityDetailRequest> reportActivityDetailRequests = reportDetailRequest
					.getReportActivityDetailRequests();

			for (ReportActivityDetailRequest reportActivityDetailRequest : reportActivityDetailRequests) {
				ReportDetail reportDetail = new ReportDetail();
				reportDetail.setAttachment(reportActivityDetailRequest.getAttachement());
				reportDetail.setDate(reportActivityDetailRequest.getDate());
				reportActivityDetailRequest.setActivityId(reportActivityDetailRequest.getActivityId());
				reportDetail.setReportActivities(
						reportActivityRepository.findOneByNameAndReportSection(reportActivityDetailRequest.getActivity(), 
						reportSectionRepo.findOneByNameAndTemplate(reportDetailRequest.getSection(), 
								reportRepository.findOneById(reportActivityDetailRequest.getReportId()).getTemplate())));
				reportDetail.setReport(reportRepository.findOneById(reportActivityDetailRequest.getReportId()));
				reportDetail.setId(reportActivityDetailRequest.getId());
				reportDetail.setPriority(reportActivityDetailRequest.getPriority());
				reportDetail.setNotes(reportActivityDetailRequest.getNotes());
				reportDetail.setStatus(reportActivityDetailRequest.getStatus());
				reportDetail.setServers(reportActivityDetailRequest.getServer());
				reportDetail.setFrequencyOfActivities(reportActivityDetailRequest.getFrequencyOfActivities());
				if(reportActivityDetailRequest.getId() == null)
				{
					reportDetail.setIsActive(true);
				}else { 
					Boolean status = reportDetailRepository.findOneById(reportActivityDetailRequest.getId()).getIsActive();
					reportDetail.setIsActive(status);
				}
				reportDetailRepository.save(reportDetail);
			}
		}
		return new ResponseEntity<>(new ResponseMessage("Okay", "Updated"), HttpStatus.OK);
	}

	public String uploadFile(final Long reportDetailId, MultipartFile file,Long activityId) {
		LOGGER.info("in Method uploadFile() "+ reportDetailId + file + activityId);
 		try {
// 				Long activityId = reportActivityRepository.findOneByName(activityName).getId();
 				Long id = reportDetailRepository.findOneByReportIdAndActivityId(reportDetailId, activityId);
				ReportDetail reportDetail = reportDetailRepository.findOneById(id);
				Report report = reportRepository
						.findOneById(reportDetailRepository.findOneById(id).getReport().getId());
				Long companyId = report.getCompany().getId();
				String fileName = fileStorageService.storeReport(companyId, file);
				reportDetail.setAttachment(fileName);
				reportDetailRepository.save(reportDetail);

				return fileName;
			 
		} catch (Exception e) {
			LOGGER.error("Activity Details Errror" + e.getMessage());
			return null;
		}	 
		
	}

	/**
	 * Download Document
	 */
	public ResponseEntity<byte[]> getReport(Long reportDetailId, String reportName) {
		Report report = reportRepository
				.findOneById(reportDetailRepository.findOneById(reportDetailId).getReport().getId());
		Long companyId = report.getCompany().getId();
		String documentName = reportDetailRepository.findOneById(reportDetailId).getAttachment();
		return fileStorageService.downloadReport(companyId, documentName);

	}
	
	
	public Long inActivateReportDetail(long id) {
		
		Optional<ReportDetail> reportDetail = reportDetailRepository.findById(id);
		if(reportDetail.isPresent())
		{
			ReportDetail current = reportDetail.get();
			current.setIsActive(false);
			reportDetailRepository.save(current);
		}
		return id;
	}

}
