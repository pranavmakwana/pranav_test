package com.knowarth.rimscp.service.company.report;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.WebUtil;

/**
 * File Storage Service to store Company Profile & User logo
 * 
 * @author vinit.prajapati
 *
 */
@Service
public class FileStorageService {

	@Value("${user.profile.base.path}")
	private String userProfileBasePath;

	@Value("${company.base.path}")
	private String companyFilesBasePath;

	
	//private String companyDocument = "Documents";
	//private String companyReport = "Reports";

	/**
	 * stores company logo
	 * 
	 * @param file company logo
	 * @return file name
	 */
	public String storeCompanyProfile(Long companyId, MultipartFile file) {
		return storeImage(companyId, file, companyFilesBasePath);
	}

	/**
	 * storeDocument
	 * 
	 * @param documentId
	 * @param file
	 * @return
	 */
	public String storeDocument(Long companyId, MultipartFile file) {
		return storeDocument(companyId, file, companyFilesBasePath, AppConstants.TYPE_DOCUMENT);
	}

	public String storeReport(Long companyId, MultipartFile file) {
		return storeDocument(companyId, file, companyFilesBasePath, AppConstants.TYPE_REPORT);
	}
	
	
	/**
	 * Store user profile
	 * 
	 * @param userId
	 * @param file
	 * @return
	 */
	public String storeUserProfile(Long userId, MultipartFile file) {
		return storeImage(userId, file, userProfileBasePath);
	}

	/**
	 * 
	 * @param srcId
	 * @param destinationId
	 * @param fileName
	 */
	public void moveDocumentFile(Long srcId, Long destinationId, String fileName) {
		moveDocument(srcId, destinationId, fileName, AppConstants.TYPE_DOCUMENT);
	}

	public ResponseEntity<byte[]> downloadReport(Long companyId, String fileName) {
		return downloadFile(companyId, fileName,AppConstants.TYPE_REPORT);
	}

	public ResponseEntity<byte[]> downloadDocument(Long companyId, String fileName) {
		return downloadFile(companyId, fileName,AppConstants.TYPE_DOCUMENT);
	}

	private String storeImage(Long Id, MultipartFile file, String basePath) {
		// Normalize file name
		String fileName = WebUtil.getUUID() + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());

		try {
			Path imageLocation = Paths.get(basePath).toAbsolutePath().normalize();
			ensureDirectoryStructure(imageLocation);
			Files.copy(file.getInputStream(), imageLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			return fileName;
		} catch (IOException ex) {
			throw new IllegalArgumentException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	/**
	 * 
	 * @param companyId
	 * @param file
	 * @param basePath
	 * @return
	 */
	private String storeDocument(Long companyId, MultipartFile file, String basePath, String folderName) {
		// Normalize file name
		String fileName = WebUtil.getUUID() + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());

		try {
			Path companyDocumentLocation = Paths.get(basePath + companyId + "/" + folderName + "/").toAbsolutePath()
					.normalize();
			ensureDirectoryStructure(companyDocumentLocation);
			Files.copy(file.getInputStream(), companyDocumentLocation.resolve(fileName),
					StandardCopyOption.REPLACE_EXISTING);
			return fileName;
		} catch (IOException ex) {
			throw new IllegalArgumentException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}
	 
	/**
	 * @param path
	 */
	private void ensureDirectoryStructure(Path path) {
		try {
			Files.createDirectories(path);

		} catch (Exception ex) {
			throw new RuntimeException("Could not create the directory where the uploaded files will be stored.", ex);
		}
	}

	private void moveDocument(Long srcId, Long destinationId, String fileName, String folderName) {
		Path srcFilePath = Paths.get(companyFilesBasePath + "/" + srcId + "/" + folderName + "/" + fileName)
				.toAbsolutePath().normalize();
		Path destinationFolderPath = Paths.get(companyFilesBasePath + "/" + destinationId + "/" + folderName)
				.toAbsolutePath().normalize();
		File file = new File(srcFilePath.toString());
		File destFile = new File(destinationFolderPath.toString());
		if (file.exists()) {
			if (!destFile.exists()) {
				ensureDirectoryStructure(destinationFolderPath);
			}
			try {
				Files.move(Paths.get(srcFilePath.toString()),
						Paths.get(destinationFolderPath.toString() + "/" + fileName),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ex) {
				throw new IllegalArgumentException("Could not move file " + fileName + ". Please try again!", ex);
			}
		}

	}

	private ResponseEntity<byte[]> downloadFile(Long companyId, String fileName, String folderName) {
		Path documentPath = Paths.get(companyFilesBasePath + "/" + companyId + "/" + folderName + "/" + fileName)
				.toAbsolutePath().normalize();
		try {
			InputStream in = Files.newInputStream(documentPath);
			byte bytes[] = IOUtils.toByteArray(in);

			HttpHeaders respHeader = new HttpHeaders();
			respHeader.setContentLength(bytes.length);
			respHeader.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);

			return new ResponseEntity<byte[]>(bytes, respHeader, HttpStatus.OK);
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not download file " + fileName + ". Please try again!", e);
		}

	}

}
