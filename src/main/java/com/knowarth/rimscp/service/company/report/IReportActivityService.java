package com.knowarth.rimscp.service.company.report;

import java.util.List;

import com.knowarth.rimscp.web.model.report.ReportActivityRequest;

public interface IReportActivityService {

	long createActivity(ReportActivityRequest reportActivityRequest);

	public List<String> getActivity();

	List<ReportActivityRequest> getAllActivities();

	public Long deleteActivity(long id);

	List<ReportActivityRequest> getInActiveAllActivities();

}
