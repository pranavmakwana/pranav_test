package com.knowarth.rimscp.service.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knowarth.rimscp.db.model.Document;
import com.knowarth.rimscp.db.model.Notification;
import com.knowarth.rimscp.db.model.Report;
import com.knowarth.rimscp.repository.DocumentRepository;
import com.knowarth.rimscp.repository.NotificationRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.company.NotificationRequest;

@Service
public class NotificationService implements INotificationService 
{

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private NotificationRepository notificationRepo;
	
	@Autowired
	private DocumentRepository documentRepo;
	
	@Autowired
	private ReportRepository reportRepo;

	@Override
	public Long createNotification(NotificationRequest notificationRequest) 
	{

		Notification notification = new Notification();
		notification.setId(notificationRequest.getId());
		notification.setType(notificationRequest.getType());
		notification.setUser(userRepo.getUserByFullName(notificationRequest.getUser()));
		notification.setRole(notification.getUser().getRole());
		notification.setText(notificationRequest.getText());
		notification.setStatus(notificationRequest.getStatus());
		notification.setComment(notificationRequest.getComment());
		notification.setCompleted(notificationRequest.getCompleted());
		notification.setDocument(notificationRequest.getDocument());
		notification.setRequestedDate(new Date());
		notificationRepo.save(notification);
		return notification.getId();
	}

	@Override
	public List<NotificationRequest> getNotificationForLatestActivities()
	{
		String roleName = userRepo.getOne(WebUtil.getLoggedInUserId()).getRole().getName();
		if (roleName.equals("Manager")) 
		{
			List<Notification> notifications = notificationRepo.getLatestActivitiesForManager();
			List<NotificationRequest> notificationRequests = new ArrayList<>(notifications.size());
			for (Notification notification : notifications) {
			notificationRequests.add(getNotificationRequest(notification));
		}
			return notificationRequests;
		}
		if (roleName.equals("Lead"))
		{
			List<Notification> notifications = notificationRepo.getLatestActivitiesForLead();
			List<NotificationRequest> notificationRequests = new ArrayList<>(notifications.size());
			for (Notification notification : notifications) {
			notificationRequests.add(getNotificationRequest(notification));
		}
			return notificationRequests;
		}
		return null;
	}
	
	@Override
	public List<NotificationRequest> getNotification()
	{
		String roleName = userRepo.getOne(WebUtil.getLoggedInUserId()).getRole().getName();
		if (roleName.equals("Manager")) 
		{
			List<Notification> notifications = notificationRepo.getNotificationForManager();
			List<NotificationRequest> notificationRequests = new ArrayList<>(notifications.size());
			for (Notification notification : notifications) {
			notificationRequests.add(getNotificationRequest(notification));
		}
			return notificationRequests;
		}
		if (roleName.equals("Lead"))
		{
			List<Notification> notifications = notificationRepo.getNotificationForLead();
			List<NotificationRequest> notificationRequests = new ArrayList<>(notifications.size());
			for (Notification notification : notifications) {
			notificationRequests.add(getNotificationRequest(notification));
		}
			return notificationRequests;
		}
		return null;
	}

	private NotificationRequest getNotificationRequest(final Notification notification)
	{
		NotificationRequest notificationRequest = new NotificationRequest();
		notificationRequest.setId(notification.getId());
		notificationRequest.setType(notification.getType());
		notificationRequest.setDocument(notification.getDocument());
		notificationRequest.setUser(notification.getUser().getFirstName() + ' ' + notification.getUser().getLastName());
		notificationRequest.setRole(notification.getRole().getName());
		notificationRequest.setText(notification.getText());
		notificationRequest.setStatus(notification.getStatus());
		notificationRequest.setComment(notification.getComment());
		notificationRequest.setCompleted(notification.getCompleted());
		notificationRequest.setProfile(notification.getUser().getProfile());
		notificationRequest.setRequestedDate(notification.getRequestedDate());
		return notificationRequest;
	}

	public void setNotificationRequest(Long id,String type) 
	{
		NotificationRequest notificationRequest = new NotificationRequest();
		if(type.equals(AppConstants.TYPE_REPORT))
		{
			Report report = reportRepo.getOne(id);
			notificationRequest.setType(type);
			notificationRequest.setDocument(id);
			notificationRequest.setUser(userRepo.getOne(report.getCreatedBy()).getFirstName()
					+ " " + userRepo.getOne(report.getCreatedBy()).getLastName());	
			notificationRequest.setRole(userRepo.getOne(report.getCreatedBy()).getRole().getName());
			notificationRequest.setText(setTextForNotification(id, type));
			if (report.getReviewedBy() == null && report.getApprovedBy() == null)
			{
				notificationRequest.setStatus(AppConstants.NOTIFICATION_STATUS_REVIEW);
				notificationRequest.setCompleted(false);
			}
			else if (report.getApprovedBy() == null) 
			{
				notificationRequest.setStatus(AppConstants.NOTIFICATION_STATUS_APPROVE);
				notificationRequest.setCompleted(false);
			} else 
			{
				notificationRequest.setStatus(AppConstants.NOTIFICATION_STATUS_COMPLETED);
				notificationRequest.setCompleted(true);
			}

			notificationRequest.setComment("comments");
		}
		if(type.equals(AppConstants.TYPE_DOCUMENT))
		{
			Document document = documentRepo.getOne(id);
			notificationRequest.setType(type);
			notificationRequest.setDocument(id);
			notificationRequest.setUser(userRepo.getOne(document.getCreatedBy()).getFirstName() + " "
					+ userRepo.getOne(documentRepo.getOne(id).getCreatedBy()).getLastName());
			notificationRequest.setRole(userRepo.getOne(document.getCreatedBy()).getRole().getName());
			notificationRequest.setText(setTextForNotification(id, type));
			if (document.getReviewedBy() == null && document.getApprovedBy() == null)
			{
				notificationRequest.setStatus(AppConstants.NOTIFICATION_STATUS_REVIEW);
				notificationRequest.setCompleted(false);
			} 
			else if (document.getApprovedBy() == null) 
			{
				notificationRequest.setStatus(AppConstants.NOTIFICATION_STATUS_APPROVE);
				notificationRequest.setCompleted(false);
			}
			else 
			{
				notificationRequest.setStatus(AppConstants.NOTIFICATION_STATUS_COMPLETED);
				notificationRequest.setCompleted(true);
			}

			notificationRequest.setComment("comments");
		}
		createNotification(notificationRequest);	
	}
	
	public String setTextForNotification(Long id,String type)
	{
		if(type.equals(AppConstants.TYPE_REPORT)) 
		{
			Report report = reportRepo.getOne(id);
			return type + " [" + report.getReportName() + " - "
			+ report.getCompany().getName() + " ] submitted by "
			+ userRepo.getOne(report.getCreatedBy()).getFirstName()
			+ " "
			+ userRepo.getOne(report.getCreatedBy()).getLastName()
			+ " on " + report.getCreatedDate();
			
		}
		if(type.equals(AppConstants.TYPE_DOCUMENT))
		{
			Document document = documentRepo.getOne(id);
			return type + " [" + document.getDocumentName() + " - "
			+ document.getCompany().getName() + " ] submitted by "
			+ userRepo.getOne(document.getCreatedBy()).getFirstName() + " "
			+ userRepo.getOne(document.getCreatedBy()).getLastName() + " on " + document.getCreatedDate();
		}
		return null;
	}
}
