package com.knowarth.rimscp.service.company.report;

import com.knowarth.rimscp.db.model.ReportActivity;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.ReportActivityRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportSectionRepository;
import com.knowarth.rimscp.repository.TemplateRepository;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.ReportActivityRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReportActivityService implements IReportActivityService {
	
	//TODO: replace systout with LOGGER.debug
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportActivityService.class);
	
	@Autowired
	private ReportSectionRepository reportSectionRepo;
	@Autowired
	private ReportActivityRepository reportActivityRepo;
	@Autowired
	private TemplateRepository templateRepository;
	@Autowired
	private ReportDetailService reportDetailService;
	@Autowired
	private ReportDetailRepository reportDetailRepo;
	@Autowired
	private ReportService reportService;

	@Override
	public long createActivity(ReportActivityRequest reportActivityRequest) {
		ReportActivity reportActivity = getDBModel2(reportActivityRequest);
		if(reportActivityRequest.getIsActive().equals("Active")) {
			reportActivity.setIsActive(true);
		}else {
			if(reportActivityRequest.getId() != null)
			{
				List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportActivityId(reportActivityRequest.getId());
				for(Long id: reportDetailsId) {
					Long reportDetailId = reportDetailService.inActivateReportDetail(id);
//					Long reportId = reportDetailRepo.getReportbyReportDetailsId(reportDetailId);
//					Long report_Id = reportService.inActivateReport(reportId);
				}
			}
			reportActivity.setIsActive(false);
		}
		reportActivityRepo.save(reportActivity);
		return reportActivity.getId();
	}
	
	public ReportActivity getDBModel2(ReportActivityRequest reportActivityRequest) {
		ReportActivity reportActivity = new ReportActivity();
		reportActivity.setId(reportActivityRequest.getId());
		reportActivity.setName(reportActivityRequest.getName());
		if (reportActivityRequest.getSectionName() != null) {
			reportActivity
					.setReportSection(reportSectionRepo.findOneByNameAndTemplate(reportActivityRequest.getSectionName(),
							templateRepository.findOneByName(reportActivityRequest.getTemplateName())));
		}
		Long modifiedBy = WebUtil.getLoggedInUserId();
		if (reportActivityRequest.getId() == null || reportActivityRequest.getId() == 0) {
			reportActivity.setCreatedBy(modifiedBy);
			reportActivity.setCreatedDate(new Date());
		} else {
			reportActivity.setCreatedBy(reportActivityRepo.getOne(reportActivityRequest.getId()).getCreatedBy());
			reportActivity.setCreatedDate(reportActivityRepo.getOne(reportActivityRequest.getId()).getCreatedDate());
			reportActivity.setUpdatedBy(modifiedBy);
			reportActivity.setUpdatedDate(new Date());
		}
		return reportActivity;
	}

	public ReportActivity getDBModel(ReportActivityRequest reportActivityRequest) {
		ReportActivity reportActivity = new ReportActivity();
		reportActivity.setId(reportActivityRequest.getId());
		reportActivity.setName(reportActivityRequest.getName());
		if (reportActivityRequest.getSectionName() != null) {
			reportActivity.setReportSection(reportSectionRepo.findOneByName(reportActivityRequest.getSectionName()));
		}
		Long modifiedBy = WebUtil.getLoggedInUserId();
		if (reportActivityRequest.getId() == null) {
			reportActivity.setCreatedBy(modifiedBy);
			reportActivity.setCreatedDate(new Date());
		} else {
			reportActivity.setCreatedBy(reportActivityRepo.getOne(reportActivityRequest.getId()).getCreatedBy());
			reportActivity.setCreatedDate(reportActivityRepo.getOne(reportActivityRequest.getId()).getCreatedDate());
			reportActivity.setUpdatedBy(modifiedBy);
			reportActivity.setUpdatedDate(new Date());
		}
		return reportActivity;
	}

	public List<String> getActivity() {
		List<ReportActivity> activity = reportActivityRepo.getActiveActivity();
		return activity.stream().map(ReportActivity::getName).collect(Collectors.toList());
	}

	public List<ReportActivityRequest> getAllActivities() {
		List<ReportActivity> activities = reportActivityRepo.getActiveActivity();
		List<ReportActivityRequest> activityRequest = new ArrayList<>(activities.size());
		for (ReportActivity activity : activities) {
			activityRequest.add(getActivityRequest(activity));
		}
		return activityRequest;
	}

	@Override
	public List<ReportActivityRequest> getInActiveAllActivities() {
		List<ReportActivity> activities = reportActivityRepo.getInActiveActivity();
		List<ReportActivityRequest> activityRequest = new ArrayList<>(activities.size());
		for (ReportActivity activity : activities) {
			activityRequest.add(getActivityRequest(activity));
		}
		return activityRequest;
	}
	
	private ReportActivityRequest getActivityRequest(final ReportActivity activities) {
		ReportActivityRequest activityRequest = new ReportActivityRequest();
		activityRequest.setId(activities.getId());
		activityRequest.setName(activities.getName());
		if(activities.getIsActive()) {
			activityRequest.setIsActive("Active");
		}else{
			activityRequest.setIsActive("InActive");
		}
		if (activities.getReportSection() != null) {
			activityRequest.setSectionName(activities.getReportSection().getName());
			activityRequest.setTemplateName(activities.getReportSection().getTemplate().getName());
		}
		return activityRequest;
	}

	@Override
	public Long deleteActivity(long id) {
		long deleteId = id;
		reportActivityRepo.deleteById(id);
		return deleteId;
	}
	
	public Long inActivateActivity(long id) {
		Optional<ReportActivity> activity = reportActivityRepo.findById(id);
		if(activity.isPresent())
		{
			ReportActivity current = activity.get();
			current.setIsActive(false);
			reportActivityRepo.save(current);
		}
		return id;
	}

}
