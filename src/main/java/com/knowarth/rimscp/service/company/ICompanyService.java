package com.knowarth.rimscp.service.company;

import java.util.List;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.web.model.company.CompanyRequest;

import org.springframework.web.multipart.MultipartFile;

/**
 * Company Service
 * 
 * @author vinit.prajapati
 *
 */
public interface ICompanyService {

	/**
	 * Create Company
	 * 
	 * @param company
	 * @return
	 */
	Long createCompany(CompanyRequest company, MultipartFile file);

	/**
	 * @return retrieves all companies
	 */
	public List<String> getCompanies();

	/**
	 * @return retrieves all companies
	 */
	public List<CompanyRequest> getAllCompanies();

	// FInd By ID
	public Company getCompany(Long id);

	byte[] getMyCompanyLogo(String logo);

	public Long deleteCompany(long id);

	public Long deactivateCompany(long id);

	public Long activateCompany(long id);

	List<CompanyRequest> getAllInActiveCompanies();

}