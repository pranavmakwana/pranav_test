package com.knowarth.rimscp.service.company.report;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.knowarth.rimscp.web.model.report.ReportMasterRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

public interface IReportService {

    public List<ReportMasterRequest> getReports();

    public ResponseEntity<ResponseMessage> createReport(ReportMasterRequest reportMasterRequest);

	public void sendReportForReview(ReportMasterRequest reportMasterRequest);

//	void reportReviewed(ReportMasterRequest reportMasterRequest);
//
//	void reportApproved(ReportMasterRequest reportMasterRequest);

	void reportReviewed(ReportMasterRequest reportMasterRequest, String reportComment);

	void reportApproved(ReportMasterRequest reportMasterRequest, String reportComment);

	public Long deleteReport(long id);

	public List<ReportMasterRequest> getInActiveReports();
	
	//  REJECT FOR REPORT
	public void reportRejectedOnReview(ReportMasterRequest documentRequest);
	
	public void reportRejectedOnApprove(ReportMasterRequest documentRequest);

	public void reportReEdit(ReportMasterRequest documentRequest);
}
