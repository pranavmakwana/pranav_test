package com.knowarth.rimscp.service.company.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.knowarth.rimscp.db.model.Report;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.NotificationRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.TemplateRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.company.NotificationService;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.ReportMasterRequest;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@Service
public class ReportService implements IReportService {

	@Autowired
	private ReportRepository repo;

	@Autowired
	private CompanyRespository companyRespository;

	@Autowired
	private TemplateRepository templateRepository;

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ReportDetailRepository reportDetailRepo;
	
	@Autowired
	private ReportDetailService reportDetailService;

	@Autowired
	private NotificationRepository notificationRepo;

	@Autowired
	private NotificationService notificationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

	@Override
	public List<ReportMasterRequest> getReports() {
		List<Report> reports = repo.getActiveReport();
		List<ReportMasterRequest> reportMasterRequests = new ArrayList<>();

		for (Report report : reports) {
			ReportMasterRequest reportMasterRequest = new ReportMasterRequest();
			reportMasterRequest.setReportId(report.getId());
			reportMasterRequest.setClientName(report.getCompany().getName());
			reportMasterRequest.setDescription(report.getDescription());
			reportMasterRequest.setName(report.getTemplate().getName());
			reportMasterRequest.setStatus(report.getStatus());
			if(report.getIsActive()) {
				reportMasterRequest.setIsActive("Active");
			}else{
				reportMasterRequest.setIsActive("InActive");
			}
			if (report.getApprovedBy() != null) {
				reportMasterRequest.setApprovedBy(userRepo.getOne(report.getApprovedBy()).getFirstName() + " "
						+ userRepo.getOne(report.getApprovedBy()).getLastName());
			}
			if (report.getReviewedBy() != null) {
				reportMasterRequest.setReviewedBy(userRepo.getOne(report.getReviewedBy()).getFirstName() + " "
						+ userRepo.getOne(report.getReviewedBy()).getLastName());
			}
			reportMasterRequest.setReviewedDate(report.getReviewedDate());
			reportMasterRequest.setApprovedDate(report.getApprovedDate());
			reportMasterRequest.setCreatedBy(userRepo.getOne(report.getCreatedBy()).getFirstName() + " "
					+ userRepo.getOne(report.getCreatedBy()).getLastName());
			reportMasterRequest.setCreatedDate(report.getCreatedDate());
			if (report.getUpdatedBy() != null) {
				reportMasterRequest.setUpdatedBy(userRepo.getOne(report.getUpdatedBy()).getFirstName() + " "
						+ userRepo.getOne(report.getUpdatedBy()).getLastName());
			}
			reportMasterRequest.setUpdatedDate(report.getUpdatedDate());
			reportMasterRequest.setRole(userRepo.getOne(report.getCreatedBy()).getRole().getName());
			reportMasterRequests.add(reportMasterRequest);
		}

		return reportMasterRequests;
	}

	@Override
	public List<ReportMasterRequest> getInActiveReports() {
		List<Report> reports = repo.getInActiveReport();
		List<ReportMasterRequest> reportMasterRequests = new ArrayList<>();

		for (Report report : reports) {
			ReportMasterRequest reportMasterRequest = new ReportMasterRequest();
			reportMasterRequest.setReportId(report.getId());
			reportMasterRequest.setClientName(report.getCompany().getName());
			reportMasterRequest.setDescription(report.getDescription());
			reportMasterRequest.setName(report.getTemplate().getName());
			reportMasterRequest.setStatus(report.getStatus());
			if(report.getIsActive()) {
				reportMasterRequest.setIsActive("Active");
			}else{
				reportMasterRequest.setIsActive("InActive");
			}
			if (report.getApprovedBy() != null) {
				reportMasterRequest.setApprovedBy(userRepo.getOne(report.getApprovedBy()).getFirstName() + " "
						+ userRepo.getOne(report.getApprovedBy()).getLastName());
			}
			if (report.getReviewedBy() != null) {
				reportMasterRequest.setReviewedBy(userRepo.getOne(report.getReviewedBy()).getFirstName() + " "
						+ userRepo.getOne(report.getReviewedBy()).getLastName());
			}
			reportMasterRequest.setReviewedDate(report.getReviewedDate());
			reportMasterRequest.setApprovedDate(report.getApprovedDate());
			reportMasterRequest.setCreatedBy(userRepo.getOne(report.getCreatedBy()).getFirstName() + " "
					+ userRepo.getOne(report.getCreatedBy()).getLastName());
			reportMasterRequest.setCreatedDate(report.getCreatedDate());
			if (report.getUpdatedBy() != null) {
				reportMasterRequest.setUpdatedBy(userRepo.getOne(report.getUpdatedBy()).getFirstName() + " "
						+ userRepo.getOne(report.getUpdatedBy()).getLastName());
			}
			reportMasterRequest.setUpdatedDate(report.getUpdatedDate());
			reportMasterRequest.setRole(userRepo.getOne(report.getCreatedBy()).getRole().getName());
			reportMasterRequests.add(reportMasterRequest);
		}

		return reportMasterRequests;
	}
	
	@Override
	public ResponseEntity<ResponseMessage> createReport(ReportMasterRequest reportMasterRequest) {
		LOGGER.debug("...in .Report Service start.");
		try {
			Report report = new Report();
			report.setId(reportMasterRequest.getReportId());
			report.setCompany(companyRespository.findOneByName(reportMasterRequest.getClientName()));
			report.setDescription(reportMasterRequest.getDescription());
			report.setTemplate(templateRepository.findOneByName(reportMasterRequest.getName()));
			report.setReportName(reportMasterRequest.getName());
			if(reportMasterRequest.getIsActive().equals("Active")) {
				report.setIsActive(true);
			}else {
				if(reportMasterRequest.getReportId() != null)
				{
					List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportId(reportMasterRequest.getReportId());
					for(Long id: reportDetailsId) {
						Long reportDetailId = reportDetailService.inActivateReportDetail(id);
					}
				}
				report.setIsActive(false);
			}
			Long modifiedBy = WebUtil.getLoggedInUserId();
			if (reportMasterRequest.getReportId() == null) {
				report.setCreatedBy(modifiedBy);
				report.setCreatedDate(new Date());
				report.setStatus(AppConstants.NOTIFICATION_STATUS_NEW);
			} else {
				report.setCreatedBy(repo.getOne(reportMasterRequest.getReportId()).getCreatedBy());
				report.setCreatedDate(repo.getOne(reportMasterRequest.getReportId()).getCreatedDate());
				report.setUpdatedBy(modifiedBy);
				report.setUpdatedDate(new Date());
				report.setStatus(repo.getOne(reportMasterRequest.getReportId()).getStatus());
				
			}
			repo.save(report);
			return new ResponseEntity<>(new ResponseMessage("Report created", "Report Created/Updated Successfully"),
					HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("EROR " + e.getMessage());
			return null;
		}
	}

	@Override
	public void sendReportForReview(ReportMasterRequest reportMasterRequest) {
		Report report = repo.getOne(reportMasterRequest.getReportId());
		report.setStatus(AppConstants.NOTIFICATION_STATUS_REVIEW);
		repo.save(report);
		notificationService.setNotificationRequest(reportMasterRequest.getReportId(), AppConstants.TYPE_REPORT);
	}

	@Override
	public void reportReviewed(ReportMasterRequest reportMasterRequest, String reportComment) {
		Report report = repo.getOne(reportMasterRequest.getReportId());
		report.setStatus(AppConstants.NOTIFICATION_STATUS_APPROVE);
		report.setReviewedBy(WebUtil.getLoggedInUserId());
		report.setReviewedDate(new Date());
		repo.save(report);
		notificationRepo.updateStatusOnReview(AppConstants.NOTIFICATION_STATUS_REVIEWED, false, reportComment,
				report.getId(), AppConstants.TYPE_REPORT);
		notificationService.setNotificationRequest(reportMasterRequest.getReportId(), AppConstants.TYPE_REPORT);
	}

	@Override
	public void reportApproved(ReportMasterRequest reportMasterRequest, String reportComment) {
		Report report = repo.getOne(reportMasterRequest.getReportId());
		report.setStatus(AppConstants.NOTIFICATION_STATUS_COMPLETED);
		report.setApprovedBy(WebUtil.getLoggedInUserId());
		report.setApprovedDate(new Date());
		repo.save(report);
		notificationRepo.updateStatusOnApprove(AppConstants.NOTIFICATION_STATUS_COMPLETED, true, reportComment,
				report.getId(), AppConstants.TYPE_REPORT, AppConstants.NOTIFICATION_STATUS_APPROVE);
		notificationRepo.updateStatusOnComplete(AppConstants.NOTIFICATION_STATUS_REVIEWED, true, report.getId(),
				AppConstants.TYPE_REPORT);
	}
	
	@Override
	public Long deleteReport(long id) {
		long deleteId = id;
		List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportId(deleteId);
		for(Long reportDetailId: reportDetailsId) {
			reportDetailRepo.deleteById(reportDetailId);
		}
		repo.deleteById(id);
		return deleteId;
	}
	
	public Long inActivateReport(long id) {
		Report dbReport = new Report();
		Optional<Report> report = repo.findById(id);
		if(report.isPresent())
		{
			Report current = report.get();
			current.setIsActive(false);
			repo.save(current);
		}
		return id;
	}
	
	//  REJECT FOR REPORT
			
	@Override
	public void reportRejectedOnReview(ReportMasterRequest reportMasterRequest) {
		Report report = repo.getOne(reportMasterRequest.getReportId());
		report.setStatus(AppConstants.NOTIFICATION_STATUS_REJECTED_ON_REVIEW);
		repo.save(report);
		notificationRepo.rejectedDocument(AppConstants.TYPE_REPORT, reportMasterRequest.getReportId());
	}
	
	@Override
	public void reportRejectedOnApprove(ReportMasterRequest reportMasterRequest) {
		Report report = repo.getOne(reportMasterRequest.getReportId());
		report.setStatus(AppConstants.NOTIFICATION_STATUS_REJECTED_ON_APPROVE);
		repo.save(report);
		notificationRepo.rejectedDocument(AppConstants.TYPE_REPORT,reportMasterRequest.getReportId());
	}
	
	@Override
	public void reportReEdit(ReportMasterRequest reportMasterRequest) {
		Report report = repo.getOne(reportMasterRequest.getReportId());
		report.setStatus(AppConstants.NOTIFICATION_STATUS_NEW);
		report.setReviewedBy(null);
		report.setReviewedDate(null);
		report.setApprovedBy(null);
		report.setApprovedDate(null);
		 repo.save(report);
	}
 
}


