package com.knowarth.rimscp.service.company.report;

import java.util.List;

import com.knowarth.rimscp.web.model.report.ReportSectionDropdownRequest;
import com.knowarth.rimscp.web.model.report.ReportSectionRequest;
import com.knowarth.rimscp.web.model.report.TemplateRequest;

public interface IReportSectionService {

	/**
	 * Create report Section
	 * @param ReportSectionRequest
	 * @return
	 */
	public Long createReportSection(ReportSectionRequest reportSectionRequest);
	
	/**
	 * GET report Sections
	 * @return List ReportSection
	 */
	public List<ReportSectionRequest> getAllReportSection();
	
	public List<ReportSectionRequest> getInActiveReportSection();
	
	
	/**
	 * GET report Section Name
	 * @return List || ReportSection Name.
	 */
	public List<String> getReportSections();

	List<ReportSectionDropdownRequest> getReportSectionForDropdown();
	
	public Long deactivateSection(long id);

	public Long activateSection(long id);

	public Long deleteSection(long id); 
}
