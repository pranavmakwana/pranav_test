package com.knowarth.rimscp.service.company.aws;

import java.util.List;

import com.knowarth.rimscp.web.model.aws.RDSDetail;

/**
 * User Service
 *
 * @author arkan.malik
 */
public interface IRDSService {
	/**
	 * // * @return retrieves all RDS Details //
	 */
	public List<RDSDetail> getRDSDetails(String id);
}
