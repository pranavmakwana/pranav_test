package com.knowarth.rimscp.service.company.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.knowarth.rimscp.db.model.Template;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.ReportActivityRepository;
import com.knowarth.rimscp.repository.ReportDetailRepository;
import com.knowarth.rimscp.repository.ReportRepository;
import com.knowarth.rimscp.repository.ReportSectionRepository;
import com.knowarth.rimscp.repository.TemplateRepository;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.report.TemplateRequest;

@Service
public class TemplateService implements ITemplateService {

	@Autowired
	private TemplateRepository templateRepo;
	
	@Autowired
	private ReportRepository reportRepo;
	
	@Autowired
	private ReportService reportService;
	
	@Autowired
	private ReportSectionService sectionService;
	
	@Autowired
	private ReportSectionRepository sectionRepo;
	
	@Autowired
	private ReportActivityRepository reportActivityRepo;
	
	@Autowired
	private ReportActivityService reportActivityService;
	
	@Autowired
	private ReportDetailService reportDetailService;
	
	@Autowired 
	private ReportDetailRepository reportDetailRepo;
	
	@Override
	public Long createTemplate(TemplateRequest templateRequest) {
		
		Template template = new Template();

		template.setId(templateRequest.getId());
		template.setName(templateRequest.getName());
		template.setDescription(templateRequest.getDescription());
		
		if(templateRequest.getIsActive().equals("Active")) {
			template.setIsActive(true);
		}else {
			if(templateRequest.getId() != null)
			{
				List<Long> reportsId = reportRepo.getReportbyTemplateId(templateRequest.getId());
				for(Long id: reportsId) {
					Long reportId = reportService.inActivateReport(id);
					List<Long> reportDetailsId = reportDetailRepo.getReportDetailsbyReportId(reportId);
					for(Long reportDetailId: reportDetailsId) {
						Long report_Detail_Id = reportDetailService.inActivateReportDetail(reportDetailId);
					}
				}
				
				List<Long> sectionsId = sectionRepo.getSectionbyTemplateId(templateRequest.getId());
				for(Long id: sectionsId) {
					Long sectionId = sectionService.deactivateSection(id);
					List<Long> reportActivitiesId = reportActivityRepo.getReportActivitybySectionId(id);
					for(Long activityId: reportActivitiesId) {
						Long reportDetailId = reportActivityService.inActivateActivity(activityId);
					}
				}
			}
			template.setIsActive(false);
		}
		
		Long modifiedBy = WebUtil.getLoggedInUserId();
		if (templateRequest.getId() == null) {
			template.setCreatedBy(modifiedBy);
			template.setCreatedDate(new Date());
		} else {
			template.setCreatedBy(templateRepo.getOne(templateRequest.getId()).getCreatedBy());
			template.setCreatedDate(templateRepo.getOne(templateRequest.getId()).getCreatedDate());
			template.setUpdatedBy(modifiedBy);
			template.setUpdatedDate(new Date());
		}

		templateRepo.save(template);
		return template.getId();
	}

	@Override  
	public List<TemplateRequest> getTemplates() {

		List<Template> templates = templateRepo.getActiveTemplates();
		List<TemplateRequest> templateRequests = new ArrayList<>(templates.size());
		for (Template template : templates) {
			templateRequests.add(getTemplateRequest(template));
		}

		return templateRequests;
	}

	private TemplateRequest getTemplateRequest(Template template) {
		TemplateRequest templateRequest = new TemplateRequest();
		templateRequest.setId(template.getId());
		if(template.getIsActive()) {
			templateRequest.setIsActive("Active");
		}else{
			templateRequest.setIsActive("InActive");
		}
		templateRequest.setName(template.getName());
		templateRequest.setDescription(template.getDescription());

		return templateRequest;
	}

	@Override
	public Long deleteTemplate(long id) {
		long deleteId = id;
		templateRepo.deleteById(id);
		return deleteId;
	}

	@Override
	public List<TemplateRequest> getInActiveTemplates() {
		List<Template> templates = templateRepo.getInActiveTemplates();
		List<TemplateRequest> templateRequests = new ArrayList<>(templates.size());
		for (Template template : templates) {
			templateRequests.add(getTemplateRequest(template));
		}

		return templateRequests;
	}

	@Override
	public Long deactivateTemplate(long id) {
		Optional<Template> template = templateRepo.findById(id);
		if(template.isPresent())
		{
			Template currentTemplate = template.get();
			currentTemplate.setIsActive(false);
			templateRepo.save(currentTemplate);
		}
		return id;
	}

	@Override
	public Long activateTemplate(long id) {
		Template dbUser = new Template();
		Optional<Template> template = templateRepo.findById(id);
		if(template.isPresent())
		{
			Template currentTemplate = template.get();
			currentTemplate.setIsActive(true);
			templateRepo.save(currentTemplate);
		}
		return id;
	}

}
