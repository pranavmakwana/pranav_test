package com.knowarth.rimscp.service.company.aws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.knowarth.rimscp.service.zoho.ZohoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.AmazonRDSClientBuilder;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.service.user.IUserService;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.RIMSUtil;
import com.knowarth.rimscp.web.model.aws.RDSDetail;

/**
 * RDS Service Implementation
 * 
 * @author arkan.malik
 */
@Service
public class RDSService implements IRDSService {

	@Autowired
	private IUserService userService;

	@Autowired
	private CompanyRespository companyRepo;

	private static final Logger LOGGER =  LoggerFactory.getLogger(RDSService.class);

	@Override
	@Cacheable(value = "rdsdetail", key = "#id")
	public List<RDSDetail> getRDSDetails(String id) {
		 
			
			List<RDSDetail> rdsList = new ArrayList<>();

			if (userService.isCurrentUserCustomer()) {
				Company company = userService.getCurrentUserCompany();
				if (company != null && RIMSUtil.isNotBlankOrNull(company.getRegion()) && company.getIsActive()) {
					rdsList.addAll(callRDSInstance(company.getName() ,company.getRegion(), company.getSecret(), company.getClientId()));
				}

			} else {
				List<Company> listCompany = companyRepo.findAll();
				for (Company tCompany : listCompany) {
					if(tCompany.getIsActive()) {						
						rdsList.addAll(callRDSInstance(tCompany.getName() ,tCompany.getRegion(), tCompany.getSecret(), tCompany.getClientId()));
					}
				}
			}

			return rdsList; 
			 
	}


//	@Cacheable(AppConstants.CACHE_KEY_RDS_DETAILS)
	private List<RDSDetail> callRDSInstance(String companyName,String pRegion, String pSecretKey, String pClientId) {

		 
			List<RDSDetail> rdsList = new ArrayList<>();
			List<String> result = Arrays.asList(pRegion.split("\\s*,\\s*"));
			if (!CollectionUtils.isEmpty(result)) {
				for (String pStr : result) {
					List<RDSDetail> tRdsList = callRdsForRegion(companyName,pStr, pClientId, pSecretKey);
					if (!CollectionUtils.isEmpty(tRdsList)) {
						rdsList.addAll(tRdsList);
					}
				}
			}
			return rdsList;
 
	}

	private List<RDSDetail> callRdsForRegion(String  companyName,String pRegion, String pClientId, String pSecretKey) {
		 
			List<RDSDetail> rdsList = new ArrayList<>();
			final AmazonRDSClient rds = (AmazonRDSClient) AmazonRDSClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(basicCredentials(pClientId, pSecretKey)))
					.withRegion(Regions.fromName(pRegion)).build();

			boolean done = false;
			DescribeDBInstancesRequest request = new DescribeDBInstancesRequest();
			while (!done) {
				DescribeDBInstancesResult response = rds.describeDBInstances(request);
				for (DBInstance instance : response.getDBInstances()) {
					RDSDetail rdsObj = new RDSDetail();
					validateAndPrepareRDSObject(companyName,instance, rdsObj);
					rdsList.add(rdsObj);
				}

				request.setMarker(response.getMarker());
				if (response.getMarker() == null) {
					done = true;
				}
			}
				 
			return rdsList; 
	}

	/**
	 * @param instance
	 * @param rdsObj
	 */
	private void validateAndPrepareRDSObject(String companyName,DBInstance instance, RDSDetail rdsObj) {
		 	if (instance != null) {
				 
				if (RIMSUtil.isNotBlankOrNull(instance.getCACertificateIdentifier())) {
					rdsObj.setIdentifier(instance.getCACertificateIdentifier());
				}
				if (RIMSUtil.isNotBlankOrNull(instance.getAvailabilityZone())) {
					rdsObj.setAvailabilityZone(instance.getAvailabilityZone());
				}
				
				if (RIMSUtil.hasId(instance.getBackupRetentionPeriod())) {
					rdsObj.setRetentionPeriod(instance.getBackupRetentionPeriod());
				}
				if (RIMSUtil.isNotBlankOrNull(instance.getStorageType())) {
					rdsObj.setStorageType(instance.getStorageType());
				}
				if (RIMSUtil.hasId(instance.getAllocatedStorage())) {
					rdsObj.setAllocatedStorage(instance.getAllocatedStorage());
				}

				if (RIMSUtil.isValid(instance.getStorageEncrypted())) {
					rdsObj.setStorageEncrypted(instance.getStorageEncrypted());
				}
				if (RIMSUtil.isValid(instance.getPubliclyAccessible())) {
					rdsObj.setPubliclyAccessible(instance.getPubliclyAccessible());
				}
				if (RIMSUtil.isNotBlankOrNull(instance.getEngine())) {
					rdsObj.setEngine(instance.getEngine());
				} 
				rdsObj.setStatus(instance.getDBInstanceStatus());
				rdsObj.setCompanyName(companyName);
				rdsObj.setCreatedTime(instance.getInstanceCreateTime());
				rdsObj.setLatestRestorableTime(instance.getLatestRestorableTime());
			}
			 
	}

	private static BasicAWSCredentials basicCredentials(String clientId, String secretKey) {
		  	LOGGER.info("RDS clientId  "+ clientId + " Secret Key" + secretKey);
			return new BasicAWSCredentials(clientId, secretKey);
		 
	}
}
