package com.knowarth.rimscp.service.zoho;

import java.util.List;

import com.knowarth.rimscp.web.model.company.MaintenanceRequest;

public interface IMaintenanceService {

	public Long createMaintenance(MaintenanceRequest maintenanceRequest);

	List<MaintenanceRequest> getMaintenance();

	public Long deleteMaintenance(long id);
	
	public void getMaintenanceFromTicketSummery();
}
