package com.knowarth.rimscp.service.zoho;

import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.TicketSummaryRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.company.report.ITicketService;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.zoho.TicketCreate;
import com.knowarth.rimscp.web.model.zoho.api.GetTicketDataRequest;
import com.knowarth.rimscp.web.model.zoho.api.TicketRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketService implements ITicketService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CompanyRespository companyRepository;

	@Autowired
	private TicketSummaryRepository ticketSummaryRepo;
	
	@Value("${zoho.url}")
	private String zohoUrl;

	@Value("${zoho.portalId}")
	private String portalId;

	@Value("${zoho.authToken}")
	private String authToken;

	public TicketRequest ticketRequest;
	
	
	private static final Logger LOGGER = Logger.getLogger(TicketService.class);


	public String getZohoBugsUrl(String projectKey, String authToken, String portalId, String zohoUrl, int indexValue) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/?authtoken=" + authToken + "&range=200&index=" + indexValue;
	}
	public String setZohoBugsUrl(String projectKey, String authToken, String portalId, String zohoUrl) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/?authtoken=" + authToken;
	}
	public String getZohoBugsUrl(String projectKey, String authToken, String portalId, String zohoUrl) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/?authtoken=" + authToken + "&range=200";

	}
	public String getZohoBugsUpdateUrl(String projectKey, String authToken, String portalId, String zohoUrl, String bugId) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/" + bugId + "/?authtoken=" + authToken;
	}

	public String getZohoDefaultFieldsUrl(String projectKey, String authToken, String portalId, String zohoUrl) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/defaultfields/?authtoken=" + authToken;
	}

	public String getOpenTicketsUrl(String projectKey, String authToken, String portalId, String zohoUrl, int indexValue) {
	 	return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/?authtoken=" + authToken
				+ "&statustype=open&range=200&index=" +indexValue;
	}
	
	public String getClosedTicketsUrl(String projectKey, String authToken, String portalId, String zohoUrl, int indexValue) {
	 	return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/?authtoken=" + authToken
				+ "&statustype=closed&range=200&index=" +indexValue;
	}

	public String getBugUrl(String projectKey, String authToken, String portalId, String zohoUrl, Long bugid) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/" + bugid + "/?authtoken=" + authToken;
	}

	public String getBugActivityUrl(String projectKey, String authToken, String portalId, String zohoUrl, Long bugid) {
		return zohoUrl + "/" + portalId + "/projects/" + projectKey + "/bugs/" + bugid + "/activities/?authtoken="
				+ authToken;
	} 

	
	@Override
//	@Cacheable(value = "ticketCount", key = "#id")
	public List<Long> getTicketCount(String id){
		String roleName = userRepository.getOne(WebUtil.getLoggedInUserId()).getRole().getName();
		List<Long> count = new ArrayList<>();
		LOGGER.debug("..............in Ticket_service_get_ticket start...................." + roleName);
		List<TicketRequest> allTicketRequest = new ArrayList<>();
		if(roleName.equals("Manager") || roleName.equals("Technician") || roleName.equals("Lead") ) {
			LOGGER.debug("..............in Ticket_get_ticket_if start....................");
			List<String> projectKeys = companyRepository.getAllProjectKeys();
			
			projectKeys.forEach(key ->{
				TicketRequest ticketRequest = new TicketRequest();
				int index = 0;
				Long all = 0L;
				Long open = 0L;
				Long close = 0L;
				Long inProgress = 0L;
				Boolean status = true;
				close = ticketSummaryRepo.getCloseCount();
				count.add(close);
//				do {
//					String url = getZohoBugsUrl(key, authToken, portalId, zohoUrl, (index * 200 + 1));
//					
//					ResponseEntity<TicketRequest> ticketResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null, TicketRequest.class);
//						ticketRequest = ticketResponseEntity.getBody();
//						if(ticketResponseEntity.getStatusCode().value() == 200 ) {
//							List<BugRequest> bug = ticketRequest.getBugs();
//							for(BugRequest ticket: bug) {
//								if(ticket.getStatus().getType().equals("Open")) {
//									all++;
//									open++;
//								}else if(ticket.getStatus().getType().equals("Closed") ){
//									all++;
//									close++;
//								}else if(ticket.getStatus().getType().equals("In progress")){
//									all++;
//									inProgress++;
//								}else {
//									all++;
//								}
//							}
//							allTicketRequest.add(ticketRequest); 
//							index++;
//						}
//					}while (status);
//				count.add(all);
//				count.add(open);
//				count.add(close);
//				count.add(inProgress);
			});
		}else {
			LOGGER.debug("..............in Ticket_get_ticket_else start....................");
			String projectKey = userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getProjectKey();
			Long companyId = userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getId();
			TicketRequest ticketRequest = new TicketRequest();
			int index = 0;
			Long all = 0L;
			Long open = 0L;
			Long close = 0L;
			Long inProgress = 0L;
			Boolean status = true;
			close = ticketSummaryRepo.getCloseCountFromCompany(companyId);
			count.add(close);
//			do {
//				String url = getZohoBugsUrl(projectKey, authToken, portalId, zohoUrl, (index * 200 + 1));
//				
//				ResponseEntity<TicketRequest> ticketResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null, TicketRequest.class);
//					ticketRequest = ticketResponseEntity.getBody();
//					if(ticketResponseEntity.getStatusCode().value() == 200 ) {
//						List<BugRequest> bug = ticketRequest.getBugs();
//						for(BugRequest ticket: bug) {
//							if(ticket.getStatus().getType().equals("Open")) {
//								all++;
//								open++;
//							}else if(ticket.getStatus().getType().equals("Closed")){
//								all++;
//								close++;
//							}else if(ticket.getStatus().getType().equals("In progress")){
//								all++;
//								inProgress++;
//							}
						}
//						allTicketRequest.add(ticketRequest); 
//						index++;
//					}else {
//						status = false;
//					}
//				}while (status);
//				count.add(all);
//				count.add(open);
//				count.add(inProgress);
//		}
		
		return count;
	}
	
	// TODO: pass ProjectID as Parameter as well as change function name to Open
	// Tickets and retrieve Open tickets only
	// i.e. for customer only customer tickets for Lead/Tech/Manager get all tickets
	// Implement Caching
	@Override
	@Cacheable(value = "TicketDetail" , key = "#id")
	public List<TicketRequest> getTickets(String id) {
		String roleName = userRepository.getOne(WebUtil.getLoggedInUserId()).getRole().getName();
		LOGGER.debug("..............in Ticket_service_get_ticket start...................." + roleName);
		List<TicketRequest> allTicketRequest = new ArrayList<>();
		if(roleName.equals("Manager") || roleName.equals("Technician") || roleName.equals("Lead") ) {
			LOGGER.debug("..............in Ticket_get_ticket_if start....................");
			List<String> projectKeys = companyRepository.getAllProjectKeys();

			projectKeys.forEach(key ->{
				TicketRequest ticketRequest = new TicketRequest();
				int index = 0;
				Boolean status = true;
				do {
					String url = getOpenTicketsUrl(key, authToken, portalId, zohoUrl, (index * 200 + 1));
					
					ResponseEntity<TicketRequest> ticketResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null, TicketRequest.class);
						ticketRequest = ticketResponseEntity.getBody();
						if(ticketResponseEntity.getStatusCode().value() == 200 ) {
							allTicketRequest.add(ticketRequest); 
							index++;
						}else {
							status = false;
						}
					}while (status);
			});
			

			return allTicketRequest;
		}else {
			LOGGER.debug("..............in Ticket_get_ticket_else start....................");
			if(userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getIsActive()) {
				String projectKey = userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getProjectKey();				
			TicketRequest ticketRequest = new TicketRequest();
			int index = 0;
			Boolean status = true;
			do {
				String url = getOpenTicketsUrl(projectKey, authToken, portalId, zohoUrl, (index * 200 + 1));
				
				ResponseEntity<TicketRequest> ticketResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null, TicketRequest.class);
					ticketRequest = ticketResponseEntity.getBody();
					if(ticketResponseEntity.getStatusCode().value() == 200 ) {
						allTicketRequest.add(ticketRequest); 
						index++;
					}else {
						status = false;
					}
				}while (status);
			return allTicketRequest;
			}else
			{
				return allTicketRequest;
			}
		}
	}
	
	// TODO: Get Project Fields based on ProjectID, pass as Parameter
	// Revisit this, it should be working based on ROLE
	// i.e. for customer only customer tickets for Lead/Tech/Manager get all tickets
	// Implement caching
	@Override
	public GetTicketDataRequest getTicketData() {
		String projectKey = userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getProjectKey();
		if(userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getIsActive()) {
			String url = getZohoDefaultFieldsUrl(projectKey, authToken, portalId, zohoUrl);
			return restTemplate.getForObject(url, GetTicketDataRequest.class);			
		}
		else {
			return null;
		}
	}

	@Override
	public TicketRequest createTicket(TicketCreate ticketCreate, MultipartFile file) {

		LOGGER.debug("..............in Ticket_Create_service start....................");
		LOGGER.info("..............in Ticket_Create_service start....................");
		try {
			String projectKey = userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getProjectKey();
			final String uri = setZohoBugsUrl(projectKey, authToken, portalId, zohoUrl);
			MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
			parameters.add("title", ticketCreate.getTitle());
			parameters.add("description", ticketCreate.getDescription());
			parameters.add("module_id", ticketCreate.getModuleId());
			parameters.add("severity_id", ticketCreate.getSeverityId());
			
			if(file!=null) {
				parameters.add("uploaddoc", file.getResource());
			}
			

			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(
					parameters, headers);

			// Create tickt
			ticketRequest = restTemplate.postForObject(uri, request,TicketRequest.class, TicketCreate.class);
			return ticketRequest;
		} catch (Exception e) {
			LOGGER.error("Error occurred on ticket service" + e.getMessage(), e);
			return null;
		}
		
	}
	@Override
	public void updateTicket(TicketCreate ticketCreate, MultipartFile file) {
		try {
			String projectKey = userRepository.getOne(WebUtil.getLoggedInUserId()).getCompany().getProjectKey();
			String bugId = ticketCreate.getBugId();
			final String uri = getZohoBugsUpdateUrl(projectKey, authToken, portalId, zohoUrl, bugId);
			MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
			parameters.add("title", ticketCreate.getTitle());
			parameters.add("description", ticketCreate.getDescription());
			parameters.add("module_id", ticketCreate.getModuleId());
			parameters.add("severity_id", ticketCreate.getSeverityId());
			
			if(file!=null) {
				parameters.add("uploaddoc", file.getResource());
			}
			

			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(
					parameters, headers);
			System.out.println(ticketCreate);
			// update tickt
			restTemplate.postForObject(uri, request, TicketCreate.class);

		} catch (Exception e) {
			LOGGER.error("Error occurred" + e.getMessage(), e);
		}
	}

}

