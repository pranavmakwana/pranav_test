package com.knowarth.rimscp.service.zoho;

import com.knowarth.rimscp.db.model.Maintenance;
import com.knowarth.rimscp.db.model.TicketSummary;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.MaintenanceRepository;
import com.knowarth.rimscp.repository.TicketSummaryRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.user.UserService;
import com.knowarth.rimscp.web.model.company.MaintenanceRequest;
import com.knowarth.rimscp.web.model.zoho.TicketCreate;
import com.knowarth.rimscp.web.model.zoho.api.BugRequest;
import com.knowarth.rimscp.web.model.zoho.api.TicketRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class MaintenanceService implements IMaintenanceService {
	@Autowired
	private MaintenanceRepository maintenanceRepo;

	@Autowired
	private TicketService ticketService;

	@Autowired
	private CompanyRespository companyRepo;

	@Autowired
	private TicketSummaryRepository ticketSummaryRepository;
	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private UserService userService;

	@Autowired
	private CompanyRespository companyRespository;

	@Value("${zoho.url}")
	private String zohoUrl;

	@Value("${zoho.portalId}")
	private String portalId;

	@Value("${zoho.authToken}")
	private String authToken;

	public TicketRequest ticketRequest;

	private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceService.class);

	@Override
	public Long createMaintenance(MaintenanceRequest maintenanceRequest) {
		LOGGER.debug("...in Maintenance Service...");
		try {
			Maintenance maintenance = new Maintenance();
			maintenance.setId(maintenanceRequest.getId());
			if (maintenanceRequest.getCompany() != null) {
				maintenance.setCompany(companyRepo.findOneByName(maintenanceRequest.getCompany()));
			}
			maintenance.setDeploymentDate(maintenanceRequest.getDeploymentDate());
			maintenance.setServerName(maintenanceRequest.getServerName());
			maintenance.setDescription(maintenanceRequest.getDescription());
			maintenance.setStartTime(maintenanceRequest.getStartTime());
			maintenance.setEndTime(maintenanceRequest.getEndTime());
			if (maintenanceRequest.getUser() != null) {
				maintenance.setUser(userRepo.getUserByFullName(maintenanceRequest.getUser()));
			}

			maintenance.setStatus(maintenanceRequest.getStatus());
			if (maintenanceRequest.getId() == null) {
				TicketCreate ticketCreate = new TicketCreate();
				ticketCreate.setModuleId(852608000013413246L);
				ticketCreate.setTitle(maintenance.getServerName());
				ticketCreate.setDescription(maintenance.getDescription());
//				ticketRequest = ticketService.createTicket(ticketCreate, null);
				maintenance.setBugId(Long.toString(ticketRequest.getBugs().get(0).getId()));
				maintenance.setTicketId(ticketRequest.getBugs().get(0).getKey());
//				System.out.println(".................................." + ticketRequest.getBugs().get(0).getKey());
			} else {
				TicketCreate ticketCreate = new TicketCreate();
				ticketCreate.setModuleId(852608000013413246L);
				ticketCreate.setTitle(maintenance.getServerName());
				ticketCreate.setDescription(maintenance.getDescription());
//				ticketCreate.setBugId(maintenanceRepo.findById(maintenanceRequest.getId()).get().getBugId());
				maintenance.setBugId(maintenanceRepo.findById(maintenanceRequest.getId()).get().getBugId());
				maintenance.setTicketId(maintenanceRepo.getOne(maintenanceRequest.getId()).getTicketId());
//				ticketService.updateTicket(ticketCreate, null);
			}
			maintenanceRepo.save(maintenance);
			return maintenance.getId();
		} catch (Exception e) {
			LOGGER.error("ERROR Maintenance Service : -" + e.getMessage());
			return null;
		}
	}

	@Override
	public List<MaintenanceRequest> getMaintenance() {
		List<Maintenance> maintenance = maintenanceRepo.getOpenMaintenance();
		List<MaintenanceRequest> maintenanceRequests = new ArrayList<>(maintenance.size());
		for (Maintenance m : maintenance) {
			if(companyRepo.getOne(m.getCompany().getId()).getIsActive()) {
				maintenanceRequests.add(getMaintenanceRequest(m));				
			}
		}
		return maintenanceRequests;
	}

	private MaintenanceRequest getMaintenanceRequest(final Maintenance m) {
		MaintenanceRequest maintenanceRequest = new MaintenanceRequest();
		maintenanceRequest.setId(m.getId());

		if (m.getCompany() != null) {
			maintenanceRequest.setCompany(m.getCompany().getName());
		}

		if(m.getServerName() == null) {
			maintenanceRequest.setServerName("-");
		}else {
			maintenanceRequest.setServerName(m.getServerName());			
		}
		maintenanceRequest.setDescription(m.getDescription());
		if(m.getStartTime() == null) {
			maintenanceRequest.setStartTime("-");
		}
		else {
			maintenanceRequest.setStartTime(m.getStartTime());			
		}
		if(m.getEndTime() == null)
		{
			maintenanceRequest.setEndTime("-");
		}else {
			maintenanceRequest.setEndTime(m.getEndTime());			
		}
		if(m.getDeploymentDate() == null)
		{
			maintenanceRequest.setDeploymentDate(LocalDate.of(1970, 01, 01));
		}else {			
			maintenanceRequest.setDeploymentDate(m.getDeploymentDate());
		}
		maintenanceRequest.setBugId(m.getBugId());
		if (m.getUser() != null) {
			maintenanceRequest.setUser(m.getUser().getFirstName() + ' ' + m.getUser().getLastName());
		}
		maintenanceRequest.setStatus(m.getStatus());
		maintenanceRequest.setTicketId(m.getTicketId());
		;
		return maintenanceRequest;
	}

	@Override
	public Long deleteMaintenance(long id) {
		long deleteId = id;
		maintenanceRepo.deleteById(id);
		return deleteId;
	}

	private String getProjectKey(){
		return userService.getCurrentUserCompany().getProjectKey();
	}
	private List<String> getAllCompaniesProjectKeys(){
		return companyRespository.getAllProjectKeys();
	}
	
//	@Scheduled(fixedDelayString = "300000")
	public void getMaintenanceFromTicketSummery() {
		List<TicketSummary> ticketSummaryOpen = ticketSummaryRepository.getOpenTicket(getAllCompaniesProjectKeys());
		List<TicketSummary> ticketSummaryClose = ticketSummaryRepository.getClosedTicket(getAllCompaniesProjectKeys());
		List<String> maintenanceTicketId = maintenanceRepo.getAllTicketId();
		closedMaintenance(ticketSummaryClose, maintenanceTicketId );
		openMaintenance(ticketSummaryOpen, maintenanceTicketId);
	}
	
	public void closedMaintenance(List<TicketSummary> ticketSummaryclose,List<String> maintenanceTicketId){
		for (TicketSummary closedBug : ticketSummaryclose) {
			for (String id : maintenanceTicketId) {
				if(id.equals(closedBug.getZohoId().toString()) ) {
					String status = maintenanceRepo.getStatusByTicketId(id);
					if(status.equals("Open")) {
						maintenanceRepo.setMaintenanceStatus(id);	
					}
				}
			}
		}
	}
	
	public void openMaintenance(List<TicketSummary> ticketSummaryOpen, List<String> maintenanceTicketId) {
		
			
		for (TicketSummary bug : ticketSummaryOpen) {
			Boolean status = true ;
			for (String id : maintenanceTicketId) {
				if(id.equals(bug.getZohoId().toString())) {
					status = false;
					continue;
				}
			}
			
			if(status) {
				
					Maintenance maintenance = new Maintenance();
					maintenance.setCompany(bug.getCompany());
					maintenance.setDescription(bug.getTitle());
					maintenance.setStatus(bug.getStatus());
					maintenance.setBugId(bug.getZohoId().toString());
					maintenance.setTicketId(bug.getTicketKey());
					maintenance.setDeploymentDate(null);
					maintenance.setStartTime(null);
					maintenance.setEndTime(null);
					maintenance.setServerName(null);
					maintenanceRepo.save(maintenance);
				
			}
		}
	}
	
//	@Scheduled(fixedDelayString = "300000")
	public void getMaintenanceFromZoho() {

		List<String> projectKeys = companyRepo.getAllProjectKeys();

		projectKeys.forEach(key -> {

			Long companyId = companyRepo.findByProjectKey(key).getId();

			String url = ticketService.getOpenTicketsUrl(key, authToken, portalId, zohoUrl,1);
//			String closedURL = ticketService.getClosedTicketsUrl(key, authToken, portalId, zohoUrl);

//			TicketRequest closedTicketRequest = getData(closedURL);
			TicketRequest ticketRequest = getData(url);

			List<String> ticketId = maintenanceRepo.getTicketId(companyId);
//			closeMaintenance(closedTicketRequest, ticketId);
			storeMaintenanceData(ticketRequest, ticketId, companyId);
		});
	}

	public TicketRequest getData(String url) {
		return restTemplate.getForObject(url, TicketRequest.class);
	}

	public void closeMaintenance(TicketRequest closedTicketRequest, List<String> ticketId) {
		List<BugRequest> closedBugRequests = closedTicketRequest.getBugs();

		for (BugRequest closedBug : closedBugRequests) {
			for (String id : ticketId) {
				if (id.equals(closedBug.getId().toString())) {
					if (closedBug.getModule().getId().equals(852608000013413246L)) {
						maintenanceRepo.setMaintenanceStatus(id);
					}
				}
			}
		}
	}

	public void storeMaintenanceData(TicketRequest ticketRequest, List<String> ticketId, Long companyId) {

		Boolean status = true;
		List<BugRequest> bugRequests = ticketRequest.getBugs();

		for (BugRequest bug : bugRequests) {
			for (String id : ticketId) {
				if (id.equals(bug.getId().toString())) {
					status = false;
				}
			}

			if (status == true) {
				if (bug.getModule().getId().equals(852608000013413246L)) {
//					Maintenance maintenance = new Maintenance();
//					maintenance.setCompany(companyRepo.getOne(companyId));
//					maintenance.setDeploymentDate(null);
////					maintenance.setServerName(serverName);
//					maintenance.setDescription(bug.getDescription());
//					maintenance.setStatus(bug.getStatus().getType());
//					maintenance.setBugId(bug.getId().toString());
//					maintenance.setTicketId(bug.getKey());
//
//					maintenanceRepo.save(maintenance);
				}

			}
		}
	}

}
