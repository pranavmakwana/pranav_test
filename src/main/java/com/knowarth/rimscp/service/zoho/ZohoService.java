package com.knowarth.rimscp.service.zoho;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.db.model.TicketSummary;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.TicketSummaryRepository;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.zoho.api.BugActivityDetailRequest;
import com.knowarth.rimscp.web.model.zoho.api.BugActivityRequest;
import com.knowarth.rimscp.web.model.zoho.api.BugRequest;
import com.knowarth.rimscp.web.model.zoho.api.TicketRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.*;


/**
 * This is service used to populate Zoho details and dumps in database All zoho
 * ticket related details are used plot dashboard diagrams
 *
 * @author vinit.prajapati
 */
@Service
public class ZohoService {

    private static final Logger LOGGER = Logger.getLogger(ZohoService.class);

    @Value("${zoho.url}")
    private String zohoUrl;

    @Value("${zoho.portalId}")
    private String portalId;

    @Value("${zoho.authToken}")
    private String authToken;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private CompanyRespository companyRespository;

    @Autowired
    private TicketSummaryRepository ticketSummaryRepository;

    // TODO: Run every hour, take this from properties file
    @Scheduled(fixedDelayString = "300000") //3600000 for 1 hour
    public void updateZohoTickets() {
        LOGGER.info("Updating Zoho details...");
        List<String> projectKeys = companyRespository.getAllProjectKeys();

        for (final String projectKey : projectKeys) {
            // Fetch ZohoTickets & save them in database
            dumpZohoTickets(projectKey);
        }

    }

    private void dumpZohoTickets(final String projectKey) {
        Company company = companyRespository.findByProjectKey(projectKey);
        Set<Long> existingTicketIds = ticketSummaryRepository.getAllOpenTicketIds(company.getId());    ///  1 2 3 4 5
        TicketRequest ticketRequest;
        List<TicketRequest> allTicketRequest = new ArrayList<>();
        int index = 0;
        Boolean status = true;
        do {
            String url = ticketService.getOpenTicketsUrl(projectKey, authToken, portalId, zohoUrl, (index * 200 + 1));

            ResponseEntity<TicketRequest> ticketResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null, TicketRequest.class);
            ticketRequest = ticketResponseEntity.getBody();
            if (ticketResponseEntity.getStatusCode().value() == 200) {
                allTicketRequest.add(ticketRequest);
                index++;
            } else {
                status = false;
            }
        } while (status);
//        String url = ticketService.getOpenTicketsUrl(projectKey, authToken, portalId, zohoUrl);
//        TicketRequest ticketRequest = restTemplate.getForObject(url, TicketRequest.class);
        if (CollectionUtils.isEmpty(allTicketRequest)) {

            updateCloseTickets(projectKey, existingTicketIds);//if ticket rqst null meaning all tickets are closed therefore update them in db
        } else {
            Set<Long> recentTicketIDs = new HashSet<>();
            allTicketRequest.forEach(ticketRequest1 -> {
                List<BugRequest> tickets = ticketRequest1.getBugs();
                for (BugRequest ticket : tickets) {
                    recentTicketIDs.add(ticket.getId());// get all latest tickets from zoho remote which are open
                }
            });


            Set<Long> copyOfRecentTicketIDs = new HashSet<>(recentTicketIDs);

            // Remove Existing Tickets,
            // So now, recentTicketIDs contains only new ticket
            // which needs to be added in DB
            recentTicketIDs.removeAll(existingTicketIds);

            // Save new tickets
            for (Long bugid : recentTicketIDs) {
                String bugUrl = ticketService.getBugUrl(projectKey, authToken, portalId, zohoUrl, bugid);
                TicketRequest ticketRequest1 = restTemplate.getForObject(bugUrl, TicketRequest.class);
                List<BugRequest> bugRequests = ticketRequest1.getBugs();
                for (BugRequest bug : bugRequests) {
                    TicketSummary ticketSummary = new TicketSummary();
                    ticketSummary.setCompany(companyRespository.findByProjectKey(projectKey));
                    ticketSummary.setCreatedDate(new Date(bug.getCreatedTimeLong()));
                    ticketSummary.setEscalationLevel(bug.getEscalationLevel());
                    ticketSummary.setModule(bug.getModule().getName());
                    ticketSummary.setSeverity(bug.getSeverity().getType());
                    ticketSummary.setStatus(bug.getStatus().getType());
                    ticketSummary.setTitle(bug.getTitle());
                    ticketSummary.setTicketKey(bug.getKey());
                    ticketSummary.setZohoId(bug.getId());
                    ticketSummary.setProjectKey(projectKey);
                    ticketSummary.setClosed(bug.getClosed());
                    ticketSummaryRepository.save(ticketSummary);
                }
            }

            // Remove open tickets
            // SO this contains ticket which all closed //12345 and copyofrecent 23456 1 has been closed
            existingTicketIds.removeAll(copyOfRecentTicketIDs);// we have all closed tickets here
            //only close tickets if they exists in existingTicketIds

            List<Long> existingTicketIdsList = new ArrayList<>(existingTicketIds);
            //check if existingTicketIds (bugid in zoho) belongs to this projectkey if not iterate with other project key to check the same.
            if (!ticketSummaryRepository.isExistingTicketOfProjectKey(projectKey, existingTicketIdsList).isEmpty()){
                if (!existingTicketIds.isEmpty()){
                    updateCloseTickets(projectKey, existingTicketIds);
                }
            }
        }
    }

    /**
     * Update Resolution Time & Response Time By calling Activities
     *
     * @param closedTicketIds
     */
    private void updateCloseTickets(final String projectKey, final Set<Long> closedTicketIds) {

        try {
            List<Long> closedTicketIdsList = new ArrayList<>(closedTicketIds);
            if (!ticketSummaryRepository.isExistingTicketOfProjectKey(projectKey, closedTicketIdsList).isEmpty()){
                for (Long ticketID : closedTicketIds) {
                    TreeSet<Long> sortedActionTimeForResponse = new TreeSet<>();
                    TreeSet<Long> sortedActionTimeForResoultion = new TreeSet<>();
                    // Fetch Activities for this ticket Ids
                    String actvityUrl = ticketService.getBugActivityUrl(projectKey, authToken, portalId, zohoUrl, ticketID);
                    LOGGER.debug(" ***********  " + actvityUrl);
                    BugActivityRequest bugActivityRequest = restTemplate.getForObject(actvityUrl, BugActivityRequest.class);

                    if (bugActivityRequest != null) {
                        List<BugActivityDetailRequest> bugActivityDetailRequests = bugActivityRequest.getActivityDetails();

                        for (BugActivityDetailRequest bugActivity : bugActivityDetailRequests) {
                            if (bugActivity.getType().equals("comment")) {
                                sortedActionTimeForResponse.add(bugActivity.getActionTimeLong());
                            }
                        }
                        for (BugActivityDetailRequest bugActivity : bugActivityDetailRequests) {
                            if (bugActivity.getType().equals("status")) {
                                sortedActionTimeForResoultion.add(bugActivity.getActionTimeLong());
                            }
                        }
                    }

                    TicketSummary closedTicket = ticketSummaryRepository.findByZohoId(ticketID);
                    closedTicket.setStatus("Closed");
                    closedTicket.setResponseTime(
                            WebUtil.diffBetweenTwoDates(closedTicket.getCreatedDate(),new Date(sortedActionTimeForResponse.first())));
                    LOGGER.info("---------response time- {}");
                    closedTicket.setResolutionTime(
                            WebUtil.diffBetweenTwoDates(closedTicket.getCreatedDate(),new Date(sortedActionTimeForResoultion.last())));
                    LOGGER.info("---------resolution time-{}");
                    closedTicket.setClosed(true);
                    ticketSummaryRepository.save(closedTicket);
                }
            }

        } catch (HttpClientErrorException e) {
            LOGGER.error("---------Error in UpdateClosedTickets " + e.getMessage(), e);
        }

    }
    public void saveAllClosedTickets(String projectKey){
        Company company = companyRespository.findByProjectKey(projectKey);
        Set<Long> existingClosedTicketIds = ticketSummaryRepository.getAllClosedTicketIds(company.getId());
        TicketRequest ticketRequest;
        List<TicketRequest> allTicketRequest = new ArrayList<>();
        int index = 0;
        Boolean status = true;
        do {
            String url = ticketService.getOpenTicketsUrl(projectKey, authToken, portalId, zohoUrl, (index * 200 + 1));

            ResponseEntity<TicketRequest> ticketResponseEntity = restTemplate.exchange(url, HttpMethod.GET, null, TicketRequest.class);
            ticketRequest = ticketResponseEntity.getBody();
            if (ticketResponseEntity.getStatusCode().value() == 200) {
                allTicketRequest.add(ticketRequest);
                index++;
            } else {
                status = false;
            }
        } while (status);

        Set<Long> recentClosedTicketIDs = new HashSet<>();
        allTicketRequest.forEach(ticketRequest1 -> {
            List<BugRequest> tickets = ticketRequest1.getBugs();
            for (BugRequest ticket : tickets) {
                recentClosedTicketIDs.add(ticket.getId());// get all latest tickets from zoho remote which are open
            }
        });

        recentClosedTicketIDs.removeAll(existingClosedTicketIds);
        for (Long bugid : recentClosedTicketIDs) {
            String bugUrl = ticketService.getBugUrl(projectKey, authToken, portalId, zohoUrl, bugid);
            TicketRequest ticketRequest1 = restTemplate.getForObject(bugUrl, TicketRequest.class);
            List<BugRequest> bugRequests = ticketRequest1.getBugs();
            for (BugRequest bug : bugRequests) {
                TicketSummary ticketSummary = new TicketSummary();
                ticketSummary.setCompany(companyRespository.findByProjectKey(projectKey));
                ticketSummary.setCreatedDate(new Date(bug.getCreatedTimeLong()));
                ticketSummary.setEscalationLevel(bug.getEscalationLevel());
                ticketSummary.setModule(bug.getModule().getName());
                ticketSummary.setSeverity(bug.getSeverity().getType());
                ticketSummary.setStatus(bug.getStatus().getType());
                ticketSummary.setTitle(bug.getTitle());
                ticketSummary.setTicketKey(bug.getKey());
                ticketSummary.setZohoId(bug.getId());
                ticketSummary.setProjectKey(projectKey);
                ticketSummary.setClosed(bug.getClosed());
                ticketSummaryRepository.save(ticketSummary);
            }
        }
    }

    @Async
    public void example() throws InterruptedException {
        System.out.println("Started artificial delay for 5 seconds");
        Thread.sleep(5000);
        System.out.println("Stopped artificial delay for 5 seconds");
    }
}
