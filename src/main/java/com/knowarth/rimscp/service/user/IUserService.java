package com.knowarth.rimscp.service.user;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.web.model.user.UserRequest;

/**
 * User Service
 *
 * @author vinit.prajapati
 */
public interface IUserService {

	/**
	 * Create user
	 *
	 * @param userRequest
	 * @return generated userid
	 */
	public Long createUser(UserRequest userRequest);

	/**
	 * Loads currently logged in User profile
	 *
	 * @return UserRequest
	 */
	public UserRequest getMyProfile();

	/**
	 * Fetches from Roles database
	 *
	 * @return List of roles
	 */
	public List<String> getRoles();

	/**
	 * // * @return retrieves all users //
	 */
	public List<UserRequest> getUsers();

	public String setUserProfile(MultipartFile profileLogo);

	public byte[] getUserProfile(final String profile);

	byte[] getMyCompanyLogo();

	List<String> getOwners();

	byte[] getUserComapnyProfile(String profile);
	
	/**
	 * check current logged in user is customer or not
	 * 
	 * @return
	 */
	public boolean isCurrentUserCustomer();
	
	/**
	 * returns current user's company
	 * 
	 * @return
	 */
	public Company getCurrentUserCompany();

	public Long deleteUser(long id);

	public Long deactivateUser(long id);

	public Long activateUser(long id);

	public List<UserRequest> getInactiveUsers();

}
