package com.knowarth.rimscp.service.user;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.knowarth.rimscp.db.model.ForgotPasswordToken;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.ForgotPasswordTokenRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.controller.company.zoho.TicketController;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

@Service
public class ForgotPasswordTokenService implements IForgotPasswordTokenService {
	
	private static final Logger LOGGER = Logger.getLogger(ForgotPasswordTokenService.class);
	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private ForgotPasswordTokenRepository forgotPasswordRepo;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private EmailService emailService;

	@Override
	public boolean isValidToken(String token) {
		ForgotPasswordToken forgotPasswordToken = forgotPasswordRepo.findOneByToken(token);
		return forgotPasswordToken.getExpiryTime().after(new Date());

	}

	@Override
	public ResponseEntity<ResponseMessage> generateForgotPasswordToken(String email) { 
		try {
			User user = userRepo.findOneByEmail(email);
			if (user == null) {
				LOGGER.info("No user found for email - {} "+email);
				return null;
			} else if(user.getIsActive()) {
				ForgotPasswordToken token = new ForgotPasswordToken();
				token.setUser(user);
				token.setToken(WebUtil.getUUID());
				token.setExpiryTime(WebUtil.getCurrentTimePlus30Minutes());

				forgotPasswordRepo.save(token);

				// Send email : user.getEmail() && token.getToken()
				emailService.sendResetPasswordEmail(user.getEmail(), token.getToken());

				return new ResponseEntity<>(new ResponseMessage("Token generated successfully", token.getToken()),
						HttpStatus.OK);
			}
			else
			{
				LOGGER.info("user found is in activate - {} "+email);
				return null;
			}
			
		} catch (Exception e) {
			LOGGER.error("..............Error occured on Forgot password Service  .............." + e.getMessage(), e);
			return null;
		}
		
	}

	@Override
	public ResponseEntity<ResponseMessage> changePassword(String token, String newPassword) {
		ForgotPasswordToken forgotPasswordToken = forgotPasswordRepo.findOneByToken(token);
		if (forgotPasswordToken != null) {
			User user = forgotPasswordToken.getUser();
			user.setPassword(encoder.encode(newPassword));
			userRepo.save(user);
			return new ResponseEntity<>(new ResponseMessage("OK", "Password Reset Successfull"), HttpStatus.OK);
		}
		return null;
	}

	@Override
	public ResponseEntity<ResponseMessage> updatePassword(String newPassword) {
		Long id = WebUtil.getLoggedInUserId();
		User user = userRepo.getOne(id);
		if (user != null && user.getIsActive()) {
			user.setPassword(encoder.encode(newPassword));
			userRepo.save(user);
			return new ResponseEntity<>(new ResponseMessage("OK", "Password Updated Successfully"), HttpStatus.OK);
		}
		return null;
	}
}
