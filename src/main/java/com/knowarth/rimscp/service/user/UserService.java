package com.knowarth.rimscp.service.user;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.db.model.Company;
import com.knowarth.rimscp.db.model.Role;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.RoleRepository;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.service.company.report.FileStorageService;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.model.user.UserRequest;

/**
 * User Service Implementation
 * 
 * @author vinit.prajapati
 */
@Service
public class UserService implements IUserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private RoleRepository roleRepo;

	@Autowired
	private CompanyRespository companyRepo;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private FileStorageService fileStorageService;

	@Value("${user.profile.base.path}")
	private String userProfileBasePath;

	@Value("${company.base.path}")
	private String companyProfileBasePath;

	@Value("${company.default.logo}")
	private String defaultImage;

	/**
	 * Encode password
	 * 
	 * @param plainPassword
	 * @return
	 */
	private String encodePassword(final String plainPassword) {
		return encoder.encode(plainPassword);
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long createUser(UserRequest userRequest) {
		User user = new User();

		user.setId(userRequest.getId());
		user.setFirstName(userRequest.getFirstName());
		user.setLastName(userRequest.getLastName());
		user.setEmail(userRequest.getEmail());

		if (userRequest.getPassword() != null) {
			if (userRequest.getId() == null) {
				user.setPassword(encodePassword(userRequest.getPassword()));
			} else {
				String password = userRepo.getOne(userRequest.getId()).getPassword();
				if(password.equals(userRequest.getPassword()))
				{
					user.setPassword(userRequest.getPassword());
				}else {
					user.setPassword(encodePassword(userRequest.getPassword()));
				}
				
			}
		}
		
		if(userRequest.getIsActive().equals("Active")) {
			user.setIsActive(true);
		}else {
			user.setIsActive(false);
		}
		
		user.setPhone(userRequest.getPhone());
//		if(userRequest.getId() == null)
//		{
//			user.setIsActive(true);
//		}else {
//			Boolean status = userRepo.findOneByEmail(userRequest.getEmail()).getIsActive();
//			user.setIsActive(status);
//		}
		
		if (userRequest.getRole() != null) {
			user.setRole(roleRepo.findOneByName(userRequest.getRole()));
		}

		if (userRequest.getCompany() != null) {
			user.setCompany(companyRepo.findOneByName(userRequest.getCompany()));
		}

		Long modifiedBy = WebUtil.getLoggedInUserId();
		if (userRequest.getId() == null) {
			user.setCreatedBy(modifiedBy);
			user.setCreatedDate(new Date());
		} else {
			// FIXME: Aviod setting created details again & again
			user.setCreatedBy(userRepo.getOne(userRequest.getId()).getCreatedBy());
			user.setCreatedDate(userRepo.getOne(userRequest.getId()).getCreatedDate());
			user.setUpdatedBy(modifiedBy);
			user.setUpdatedDate(new Date());
		}

		userRepo.save(user);
		return user.getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserRequest getMyProfile() {
		Long currentUserid = WebUtil.getLoggedInUserId();
		Optional<User> userOp = userRepo.findById(currentUserid);
		UserRequest userRequest = null;

		if (userOp.isPresent() && userOp.get().getIsActive()) {
			User currentUser = userOp.get();
			userRequest = new UserRequest();
			userRequest.setId(currentUserid);
			userRequest.setFirstName(currentUser.getFirstName());
			userRequest.setLastName(currentUser.getLastName());
			userRequest.setEmail(currentUser.getEmail());
			userRequest.setProfile(currentUser.getProfile());

			userRequest.setPhone(currentUser.getPhone());
			userRequest.setRole(currentUser.getRole().getName());
			userRequest.setCompany(currentUser.getCompany().getName());

		}
		return userRequest;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getRoles() {
		List<Role> roles = roleRepo.findAll();
		return roles.stream().map(Role::getName).collect(Collectors.toList());
	}

	/**
	 * @return retrieves all Users
	 */
	@Override
	public List<UserRequest> getUsers() {
		List<User> users = userRepo.getActiveUser();
		List<UserRequest> userRequests = new ArrayList<>(users.size());
		for (User user : users) {
			userRequests.add(getUserRequest(user));
		}
		return userRequests;
	}

	private UserRequest getUserRequest(final User user) {
		UserRequest userRequest = new UserRequest();
		userRequest.setId(user.getId());
		userRequest.setFirstName(user.getFirstName());
		userRequest.setLastName(user.getLastName());
		userRequest.setEmail(user.getEmail());
		userRequest.setPassword(user.getPassword());
		userRequest.setPhone(user.getPhone());
		userRequest.setProfile(user.getProfile());
		if(user.getIsActive()) {
			userRequest.setIsActive("Active");
		}else{
			userRequest.setIsActive("InActive");
		}
		if (user.getRole() != null) {
			userRequest.setRole(user.getRole().getName());
		}
		if (user.getCompany() != null) {
			userRequest.setCompany(user.getCompany().getName());
		}
		return userRequest;
	}
	@Override
	public List<UserRequest> getInactiveUsers() {
		List<User> users = userRepo.getInActiveUser();
		List<UserRequest> userRequests = new ArrayList<>(users.size());
		for (User user : users) {
			userRequests.add(getUserRequest(user));
		}
		return userRequests;
	}
	// Set User Profile..

	@Override
	public String setUserProfile(MultipartFile profileLogo) {
		Long userId = WebUtil.getLoggedInUserId();
		Optional<User> userOp = userRepo.findById(userId);

		if (userOp.isPresent()) {
			User currentUser = userOp.get();
			String profileName = fileStorageService.storeUserProfile(userId, profileLogo);
			if (currentUser != null) {
				currentUser.setProfile(profileName);
				userRepo.save(currentUser);
				return profileName;
			}
		}
		return null;
	}

	@Override
	public byte[] getUserProfile(String profile) {
		try {
			Path userProfilePath = Paths.get(userProfileBasePath + profile).toAbsolutePath().normalize();
			InputStream in = Files.newInputStream(userProfilePath);
			return IOUtils.toByteArray(in);
		} catch (IOException e) {
			LOGGER.error("Error while fetching file " + e.getMessage(), e);
		}
		return null;
	}

	@Override
	public byte[] getMyCompanyLogo() {
		try {
			Long userId = WebUtil.getLoggedInUserId();
			String companyLogo = userRepo.getOne(userId).getCompany().getLogo();

			if (companyLogo != null && !companyLogo.isEmpty()) {
				Path userProfilePath = Paths.get(companyProfileBasePath + companyLogo).toAbsolutePath().normalize();
				InputStream in = Files.newInputStream(userProfilePath);
				return IOUtils.toByteArray(in);
			} else {
				Path userProfilePath = Paths.get(defaultImage + "companyDefault.png").toAbsolutePath().normalize();
				InputStream in = Files.newInputStream(userProfilePath);
				return IOUtils.toByteArray(in);
			}

		} catch (IOException e) {
			LOGGER.error("Error while fetching file " + e.getMessage(), e);
		}
		return null;
	}

	@Override
	public List<String> getOwners() {
		return userRepo.getUsersFullName();
	}

	@Override
	public byte[] getUserComapnyProfile(String profile) {
		return null;
	}

	@Override
	public boolean isCurrentUserCustomer() {
		Long userId = WebUtil.getLoggedInUserId();
		User user = userRepo.getOne(userId);

		return user.getRole().getName().equals(AppConstants.ROLE_CUSTOMER);
	}

	@Override
	public Company getCurrentUserCompany() {
		Long userId = WebUtil.getLoggedInUserId();
		User user = userRepo.getOne(userId);

		return user.getCompany();
	}
	
	@Override
	public Long deleteUser(long id) {
		long deleteId = id;
		userRepo.deleteById(id);
		return deleteId;
	}

	@Override
	public Long deactivateUser(long id) {
		User dbUser = new User();
		Optional<User> user = userRepo.findById(id);
		if(user.isPresent())
		{
			User currentUser = user.get();
			currentUser.setIsActive(false);
			userRepo.save(currentUser);
		}
		return id;
	}
	
	@Override
	public Long activateUser(long id) {
		User dbUser = new User();
		Optional<User> user = userRepo.findById(id);
		if(user.isPresent())
		{
			User currentUser = user.get();
			currentUser.setIsActive(true);
			userRepo.save(currentUser);
		}
		return id;
	}
}
