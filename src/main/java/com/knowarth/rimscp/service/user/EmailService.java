/**
 * 
 */
package com.knowarth.rimscp.service.user;

import java.io.StringWriter;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * @author parth.pithadia
 */

@Component
public class EmailService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	private JavaMailSender emailSender;
	
	@Autowired
	private VelocityEngine velocityEngine;

	@Value("${email.from}")
	private String emailFrom;

	@Value("${email.template.forgotpassword}")
	private String forgotPasswordTemplate;

	@Value("${email.company.logo.location}")
	private String companyLogoLocation;

	@Value("${email.fromPersonalName}")
	private String personalName;

	@Value("${email.subject}")
	private String emailSubject;

	@Value("${base.url.forgotpassword}")
	private String forgotPasswordBaseUrl;

	public void sendResetPasswordEmail(String to, String token) {
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message);
			mimeMessageHelper.setSubject(emailSubject);
			mimeMessageHelper.setTo(to);
			mimeMessageHelper.setFrom(emailFrom, personalName);

			StringWriter stringWriter = new StringWriter();
			VelocityContext velocityContext = new VelocityContext();

			velocityContext.put("resetPasswordLink", forgotPasswordBaseUrl.replace("{token}", token));
			velocityEngine.mergeTemplate(forgotPasswordTemplate, "UTF-8", velocityContext, stringWriter);

			BodyPart messageBodyPart = new MimeBodyPart();
			String htmlText = stringWriter.toString();
			messageBodyPart.setContent(htmlText, "text/html");

			MimeMultipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(companyLogoLocation);
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<companyLogo>");
			messageBodyPart.setHeader("Content-Type", "image/png; name=\"knowarth_logo.png\"");
			// add image to the multipart
			multipart.addBodyPart(messageBodyPart);

			// put everything together
			message.setContent(multipart);

			emailSender.send(message);
			LOGGER.debug("Reset Password Mail sent successfully");

		} catch (Exception exception) {
			LOGGER.error("Error in sending Reset Password Mail", exception);
		}
	}
}
