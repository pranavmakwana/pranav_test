package com.knowarth.rimscp.service.user;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.knowarth.rimscp.db.model.User;
import com.knowarth.rimscp.repository.UserRepository;
import com.knowarth.rimscp.web.model.user.RIMSUser;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		User user = userRepository.findOneByEmail(username);

		if (user != null) {
			return new RIMSUser(user);
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
}
