package com.knowarth.rimscp.service.user;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.knowarth.rimscp.web.model.report.DocumentRequest;

/**
 * Document Service
 * 
 */
public interface IDocumentService {

	/**
	 * Create Document
	 * 
	 * @param documentRequest
	 * @param file
	 * @return
	 */
	public Long createDocument(DocumentRequest documentRequest, MultipartFile file);

	/**
	 * get All Documents
	 * 
	 * @return
	 */
	public List<DocumentRequest> getDocuments();

	/**
	 * Download Document
	 * 
	 * @param name
	 * @param fileName
	 * @return
	 */
	public ResponseEntity<byte[]> getDocuments(String name, String fileName);

	public void sendDocumentForReview(DocumentRequest documentRequest);

	ResponseEntity<byte[]> getDocumentForDispaly(String fileName, String companyName);

	void documentApproved(DocumentRequest documentRequest, String documentComment);

	void documentReviewed(DocumentRequest documentRequest, String documentComment);

	public void documentRejectedOnReview(DocumentRequest documentRequest);
	
	public void documentRejectedOnApprove(DocumentRequest documentRequest);

	public void documentReEdit(DocumentRequest documentRequest);

	public List<DocumentRequest> getInActiveDocuments();
}
