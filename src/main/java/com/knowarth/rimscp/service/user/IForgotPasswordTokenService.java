package com.knowarth.rimscp.service.user;

import org.springframework.http.ResponseEntity;

import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;

public interface IForgotPasswordTokenService {

	public boolean isValidToken(final String token);

	public ResponseEntity<ResponseMessage> generateForgotPasswordToken(final String email);

	public ResponseEntity<ResponseMessage> changePassword(final String token, final String newPassword);

    public ResponseEntity<ResponseMessage> updatePassword(final String newPassword);

}
