
package com.knowarth.rimscp.service.cache;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.knowarth.rimscp.util.AppConstants;
import com.knowarth.rimscp.util.WebUtil;
import com.knowarth.rimscp.web.controller.company.zoho.TicketController;
/**
 * Cache Service
 *
 * @author vinit.prajapati
 *
 */
@Service
public class CacheService {
	@Autowired
	private CacheManager cacheManager;
	

	private static final Logger LOGGER = Logger.getLogger(CacheService.class);
 
	
	/**
	 * Clears AWS RDS & EC2 details
	 *
	 * TODO: Make time configurable from properties
	 *	NOT USED
	 */
	@Scheduled(cron = "0 0 23 * * ?")
	public void clearAWSDetails() {
		for (String name : cacheManager.getCacheNames()) {
			if (name.equals(AppConstants.CACHE_KEY_EC2_DETAILS) || name.equals(AppConstants.CACHE_KEY_RDS_DETAILS))
				cacheManager.getCache(name).clear();
		}
	}
	
	@Scheduled(cron = "0 0 23 * * ?")
	public void clearEc2Details() {
		String id = WebUtil.getLoggedInUserId().toString();
		for (String name : cacheManager.getCacheNames()) {
			LOGGER.info(" \n ****EC2 Cache Method has been Called.");
			if (name.equals(AppConstants.CACHE_KEY_EC2_DETAILS))
				cacheManager.getCache(name).evict(id);
		}
	}
	
	@Scheduled(cron = "0 0 23 * * ?")
	public void clearRDSDetails() {
		String id = WebUtil.getLoggedInUserId().toString();
		for (String name : cacheManager.getCacheNames()) {
			LOGGER.info(" \n ****RDS Cache Method has been Called.");
			if (name.equals(AppConstants.CACHE_KEY_RDS_DETAILS))
				cacheManager.getCache(name).evict(id);
		}
	}
	
	/**
	 * Clears ZOHO Ticket details
	 *
	 */
	@Scheduled(cron = "0 15 0 * * ?")
	public void clearTicketDetails() {
		String id = WebUtil.getLoggedInUserId().toString();
		for (String name : cacheManager.getCacheNames()) {
			if (name.equals(AppConstants.CACHE_KEY_ZOHO_TICKETS))
				cacheManager.getCache(name).evict(id);
		}
	}
	@Scheduled(cron = "0 15 0 * * ?")
	public void clearTicketCount() {
		String id = WebUtil.getLoggedInUserId().toString();
		for (String name : cacheManager.getCacheNames()) {
			if (name.equals(AppConstants.CACHE_KEY_ZOHO_COUNT))
				cacheManager.getCache(name).evict(id);
		}
	}
	
}