package com.knowarth.rimscp.service;

import com.knowarth.rimscp.repository.CompanyRespository;
import com.knowarth.rimscp.repository.TicketSummaryRepository;
import com.knowarth.rimscp.service.user.UserService;
import com.knowarth.rimscp.web.model.ChartData;
import com.knowarth.rimscp.web.model.ChartDataV2;
import com.knowarth.rimscp.web.model.DataSet;
import com.knowarth.rimscp.web.model.zoho.ChartDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ChartService {

	@Autowired
	private TicketSummaryRepository ticketSummaryRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private CompanyRespository companyRespository;


	private String getCurrentCompanyProjectKey(){
		return userService.getCurrentUserCompany().getProjectKey();
	}

	private List<String> getAllCompaniesProjectKeys(){
		return companyRespository.getAllProjectKeys();
	}

	private Long getCurrentCompanyId(){
		return userService.getCurrentUserCompany().getId();
	}

	//converting List<Object[]> to ChartDataHandler list
	private List<ChartDataHandler> getChartDataHandler(final List<Object[]> rawDatas) {
		List<ChartDataHandler> chartDataHandlers = new ArrayList<>();

		for (Object[] rawData : rawDatas) {

			ChartDataHandler chartDataHandler = new ChartDataHandler();
			chartDataHandler.setType((String) rawData[0]);
			if(rawData[1] == null) {
				chartDataHandler.setData(((BigInteger) BigInteger.ZERO).intValue()); // object[1] giving BigInteger
			}else {
				chartDataHandler.setData(((BigInteger) rawData[1]).intValue());
			}
			chartDataHandler.setLabel((String) rawData[2]);
			chartDataHandlers.add(chartDataHandler);
		}

		return chartDataHandlers;
	}

	//covert ChartDataHandlers to list of chartData
	private  List<ChartData> getChartData(final List<ChartDataHandler> chartDataHandlerSet) {
		List<ChartData> chartDataSet = new ArrayList<>(chartDataHandlerSet.size());

		Set<String> distinctLabels = new LinkedHashSet<>();
		// For Sorting Array based on Alphabets
		TreeSet<String> sortedNames = new TreeSet<>();

		chartDataHandlerSet.forEach((chartDataHandler) -> {
			distinctLabels.add(chartDataHandler.getLabel());
			sortedNames.add(chartDataHandler.getType());
		});

		// Adding Map for Uniqueness for
		Map<String,  Integer[]> interMediateData = new LinkedHashMap<>(distinctLabels.size());
		Map<String,  String[]> interMediateType = new LinkedHashMap<>(distinctLabels.size());

		chartDataHandlerSet.forEach((chartDataHandler) -> {
			if (interMediateData.containsKey(chartDataHandler.getLabel())) {
				Integer values[] = interMediateData.get(chartDataHandler.getLabel());
				values[sortedNames.headSet(chartDataHandler.getType()).size()] = chartDataHandler.getData();

				String types[] = interMediateType.get(chartDataHandler.getLabel());
				types[sortedNames.headSet(chartDataHandler.getType()).size()] = chartDataHandler.getType();
			} else {
				Integer values[] = new  Integer[sortedNames.size()];
				values[sortedNames.headSet(chartDataHandler.getType()).size()] = chartDataHandler.getData();
				interMediateData.put(chartDataHandler.getLabel(), values);

				String types[] = new String[sortedNames.size()];
				types[sortedNames.headSet(chartDataHandler.getType()).size()] = chartDataHandler.getType();
				interMediateType.put(chartDataHandler.getLabel(), types);
			}
		});

		// Final Populate Array
		interMediateData.forEach((k, v) -> {
			ChartData chartData = new ChartData();
			chartData.setLabel(k);
			chartData.setValues(Arrays.asList(v));
			chartData.setTypes(Arrays.asList(interMediateType.get(k)));
			chartDataSet.add(chartData);
		});

		//if values contains null then make them zero
		for(ChartData chartData : chartDataSet) {
			if(chartData.getValues().contains(null)) {
				chartData.getValues().forEach(
						(value) -> {
							if (value == null){
								int index= chartData.getValues().indexOf(value);
								chartData.getValues().set(index, 0);
							}
						}
				);
			}
		}

		List<String> uniqueTypesList = new ArrayList<>();
		//puts all possible type in a list
		for(ChartData chartData: chartDataSet){
			uniqueTypesList.addAll(chartData.getTypes());
		}
		//remove null
		uniqueTypesList.removeIf(Objects::isNull);
		//get distinct types and sort them with natural sorting order
		List<String> newTypeList = uniqueTypesList.stream().distinct().sorted().collect(Collectors.toList());
		//set all types list in chartdata with newType list
		for (ChartData chartData: chartDataSet) {
			chartData.setTypes(newTypeList);
		}
		return chartDataSet;
	}

	//covert list of chartData to new version of charData
	private static ChartDataV2 getChartDataV2(final List<ChartData> chartDataSet){
		ChartDataV2 chartDataV2 = new ChartDataV2();

		String[] labels = new String[chartDataSet.size()];
		labels = chartDataSet.stream().map(ChartData::getLabel).collect(Collectors.toList()).toArray(labels);

		chartDataV2.setLabel(labels);
		chartDataV2.setDatasets(getDataSet(chartDataSet));

		return chartDataV2;
	}

	//provide dataset for corresponding chartDataSet
	private static DataSet[] getDataSet(final List<ChartData> chartDataSet){
		DataSet[] dataSets = new DataSet[chartDataSet.get(0).getTypes().size()];
		String[] color = new String[]{"rgba(102,0,204,0.4)","rgba(0,153,204,0.4)","rgba(255,204,0,0.4)",
				"rgba(0,153,0,0.4)","rgba(197, 239, 247, 1)","rgba(20,153,0,0.4)","rgba(123,153,0,0.4)",
				"rgba(44, 130, 201, 1)", "rgba(0,153,20,0.4)","rgba(228, 241, 254, 1)","rgba(228, 34, 254, 1)",
				"rgba(228, 241, 23, 1)", "rgba(32, 241, 34, 1)","rgba(100, 25, 125, 1)"};

		for(int index=0; index<chartDataSet.get(0).getTypes().size(); index++) {
			DataSet dataSet = new DataSet();

			dataSet.setLabel(chartDataSet.get(0).getTypes().get(index));

			int data[] = new int[chartDataSet.size()];
			for(int innerIndex = 0; innerIndex < chartDataSet.size(); innerIndex++) {
				data[innerIndex] = chartDataSet.get(innerIndex).getValues().get(index);
				dataSet.setBackgroundColor(color[index]);
			}
			dataSet.setData(data);

			dataSets[index] = dataSet;
		}

		return dataSets;
	}


	public Map<String,String> getWeeklyAvgResolutionTime() {
		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			objects = ticketSummaryRepository.getWeeklyAvgResolutionTimeForCustomer(getCurrentCompanyId(),getCurrentCompanyProjectKey());
		}else{
			objects  = ticketSummaryRepository.getWeeklyAvgResolutionTime();
		}
		return getAvgData(objects);
	}


	public Map<String,String> getMonthlyAvgResolutionTime() {

		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			objects = ticketSummaryRepository.getMonthlyAvgResolutionTimeForCustomer(getCurrentCompanyId(),getCurrentCompanyProjectKey());
		}else{
			objects  = ticketSummaryRepository.getMonthlyAvgResolutionTime();
		}
		return getAvgData(objects);
	}


	public Map<String,String> getThreeMonthlyAvgResolutionTime() {

		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			objects = ticketSummaryRepository.getThreeMonthlyAvgResolutionTimeForCustomer(getCurrentCompanyId(),getCurrentCompanyProjectKey());
		}else{
			objects  = ticketSummaryRepository.getThreeMonthlyAvgResolutionTime();
		}
		return getAvgData(objects);
	}

	public Map<String,String> getYearlyAvgResolutionTime() {
		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			objects = ticketSummaryRepository.getYearlyAvgResolutionTimeForCustomer(getCurrentCompanyId(),getCurrentCompanyProjectKey());
		}else{
			objects  = ticketSummaryRepository.getYearlyAvgResolutionTime();
		}
		return getAvgData(objects);
	}

	// Resolution Time , Response Time Extract Method.
	private Map<String, String> getAvgData(List<Object[]> objects) {
		Map<String,String> mapTimeStatus = new LinkedHashMap();

		for(Object[] object : objects) {
			mapTimeStatus.put("AvgResolutionTime", (String)object[0]);
			mapTimeStatus.put("AvgResponsTime", (String)object[1]);

		}
		return mapTimeStatus;
	}


	// WEEK WISE SEVERITY
	public ChartDataV2 getWeekChartDataBySeverity() {

		List<Object[]> rawDatas;

		if(userService.isCurrentUserCustomer())
		{
			Long companyId = userService.getCurrentUserCompany().getId();
			rawDatas = ticketSummaryRepository.getWeeklyChartDataBySeverityForCustomer(companyId,getCurrentCompanyProjectKey());
		}
		else {
			rawDatas = ticketSummaryRepository.getWeeklyChartDataBySeverity(getAllCompaniesProjectKeys());
		}

		List<ChartDataHandler> chartDataHandlers = getChartDataHandler(rawDatas);

		List<ChartData> chartDataSet = getChartData(chartDataHandlers);

		return getChartDataV2(chartDataSet);
	}


	// MONTH WISE SEVERITY
	public ChartDataV2 getMonthlyChartDataBySeverity() {
		List<Object[]> rawDatas;
		if(userService.isCurrentUserCustomer())
		{
			Long companyId = userService.getCurrentUserCompany().getId();
			rawDatas = ticketSummaryRepository.getMonthlyChartDataBySeverityForCustomer(companyId,getCurrentCompanyProjectKey());
		}
		else {
			rawDatas = ticketSummaryRepository.getMonthlyChartDataBySeverity(getAllCompaniesProjectKeys());
		}

		List<ChartDataHandler> chartDataHandlers = getChartDataHandler(rawDatas);

		List<ChartData> chartDataSet = getChartData(chartDataHandlers);

		return getChartDataV2(chartDataSet);
	}

	// THREE MONTH WISE SEVERITY
	public ChartDataV2 getThreeMonthChartDataBySeverity() {

		List<Object[]> rawDatas;

		if(userService.isCurrentUserCustomer())
		{
			Long companyId = userService.getCurrentUserCompany().getId();
			rawDatas = ticketSummaryRepository.getThreeMonthChartDataBySeverityForCustomer(companyId,getCurrentCompanyProjectKey());
		}
		else {
			rawDatas = ticketSummaryRepository.getThreeMonthChartDataBySeverity(getAllCompaniesProjectKeys());
		}

		List<ChartDataHandler> chartDataHandlers = getChartDataHandler(rawDatas);

		List<ChartData> chartDataSet = getChartData(chartDataHandlers);

		return getChartDataV2(chartDataSet);
	}

	// WEEK WISE Escalation
	public ChartDataV2 getWeeklyChartDataByEscalation() {

		List<Object[]> rawDatas;

		if(userService.isCurrentUserCustomer())
		{
			Long companyId = userService.getCurrentUserCompany().getId();
			rawDatas = ticketSummaryRepository.getWeeklyChartDataByEscalationLevelForCustomer(companyId,getCurrentCompanyProjectKey());
		}
		else {
			rawDatas = ticketSummaryRepository.getWeeklyChartDataByEscalationLevel(getAllCompaniesProjectKeys());
		}

		List<ChartDataHandler> chartDataHandlers = getChartDataHandler(rawDatas);

		List<ChartData> chartDataSet = getChartData(chartDataHandlers);

		return getChartDataV2(chartDataSet);
	}


	// MONTH WISE Escalation
	public ChartDataV2 getMonthlyChartDataByEscalation() {

		List<Object[]> rawDatas;

		if(userService.isCurrentUserCustomer())
		{
			Long companyId = userService.getCurrentUserCompany().getId();
			rawDatas = ticketSummaryRepository.getMonthlyChartDataByEscalationLevelForCustomer(companyId,getCurrentCompanyProjectKey());
		}
		else {
			rawDatas = ticketSummaryRepository.getMonthlyChartDataByEscalationLevel(getAllCompaniesProjectKeys());
		}

		List<ChartDataHandler> chartDataHandlers = getChartDataHandler(rawDatas);

		List<ChartData> chartDataSet = getChartData(chartDataHandlers);

		return getChartDataV2(chartDataSet);

	}

	// Three Month WISE Escalation
	public ChartDataV2 getThreeMonthChartDataByEscalation() {

		List<Object[]> rawDatas;

		if(userService.isCurrentUserCustomer())
		{
			Long companyId = userService.getCurrentUserCompany().getId();
			rawDatas = ticketSummaryRepository.getThreeMonthChartDataByEscalationLevelForCustomer(companyId,getCurrentCompanyProjectKey());
		}
		else {
			rawDatas = ticketSummaryRepository.getThreeMonthChartDataByEscalationLevel(getAllCompaniesProjectKeys());
		}

		List<ChartDataHandler> chartDataHandlers = getChartDataHandler(rawDatas);

		List<ChartData> chartDataSet = getChartData(chartDataHandlers);

		return getChartDataV2(chartDataSet);
	}

	private Map<String, Integer> getStringIntegerMap(List<Object[]> objects) {
		Map<String, Integer> ticketData= new TreeMap<>();
		objects.forEach((objects1) -> {
			ticketData.put((String)objects1[0], ((BigInteger) objects1[1]).intValue());
		});
		return ticketData;
	}

	public Map<String, Integer> getGroupedTickedDataForOneWeek(){
		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			Long companyId = userService.getCurrentUserCompany().getId();
			objects = ticketSummaryRepository.getGroupedTickedDataForOneWeekCustomer(companyId);
		} else{
			objects = ticketSummaryRepository.getGroupedTickedDataForOneWeek();
		}
		return getStringIntegerMap(objects);
	}

	public Map<String, Integer> getGroupedTickedDataForOneMonth(){
		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			Long companyId = userService.getCurrentUserCompany().getId();
			objects = ticketSummaryRepository.getGroupedTickedDataForOneMonthCustomer(companyId);
		} else{
			objects =  ticketSummaryRepository.getGroupedTickedDataForOneMonth();
		}
		return getStringIntegerMap(objects);
	}

	public Map<String, Integer> getGroupedTickedDataForThreeMonth(){
		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			Long companyId = userService.getCurrentUserCompany().getId();
			objects = ticketSummaryRepository.getGroupedTickedDataForThreeMonthCustomer(companyId);
		} else{
			objects =  ticketSummaryRepository.getGroupedTickedDataForThreeMonth();
		}
		return getStringIntegerMap(objects);
	}

	public Map<String, Integer> getGroupedTickedDataForOneYear(){
		List<Object[]> objects;
		if(userService.isCurrentUserCustomer()){
			Long companyId = userService.getCurrentUserCompany().getId();
			objects = ticketSummaryRepository.getGroupedTickedDataForOneYearCustomer(companyId);
		} else{
			objects =  ticketSummaryRepository.getGroupedTickedDataForOneYear();
		}
		return getStringIntegerMap(objects);
	}

}
