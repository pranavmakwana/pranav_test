package com.knowarth.rimscp.util;

import org.apache.commons.lang.StringUtils;

/**
 * Web Utility Methods
 * 
 * @author arkan.malik
 */
public class RIMSUtil {

	// Checks String Value
	public static boolean isNotBlankOrNull(String pString) {
		return !StringUtils.isEmpty(pString);
	}

	// Checks Long Value
	public static boolean hasId(Long pLong) {
		return pLong != null;
	}

	// Checks Boolean Value
	public static boolean isValid(Boolean bValue) {
		return bValue != null;
	}

	// Checks Integer Value
	public static boolean hasId(Integer pInteger) {
		return pInteger != null;
	}
}
