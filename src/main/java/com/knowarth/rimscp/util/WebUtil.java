package com.knowarth.rimscp.util;


import com.knowarth.rimscp.web.model.user.RIMSUser;
import com.knowarth.rimscp.web.model.zoho.api.ResponseMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Web Utility Methods
 * 
 * @author vinit.prajapati
 */
public class WebUtil {

	private static final List<String> MONTH_NAMES = getMonthNamesList();

	/**
	 *
	 * @param status
	 * @param id
	 * @param type
	 * @return
	 */
	public static ResponseMessage getResponse(final String status, final Long id, final String type) {
		return new ResponseMessage(status, "Resource " + type + " created, id => " + id);
	}

	/**
	 * Retrieves LoggedInUser details Mainly used for setting created By & Created
	 * date at various places
	 * 
	 * @return LoggedInUser Id
	 */
	public static Long getLoggedInUserId() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		RIMSUser loggedInUser = (RIMSUser) authentication.getPrincipal();
		return loggedInUser.getId();
	}


	
	/**
	 * Generate UUID for File name or forgot password token
	 * 
	 * @return
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * @return currenTime plus 30 minutes
	 */
	public static Date getCurrentTimePlus30Minutes() {
		LocalDateTime now = LocalDateTime.now();
		return Date.from(now.plusMinutes(30).atZone(ZoneId.systemDefault()).toInstant());
	}

	public static double convertToTwoDecimalPLaces(double number){
		DecimalFormat df = new DecimalFormat("#.00");
		String numberFormated = df.format(number);
		return Double.parseDouble(numberFormated);
	}

	public static String getCurrentYear(){
		LocalDateTime l = LocalDateTime.now();
		return Integer.toString(l.getYear());
	}

	public static String getPreviousYear(){
		LocalDateTime l = LocalDateTime.now();
		return Integer.toString(l.getYear()-1);
	}

	public static String getCurrentMonth(){
		LocalDateTime l = LocalDateTime.now();
		return l.getMonth().name();
	}

	public static String getPreviousMonth(){
		LocalDateTime l = LocalDateTime.now();
		return Month.of(l.getMonth().ordinal()).name();
	}

	public static List<String> getMonthNamesList() {
		Month[] monthArray = Month.values();
		List<String> monthNames = new ArrayList<>();
		monthNames = Arrays.stream(monthArray).map(Enum::name).collect(Collectors.toList());
		return monthNames;
	}

	public static int getCurrentMonthIndex(){
		return MONTH_NAMES.indexOf(getCurrentMonth());
	}

	public static List<String> getPreviousMonthList(){
		List<String> newMonths = new ArrayList<>(3);
		int currentMonthIndex = MONTH_NAMES.indexOf(getCurrentMonth());
		if(currentMonthIndex == 2) {
			newMonths.add("FEBRUARY");
			newMonths.add("JANUARY");
			newMonths.add("DECEMBER");
		}else if(currentMonthIndex == 1){
			newMonths.add("JANUARY");
			newMonths.add("DECEMBER");
			newMonths.add("NOVEMBER");
		}else if(currentMonthIndex == 0){
			newMonths.add("DECEMBER");
			newMonths.add("NOVEMBER");
			newMonths.add("OCTOBER");
		}else {
			for (int i = currentMonthIndex-1; i > (currentMonthIndex -4) ; i--) {
				newMonths.add(MONTH_NAMES.get(i));
			}
		}
		return newMonths;
	}



//	static SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	static SimpleDateFormat myFormat = new SimpleDateFormat("dd-MMM-yy");
//	static SimpleDateFormat otherFormat = new SimpleDateFormat("dd-MMM-yy HH:mm");
//	public static void main(String[] args) {
//		try {
//			String reformattedStr = otherFormat.format(fromUser.parse("2020-05-21 18:03:23"));
//			System.out.println(reformattedStr);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//	}

	public static List<List<String>> getCurrentYearMonthList(){

		List<List<String>> yearMonthList = new ArrayList<>();
		List<String> currentYearMonths = new ArrayList<>();
		List<String> previousYearMonths = new ArrayList<>();
		int currentMonthIndex = getCurrentMonthIndex();
		for (int i = currentMonthIndex; i < 12 ; i++) {
			previousYearMonths.add(MONTH_NAMES.get(i));
		}
		for(int i = currentMonthIndex; i >= 0 ; i--){
			currentYearMonths.add(MONTH_NAMES.get(i));
		}
		yearMonthList.add(previousYearMonths);
		yearMonthList.add(currentYearMonths);
		return yearMonthList;
	}

	public static Long diffBetweenTwoDates(Date date1,Date date2) {
		Long sec = (date2.getTime() - date1.getTime())/1000;
		return sec;
	}

}
