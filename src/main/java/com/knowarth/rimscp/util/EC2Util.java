package com.knowarth.rimscp.util;

import java.util.ArrayList;
import java.util.List;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.knowarth.rimscp.web.model.aws.EC2Detail;
 

public class EC2Util
{

	public static void main(String[] args)
	{
		final AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicCredentials()))
				.withRegion(Regions.fromName("ap-south-1")).build();

		// us-east-1 ap-south-1
		boolean done = false;
		List<EC2Detail> tAWSList = new ArrayList<EC2Detail>();
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		while (!done)
		{
			DescribeInstancesResult response = ec2.describeInstances(request);

			for (Reservation reservation : response.getReservations())
			{
				System.out.println("*************************************************************************************");

				for (Instance instance : reservation.getInstances())
				{

					System.out.println(instance.getTags().get(0).getKey() + ": " + instance.getTags().get(0).getValue());
					System.out.println("Public IP: " + instance.getPublicIpAddress());
					System.out.println("Private IP: " + instance.getPrivateIpAddress());
					System.out.println("Instance Type: " + instance.getInstanceType());
					System.out.println("Instance Id: " + instance.getInstanceId());
					System.out.println("Volume: " + instance.getBlockDeviceMappings().get(0).getEbs().getVolumeId());
					System.out.println("CPUs: " + instance.getCpuOptions().getCoreCount());
					System.out.println("Availability Zone: " + instance.getPlacement().getAvailabilityZone());
					System.out.println("Status: " + instance.getState().getName());
					System.out.println("Launch Time: " + instance.getLaunchTime());
					System.out.println(instance);
					System.out.println("\n\n\n");
				}
				System.out.println("*************************************************************************************");
			}
			request.setNextToken(response.getNextToken());
			if (response.getNextToken() == null)
			{
				done = true;
			}
		}
	}

	private static BasicAWSCredentials basicCredentials()
	{
		return new BasicAWSCredentials("AKIAW6DX4NH3ZUGGPHGK", "uqkrLDqfUdqIg0IPAQkEXK45fiDBX9zd1GUtuH1p");
	}
}
