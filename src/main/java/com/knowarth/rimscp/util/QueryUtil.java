package com.knowarth.rimscp.util;

import java.util.Date;

public class QueryUtil {

	public static String getWeeklyRecordsSQL(final String column) {
		// Group by day wise
		// select severity, count(severity), DATE_FORMAT(createdatee,'%d-%m-%y')
		// from tickets group by DATE_FORMAT(createdatee,'%d-%m-%y');
		return null;
	}

	public static String getMonthlyRecordsSQL(final String column) {
		return getSQL(column, "Week 1", 28, 22) 
				 + " UNION ALL " + 
			 getSQL(column, "Week 2", 21, 15) 	
				 + " UNION ALL " + 
			 getSQL(column, "Week 3", 14, 8) 
				 + " UNION ALL " + 
			 getSQL(column, "Week 4", 7, 0);
	}

	public static String getThreeMonthsRecordsSQL(final String column) {
		return getSQL(column, "Fortnight 1", 90, 76) 
				 + " UNION ALL " + 
			 getSQL(column, "Fortnight 2", 75, 61) 	
				 + " UNION ALL " + 
			 getSQL(column, "Fortnight 3", 60, 46) 
				 + " UNION ALL " + 
			 getSQL(column, "Fortnight 4", 45, 31)
				 + " UNION ALL " + 
			 getSQL(column, "Fortnight 5", 30, 16) 
				 + " UNION ALL " + 
			 getSQL(column, "Fortnight 6", 15, 0);
	}

	public static String getDateRangeRecords(Date startDate, Date endDate) {
		// Divide in 5 range
		
		return null;
	}

	private static String getSQL(final String column, final String data, final int start, final int end) {
		return "select " + column + ", count(" + column + "), " + data + " from tickets " + " where "
				+ dateFilter("createdDate", start, end);
	}

	private static String dateFilter(String column, int start, int end) {
		// https://dba.stackexchange.com/questions/24262/get-two-weeks-of-data-but-group-by-a-period-of-7-days
		return column + " >= CURDATE() - INTERVAL  " + start + " DAY " + "      AND " + column
				+ "  < CURDATE() - INTERVAL  " + end + " DAY";

	}


	public static void main(String[] args) {
		System.out.println(getThreeMonthsRecordsSQL("severity"));
	}

}
