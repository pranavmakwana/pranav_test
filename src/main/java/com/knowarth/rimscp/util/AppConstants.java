package com.knowarth.rimscp.util;

/**
 * Application Constants
 * 
 * @author vinit.prajapati
 *
 */ 
public class AppConstants {

	/* Customer Role */
	public static final String ROLE_CUSTOMER = "Customer";

	/* Technician Role */
	public static final String ROLE_TECHNICIAN = "Technician";

	/* Lead Role */
	public static final String ROLE_LEAD = "Lead";

	/* Manager Role */
	public static final String ROLE_MANAGER = "ROLE_Manager";

	public static final String COMMA = ",";

	public static final String DASH = "---";

	public static final String NOTIFICATION_STATUS_NEW = "New";
	
	public static final String NOTIFICATION_STATUS_REVIEW = "Available For Review";
	
	public static final String NOTIFICATION_STATUS_REVIEWED = "Reviewed";

	public static final String NOTIFICATION_STATUS_APPROVE = "Available For Approve";

	public static final String NOTIFICATION_STATUS_COMPLETED = "Approved";
	
	public static final String NOTIFICATION_STATUS_REJECTED_ON_REVIEW = "Rejected on Review";
	
	public static final String NOTIFICATION_STATUS_REJECTED_ON_APPROVE = "Rejected on Approve";

	// Cache Keys

	/** EC2 Cache Key **/
	public static final String CACHE_KEY_EC2_DETAILS = "ecdetail";

	/** RDS Cache Key **/
	public static final String CACHE_KEY_RDS_DETAILS = "rdsdetail";
	
	/** Ticket Cache Key **/
	public static final String CACHE_KEY_ZOHO_TICKETS = "TicketDetail";
	
	public static final String CACHE_KEY_ZOHO_COUNT = "ticketCount";
	
	// Type of Notifications
	public static final String TYPE_DOCUMENT = "Document";

	public static final String TYPE_REPORT = "Report";
	
	public static final String TYPE_STAGING = "staging";
	
	public static final String ENTITY_TEMPLATE = "template";
	
	public static final String ENTITY_SECTION = "section";
	
	public static final String ENTITY_ACTIVITY = "activity";
	
	public static final String ENTITY_COMPANY = "company";
	
	public static final String ENTITY_USER = "user";
	
	public static final String ENTITY_REPORT = "report";
	
	public static final String ENTITY_DOCUMENT = "document";
}
