# Getting Started

### Database setup
```
create database rimscp;
CREATE USER 'rimscp'@'localhost' IDENTIFIED BY 'rimscp';
GRANT ALL PRIVILEGES ON rimscp.* TO 'rimscp'@'localhost';
flush privileges;
```

#### Reference Queries
```
insert into roles (id, name) values(1,'Manager');
insert into roles (id, name) values(2,'Lead');
insert into roles (id, name) values(3,'Technician');
insert into roles (id, name) values(4,'Customer');

 insert into companies( name, address, website, contactno) values('Knowarth Technologies', '11, Aryan Corporate Park, Ahmedabad','https://knowarth.com','+91(0) 79 2234 4565');

--password is 'password'
insert into users( firstname, lastname, email, password, roleid, companyid) 
	values('Vinit', 'Prajapati','vinit.prajapati@knowarth.com', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6',1,1);


```
### Setting up Lombok in Eclipse or STS
```
	1) Download Lombok jar : https://projectlombok.org/download
	2) Eclipse : 
		1.Close eclipse
		2.Run lombok.jar
		2.it will auto detect eclipse.exe > Install and CLose
		3.Restart eclipse
	OR
	2) STS : 
		1.Close STS
		2.Copy lombok in folder where SpringToolSuite4.exe is present (say : /sts-4.1.1.RELEASE)
		3.Run lombok jar : select SpringToolSuite4.exe > Install close
		4.Make sure afer install > Open SpringToolSuite4.ini > -javaagent:lombok.jar is present
		5.Restart STS
	
	3) Adding new POJO : Just add @Data annotation at class level in order to use lombok
```

